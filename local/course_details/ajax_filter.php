<?php

require_once('../../config.php');
require_once('lib.php');
$context = context_system::instance();
$PAGE->set_context($context);
global $DB;
if (isset($_POST["search_text"])) {
	$search_text = $_POST["search_text"];
}
if (isset($_POST["course_categories"])) {
	$course_categories = (array) $_POST["course_categories"];
}
if (isset($_POST["level"])) {
	$level = (array) $_POST["level"];
}
if (isset($_POST["language"])) {
	$language = (array) $_POST["language"];
}
if (isset($_POST["cost_type"])) {
	$cost_type = (array) $_POST["cost_type"];
}
if (isset($_POST["affiliation_name"])) {
	$affiliation_name = (array) $_POST["affiliation_name"];
}
if (isset($_POST["delivery_type"])) {
	$delivery_type = (array) $_POST["delivery_type"];
}
if (isset($_POST["certificate_available"])) {
	$certificate_available = (array) $_POST["certificate_available"];
}

$html = '';
$sql = 'SELECT DISTINCT c.id
		FROM {course} c
		INNER JOIN {customfield_data} cd ON c.id=cd.instanceid
		INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id';
$sql .=' WHERE c.visible=1 && c.id!=1 ';

// if search any text
if(!empty($search_text)){
	$sql .=" && (c.fullname LIKE '%".$search_text."%' || c.shortname  LIKE '%".$search_text."%' || c.summary LIKE '%".$search_text."%')";
	
}

// if category selected
if(!empty($course_categories)){
	$sql .=' && c.category IN (';
	$i=0;
	
	foreach ($course_categories as $ccid) {
		if($i>0){
			$sql .=',';
		}
		$sql .=$ccid;
		$i++;
	}

	$sql .=')';
}

$course_details = $DB->get_records_sql($sql);
foreach ($course_details as $key => $value) {
	$course_details_new[]=$key;
}


// if level is selected
$course_details_level_new=[];
if(!empty($level)){
	$sql_level = $sql.' && (cf.shortname="level" && cd.value IN (';
	$i=0;
	
	foreach ($level as $level_val) {
		if($i>0){
			$sql_level .=',';
		}
		$sql_level .=$level_val;
		$i++;
	}

	$sql_level .='))';

	$course_details_level = $DB->get_records_sql($sql_level);
	foreach ($course_details_level as $key => $value) {
		$course_details_level_new[]=$key;
	}


}

// if language is selected
$course_details_language_new=[];
if(!empty($language)){
	$sql_language = $sql.' && (cf.shortname="language" && cd.value IN (';
	$i=0;
	
	foreach ($language as $language_val) {
		if($i>0){
			$sql_language .=',';
		}
		$sql_language .=$language_val;
		$i++;
	}

	$sql_language .='))';

	$course_details_language = $DB->get_records_sql($sql_language);

	foreach ($course_details_language as $key => $value) {
		$course_details_language_new[]=$key;
	}

}

// if cost_type is selected
$course_details_cost_type_new=[];
if(!empty($cost_type)){
	$sql_cost_type = $sql.' && (cf.shortname="cost_type" && cd.value IN (';
	$i=0;
	
	foreach ($cost_type as $cost_type_val) {
		if($i>0){
			$sql_cost_type .=',';
		}
		$sql_cost_type .=$cost_type_val;
		$i++;
	}

	$sql_cost_type .='))';
	$course_details_cost_type = $DB->get_records_sql($sql_cost_type);

	foreach ($course_details_cost_type as $key => $value) {
		$course_details_cost_type_new[]=$key;
	}
}

// if affiliation_name is selected
$course_details_affiliation_name_new=[];
if(!empty($affiliation_name)){
	$sql_affiliation_name = $sql.' && (cf.shortname="affiliation_name" && cd.value IN (';
	$i=0;
	
	foreach ($affiliation_name as $affiliation_name_val) {
		if($i>0){
			$sql_affiliation_name .=',';
		}
		$sql_affiliation_name .=$affiliation_name_val;
		$i++;
	}

	$sql_affiliation_name .='))';

	$course_details_affiliation_name = $DB->get_records_sql($sql_affiliation_name);

	foreach ($course_details_affiliation_name as $key => $value) {
		$course_details_affiliation_name_new[]=$key;
	}
}

// if delivery_type is selected
$course_details_delivery_type_new=[];
if(!empty($delivery_type)){
	$sql_delivery_type = $sql.' && (cf.shortname="delivery_type" && cd.value IN (';
	$i=0;
	
	foreach ($delivery_type as $delivery_type_val) {
		if($i>0){
			$sql_delivery_type .=',';
		}
		$sql_delivery_type .=$delivery_type_val;
		$i++;
	}

	$sql_delivery_type .='))';

	$course_details_delivery_type = $DB->get_records_sql($sql_delivery_type);

	foreach ($course_details_delivery_type as $key => $value) {
		$course_details_delivery_type_new[]=$key;
	}
}

// if certificate_available is selected
$course_details_certificate_available_new=[];

if(!empty($certificate_available)){
	$sql_certificate_available = $sql.' && (cf.shortname="certificate_available" && cd.value IN (';
	$i=0;
	
	foreach ($certificate_available as $certificate_available_val) {
		if($i>0){
			$sql_certificate_available .=',';
		}
		$sql_certificate_available .=$certificate_available_val;
		$i++;
	}

	$sql_certificate_available .='))';

	$course_details_certificate_available = $DB->get_records_sql($sql_certificate_available);

	foreach ($course_details_certificate_available as $key => $value) {
		$course_details_certificate_available_new[]=$key;
	}
}


//get all the filter arrays.
$array=array();
// create array of arrays
$array[] = $course_details_new;
$array[] = $course_details_level_new;
$array[] = $course_details_language_new;
$array[] = $course_details_cost_type_new;
$array[] = $course_details_affiliation_name_new;
$array[] = $course_details_delivery_type_new;
$array[] = $course_details_certificate_available_new;


//filter out empty array
$array = array_filter($array);
if(count($array) > 1){
  $lastarray=array_intersect(...$array);
}else{
  foreach ($array as $akey => $avalue) {
    $lastarray = $avalue;
  }
}

// print_object($lastarray);

if (!empty($lastarray)) {
	foreach ($lastarray as $cid) {
		$html .= display_course_card($cid);
	}
	echo $html;
} else {
	echo '<h1 class="text-danger text-center">No record found</h1>';
}

function myfunction($a,$b)
{
if ($a===$b)
  {
  return 0;
  }
  return ($a>$b)?1:-1;
}

// echo '<h1>AJEX WORKING FINE</h1>';