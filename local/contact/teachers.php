<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package
 * @copyright
 * @copyright
 * @license
**/
require_once('../../config.php');
defined('MOODLE_INTERNAL') || die();
global $CFG, $DB;
global $PAGE,$OUTPUT;
$context = context_system::instance();
$contextid = $context->contextlevel;
$title = "Our Teachers";
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_title($title);
$PAGE->set_url($CFG->wwwroot . '/local/contact/teachers.php');

echo $OUTPUT->header();
$html = "";
// $html  .= html_writer::start_tag('div',array('class'=>'container-fluid'));//container start//
// 	$html .=html_writer::start_tag('div',array('class'=>'row p-5'));//row start
$html .='<div class="advisor-area bg-gray advisor-padding bottom-less bg-cover">';
$html .='<div class="container">';

$html .='<div class="row">';
$html .='<div class="col-12">';
$html .='<div class="section__title text-center">
							<h2 class="title__line">Our Valuable Contributors</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>';
$html .='</div>';
$html .='</div>';

$html .='<div class="row">';
$html .='<div class=" advisor-items  text-center col-12">';

$sql = "SELECT DISTINCT userid FROM {role_assignments} WHERE roleid = 3";
$teachers = $DB->get_records_sql($sql);

foreach ($teachers as $teacher) {

  $getuserdetails = $DB->get_record('user', array('id'=>$teacher->userid));
  $userfullname = $getuserdetails->firstname.' '.$getuserdetails->lastname;
  $userdesc = substr($getuserdetails->description,0,100);
  $userdept = $getuserdetails->department;
  $usermobile = $getuserdetails->phone2;

  $options = array(
    'size' => 200,
    'includefullname' => false,          // New in Moodle 3.4. Setting to true will render the user's full name beside it. Defaults to false.
);
   $user_picture = new user_picture($getuserdetails,$options);
   //$src = $user_picture->get_url($PAGE);
   $userpic2 = $OUTPUT->user_picture($getuserdetails, $options);

  //$userpic = new moodle_url('/user/pix.php/'.$teacher->userid.'/f3.jpg');

  $html .= '<div class="col-md-4 col-sm-6 single-item">
                          <div class="item">
                              <div class="thumb">
                                  '.$userpic2.'
                                  <p>'.$userdesc.'</p>
                              </div>
                              <div class="info text-light">
                                  <span>'.$userdept.' | '.$usermobile.'</span>
                                  <h4>'.$userfullname.'</h4>
                              </div>
                          </div>
                      </div>';
                    }


$html .='</div>';
$html .='</div>';
$html .='</div>';
$html .='</div>';


echo $html;

echo $OUTPUT->footer();


/*
<ul>
    <li class="facebook">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
    </li>
    <li class="twitter">
        <a href="#"><i class="fab fa-twitter"></i></a>
    </li>
    <li class="dribbble">
        <a href="#"><i class="fab fa-dribbble"></i></a>
    </li>
    <li class="youtube">
        <a href="#"><i class="fab fa-youtube"></i></a>
    </li>
</ul>
*/
