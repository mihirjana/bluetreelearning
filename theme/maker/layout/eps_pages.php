<?php


defined('MOODLE_INTERNAL') || die();

$bodyattributes = $OUTPUT->body_attributes([]);

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => false
];

$cid = optional_param('cid','',PARAM_INT);
if(!empty($cid)){
	global $DB,$CFG;
	require_once($CFG->dirroot.'/local/course_details/lib.php');   
	$course_detail = $DB->get_record('course',array('id'=>$cid));

	if(!empty($course_detail)){

		$courseimage = course_image_cd($course_detail);
		$coursename = $course_detail->fullname;
		$course_details_page_link = new moodle_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$cid));
		$templatecontext['og_metatag_image'] = $courseimage;
		$templatecontext['og_metatag_title'] = $coursename;
		$templatecontext['og_metatag_page_link'] = $course_details_page_link;
	}

}

$pageid = optional_param('page','',PARAM_INT);
if(!empty($pageid)){
	require_once($CFG->dirroot.'/local/edu_blogs/lib.php');   
	$block_detail = display_single_blog_meta_data($pageid);

	if(!empty($block_detail)){

		$templatecontext['og_metatag_image'] = $block_detail['image'];
		$templatecontext['og_metatag_title'] = $block_detail['subject'];
		$templatecontext['og_metatag_page_link'] = $block_detail['blog_url'];
	}

}

$PAGE->requires->jquery ();
$PAGE->requires->js('/theme/maker/plugins/back-to-top.js');

echo $OUTPUT->render_from_template('theme_maker/eps_pages', $templatecontext);

