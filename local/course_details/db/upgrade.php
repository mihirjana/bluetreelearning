<?php
// This file keeps track of upgrades to
// the assignment module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installation to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the methods of database_manager class
//
// Please do not forget to use upgrade_set_timeout()
// before any action that may take longer time to finish.

defined('MOODLE_INTERNAL') || die();

 function xmldb_local_course_details_upgrade($oldversion) {
     global $CFG,$DB;

    $dbman = $DB->get_manager();
    //      if ($oldversion < 2019121001) {

    //     // Define field patient_id to be added to patient_complete_details.
    //         $table = new xmldb_table('local_course_details');
    //     //organization Address
    //         $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, 
    //             XMLDB_NOTNULL, XMLDB_SEQUENCE, null); 
    //         $table->add_field('catid', XMLDB_TYPE_INTEGER, '10',
    //             null, null,null, null, null);
    //         $table->add_field('catdescription', XMLDB_TYPE_TEXT, '250',
    //             null, null,null, null, null);
    //         $table->add_field('catimage', XMLDB_TYPE_TEXT, '20',
    //             null, null,null, null, null);
    //         $table->add_field('catlink', XMLDB_TYPE_TEXT, '30',
    //             null, null,null, null, null);
    //         $table->add_field('catdisplay', XMLDB_TYPE_TEXT, '20',
    //             null, null,null, null, null);
    //         $table->add_field('catlogo', XMLDB_TYPE_TEXT, '20',
    //             null, null,null, null, null);
    //         $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    //     // Conditionally launch add field organization address.
    //     if (!$dbman->table_exists($table)) {
    //         $dbman->create_table($table);
    //     }

    //     // Patientrecord savepoint reached.
    //     upgrade_plugin_savepoint(true, 2019121001,'local', 'course_details');
    // }
    //for more info table
    // if ($oldversion < 2020010902) {

    //     // Define field patient_id to be added to patient_complete_details.
    //     $table = new xmldb_table('local_gs_moreinfo');
    //     //organization Address
    //     $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, 
    //         XMLDB_NOTNULL, XMLDB_SEQUENCE, null); 
    //     $table->add_field('firstname', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('lastname', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('email', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('contactno', XMLDB_TYPE_INTEGER, '20',
    //         null, null,null, null, null);
    //     $table->add_field('corporatename', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('upskills', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('comment', XMLDB_TYPE_TEXT, '250',
    //         null, null,null, null, null);
    //     $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    //     // Conditionally launch add field organization address.
    //     if (!$dbman->table_exists($table)) {
    //         $dbman->create_table($table);
    //     }

    //     // Patientrecord savepoint reached.
    //     upgrade_plugin_savepoint(true, 2020010902,'local', 'course_details');
    // }
    //  //for newsletter table
    // if ($oldversion < 2020010902) {

    //     // Define field patient_id to be added to patient_complete_details.
    //     $table = new xmldb_table('local_gs_newsletter');
    //     //organization Address
    //     $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, 
    //         XMLDB_NOTNULL, XMLDB_SEQUENCE, null); 
    //     $table->add_field('emailid', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('subdate', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_field('unsubdate', XMLDB_TYPE_TEXT, '50',
    //         null, null,null, null, null);
    //     $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    //     // Conditionally launch add field organization address.
    //     if (!$dbman->table_exists($table)) {
    //         $dbman->create_table($table);
    //     }

    //     // Patientrecord savepoint reached.
    //     upgrade_plugin_savepoint(true, 2020010902,'local', 'course_details');
    // }
 //for contact table
    if ($oldversion < 2020010903) {

        // Define field patient_id to be added to patient_complete_details.
        $table = new xmldb_table('local_gs_contact');
        //organization Address
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, 
            XMLDB_NOTNULL, XMLDB_SEQUENCE, null); 
        $table->add_field('firstname', XMLDB_TYPE_TEXT, '50',
            null, null,null, null, null);
        $table->add_field('lastname', XMLDB_TYPE_TEXT, '50',
            null, null,null, null, null);
        $table->add_field('email', XMLDB_TYPE_TEXT, '50',
            null, null,null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch add field organization address.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Patientrecord savepoint reached.
        upgrade_plugin_savepoint(true, 2020010903,'local', 'course_details');
    }

    if ($oldversion < 2020010905) {

        $table = new xmldb_table('local_course_details');
        //organization Address
        $field1 = new xmldb_field('std_login_pg_title', XMLDB_TYPE_CHAR, '256', null, null, null, null, 'catlogo');
        $field2 = new xmldb_field('std_login_pg_desc', XMLDB_TYPE_CHAR, '256', null, null, null, null, 'std_login_pg_title');
        $field3 = new xmldb_field('std_login_pg_logo', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'std_login_pg_desc');
        $field4 = new xmldb_field('std_login_pg_bgimg', XMLDB_TYPE_INTEGER, '10', null, null, null, 0, 'std_login_pg_logo');
         $field5 = new xmldb_field('std_login_pg_color', XMLDB_TYPE_CHAR, '256', null, null, null, null, 'std_login_pg_title');

        
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        if (!$dbman->field_exists($table, $field3)) {
            $dbman->add_field($table, $field3);
        }
        if (!$dbman->field_exists($table, $field4)) {
            $dbman->add_field($table, $field4);
        }
        if (!$dbman->field_exists($table, $field5)) {
            $dbman->add_field($table, $field5);
        }
        upgrade_plugin_savepoint(true, 2020010905,'local', 'local_course_details');
    }

    if ($oldversion < 2020010906) {

        $table = new xmldb_table('local_course_details');
        $field2 = new xmldb_field('catsupportemail', XMLDB_TYPE_CHAR, '256', null, null, null, null, 'catdisplay');

        
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        upgrade_plugin_savepoint(true, 2020010906,'local', 'local_course_details');
    }

      if ($oldversion < 2020010910) {

        // Define field patient_id to be added to patient_complete_details.
        $table = new xmldb_table('local_wishlist');
        //organization Address
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, 
            XMLDB_NOTNULL, XMLDB_SEQUENCE, null); 
        $table->add_field('userid', XMLDB_TYPE_CHAR, '256',
            null, null,null, null, null);
        $table->add_field('course_id', XMLDB_TYPE_CHAR, '256',
            null, null,null, null, null);
        $table->add_field('date', XMLDB_TYPE_CHAR, '256',
            null, null,null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch add field organization address.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Patientrecord savepoint reached.
        upgrade_plugin_savepoint(true, 2020010910,'local', 'local_wishlist');
    }
     if ($oldversion < 2020010911) {

        // Define field patient_id to be added to patient_complete_details.
        $table = new xmldb_table('local_wishlist');

        $field1 = new xmldb_field('reg_data', XMLDB_TYPE_CHAR, '256', null, null, null, null, 'course_id');
     

        // Conditionally launch add field organization address.
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }

        // Patientrecord savepoint reached.
        upgrade_plugin_savepoint(true, 2020010911,'local', 'local_wishlist');
    }



    return true;
}
