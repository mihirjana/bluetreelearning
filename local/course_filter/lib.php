<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**

 */
function filter_get_sectors() {
  global $DB;
  $data = $DB->get_records_sql('SELECT * FROM {course_categories} where visible = 1 order by name');
  // print_object($data);
  return $data;
}
//get customfield dropdown values for dropdown.
function filter_get_customfield_dropdown($shortname){
  global $DB;
  // $course_extradetail->shortname = $shortname;
    $getconfigdata = $DB->get_record('customfield_field', array('shortname' => $shortname));

    if (!empty($getconfigdata)) {
      $data['configdata']['options'] = $getconfigdata->configdata;

      $datafieldid = $getconfigdata->id; // this is the field id important we need for faster query once user submits the form

      $explode = explode(":",$getconfigdata->configdata);
      $exp3 = $explode[3];

      $explode22 = explode(",",$exp3);

      $finalstr = $explode22[0];
      $mystr =  str_replace('"',' ',$explode22[0]);
      $skuList = str_replace("\\r\\n", "\n", $mystr);
      $skuList = str_replace("\\n\\r", "\n", $skuList);
      $skuList = preg_split("/\n/", $skuList);
      $i = 1;
      foreach ($skuList as $key => $values) {
        $course_extradetails_arr2[$i] = array('id'=>$i, 'fieldid' => $datafieldid, 'coursetypevalue'=>$values) ;
        $i = $i +1;
      }
    }
  return $course_extradetails_arr2;
}

//get customfield values as an array.
function filter_get_customfieldarray($customname){
  global $DB;
  // $course_extradetail->shortname = $customname;

  $getconfigdata = $DB->get_record('customfield_field', array('shortname' => $customname));

  if (!empty($getconfigdata)) {
    $data['configdata']['options'] = $getconfigdata->configdata;

      $datafieldid = $getconfigdata->id; // this is the field id important we need for faster query once user submits the form

      $explode = explode(":",$getconfigdata->configdata);
      $exp3 = $explode[3];

      $explode22 = explode(",",$exp3);

      $finalstr = $explode22[0];
      $mystr =  str_replace('"',' ',$explode22[0]);
      $skuList = str_replace("\\r\\n", "\n", $mystr);
      $skuList = str_replace("\\n\\r", "\n", $skuList);
      $skuList = preg_split("/\n/", $skuList);
      $i = 1;
    }

    return $skuList;
  }




  function filter_all_enrolled_usersdata($courseid)
  {
    global $DB;
    $sql ="SELECT DISTINCT ue.id, u.id AS userid, c.id AS courseid, ue.timecreated as enroldate
    FROM mdl_user u
    JOIN mdl_user_enrolments ue ON ue.userid = u.id
    JOIN mdl_enrol e ON e.id = ue.enrolid
    JOIN mdl_role_assignments ra ON ra.userid = u.id
    JOIN mdl_context ct ON ct.id = ra.contextid
    AND ct.contextlevel =50
    JOIN mdl_course c ON c.id = ct.instanceid
    AND e.courseid = c.id
    JOIN mdl_role r ON r.id = ra.roleid
    AND r.shortname =  'student'
    WHERE e.status =0
    AND u.suspended =0
    AND u.deleted =0
    AND courseid =".$courseid."";
    $allenrolleduser = $DB->get_records_sql($sql);
    $listofusers =[];
    foreach ($allenrolleduser as $user) {
      $listofusers[] = $user->enroldate;
    }
    sort($listofusers);
    return  $listofusers;
  }


  function filter_get_course_image($course_detail) {
    global $CFG, $PAGE, $OUTPUT;
  // Get course overview files.
    if (empty($CFG->courseoverviewfileslimit)) {
      return '';
    }
    require_once ($CFG->libdir . '/filestorage/file_storage.php');
    require_once ($CFG->dirroot . '/course/lib.php');
    $fs = get_file_storage();
    $context = context_course::instance($course_detail);
    $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', false, 'filename', false);
    if (count($files)) {
      $overviewfilesoptions = course_overviewfiles_options($course_detail);
      $acceptedtypes = $overviewfilesoptions['accepted_types'];
      if ($acceptedtypes !== '*') {
          // Filter only files with allowed extensions.
        require_once ($CFG->libdir . '/filelib.php');
        foreach ($files as $key => $file) {
          if (!file_extension_in_typegroup($file->get_filename() , $acceptedtypes)) {
            unset($files[$key]);
          }
        }
      }
      if (count($files) > $CFG->courseoverviewfileslimit) {
          // Return no more than $CFG->courseoverviewfileslimit files.
        $files = array_slice($files, 0, $CFG->courseoverviewfileslimit, true);
      }
    }

  // Get course overview files as images - set $courseimage.
  // The loop means that the LAST stored image will be the one displayed if >1 image file.
    $courseimage = '';
    foreach ($files as $file) {
      $isimage = $file->is_valid_image();
      if ($isimage) {
        $courseimage = file_encode_url("$CFG->wwwroot/pluginfile.php", '/' . $file->get_contextid() . '/' . $file->get_component() . '/' . $file->get_filearea() . $file->get_filepath() . $file->get_filename() , !$isimage);
      }
    }

    if($courseimage!=''){
      return $courseimage;
    }else{
      return $CFG->wwwroot.'/local/course_filter/pics/course_1.png';
    }


  }


  function filter_get_course_filter_list($request_data) {
    global $DB;
   

        //creating temp array.
    $temparray=[];
  //get all the courses from selected categories.
    $searchcourses=[];
    if(!empty($request_data['search'])){
      $searchkeyword = $request_data['search'];
      $searchsql="SELECT id from {course} WHERE fullname LIKE '%".$searchkeyword."%' OR shortname LIKE '%".$searchkeyword."%'";
      $searchedcourses=$DB->get_records_sql($searchsql);
      foreach ($searchedcourses as $svalue) {
        $searchcourses[]=$svalue->id;
      }
      if(!empty($searchcourses)){
        $temparray[]=$searchcourses;
      }
    }
    $catcourses=[];
    if(!empty($request_data['sectorid'])){
    	foreach ($request_data['sectorid'] as $categoryid) {
    		$sql="SELECT c.id FROM {course} c
    		JOIN {course_categories} ct  ON ct.id=c.category
    		WHERE c.category=$categoryid OR ct.path LIKE '%/".$categoryid."/%'";
    		$course=$DB->get_records_sql($sql);
    		foreach ($course as $cid) {
    			$catcourses[] = $cid->id;
    		}
    	}
      if(!empty($catcourses)){
        $temparray[]=$catcourses;
      }
    }
    $modcourses=[];
    if(!empty($request_data['modid'])){
    	$modes = $request_data['modid'];
    	foreach ($modes as $mkey => $mvalue) {
    		$marray=explode("-", $mvalue);
    		$fieldid = $marray[0];
    		$fieldvalue = $marray[1];

    		$mquery='SELECT instanceid FROM {customfield_data} WHERE
    		fieldid = '.$fieldid.' AND value = '.$fieldvalue.'';
    		$modcourse=$DB->get_records_sql($mquery);
    		if(!empty($modcourse)){
    			foreach ($modcourse as $mdvalue) {
    				$modcourses[]=$mdvalue->instanceid;
    			}
    		}
    	}
            if(!empty($modcourses)){
        $temparray[]=$modcourses;
      } else {
        return null;
      }
    }
    $langcourses=[];
    if(!empty($request_data['lang'])){

    	$lang = $request_data['lang'];
    	foreach ($lang as $lkey => $lvalue) {
    		$larray=explode("-", $lvalue);
    		$lfieldid = $larray[0];
    		$lfieldvalue = $larray[1];

    		$lquery='SELECT instanceid FROM {customfield_data} WHERE
    		fieldid = '.$lfieldid.' AND value = '.$lfieldvalue.'';
    		$langcourse=$DB->get_records_sql($lquery);
    		if(!empty($langcourse)){
    			foreach ($langcourse as $lgvalue) {
    				$langcourses[]=$lgvalue->instanceid;
    			}
    		}
    	}
      if(!empty($langcourses)){
        $temparray[]=$langcourses;
      }else {
        return null;
      }
    }
    //delivery type
    $cmodcoursearray=[];
    if(!empty($request_data['cmodid'])){
    	$cmod = $request_data['cmodid'];
    	foreach ($cmod as $ckey => $cvalue) {
    		$carray=explode("-", $cvalue);
    		$cfieldid = $carray[0];
    		$cfieldvalue = $carray[1];

    		$cquery='SELECT instanceid FROM {customfield_data} WHERE
    		fieldid = '.$cfieldid.' AND value = '.$cfieldvalue.'';
    		$cmodcourse=$DB->get_records_sql($cquery);
    		if(!empty($cmodcourse)){
    			foreach ($cmodcourse as $cmvalue) {
    				$cmodcoursearray[]=$cmvalue->instanceid;
    			}
    		}
    	}
    	if(!empty($cmodcoursearray)){
    		$temparray[]=$cmodcoursearray;
    	}else {
        return null;
      }
    }
    //cost type
    //delivery type
    $costarray=[];
    if(!empty($request_data['costid'])){
      $cost = $request_data['costid'];
      foreach ($cost as $ckey => $value) {
        $costidarray=explode("-", $value);
        $costid = $costidarray[0];
        $costvalue = $costidarray[1];

        $cstquery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$costid.' AND value = '.$costvalue.'';
        $costcourse=$DB->get_records_sql($cstquery);
        if(!empty($costcourse)){
          foreach ($costcourse as $cmvalue) {
            $costarray[]=$cmvalue->instanceid;
          }
        }
      }
      if(!empty($costarray)){
        $temparray[]=$costarray;
      }else {
        return null;
      }
    }
    //company type
    $company_name_array=[];
    if(!empty($request_data['cnid'])){
      $company = $request_data['cnid'];
      foreach ($company as $companykey => $value) {
        $companyidarray=explode("-", $value);
        $companyid = $companyidarray[0];
        $companyvalue = $companyidarray[1];

        $comquery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$companyid.' AND value = '.$companyvalue.'';
        $company_details=$DB->get_records_sql($comquery);
        if(!empty($company_details)){
          foreach ($company_details as $cmvalue) {
            $company_name_array[]=$cmvalue->instanceid;
          }
        }
      }
      if(!empty($company_name_array)){
        $temparray[]=$company_name_array;
      }else {
        return null;
      }
    }
    //certificate type
    $certificate_array=[];
    if(!empty($request_data['caid'])){
      $certificate = $request_data['caid'];
      foreach ($certificate as $certificatekey => $value) {
        $certificateidarray=explode("-", $value);
        $certificateid = $certificateidarray[0];
        $certificatevalue = $certificateidarray[1];


        $certificatequery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$certificateid.' AND value = '.$certificatevalue.'';
        $certificate_details=$DB->get_records_sql($certificatequery);
        if(!empty($certificate_details)){
          foreach ($certificate_details as $cmvalue) {
            $certificate_array[]=$cmvalue->instanceid;
          }
        }
      }
      if(!empty($certificate_array)){
        $temparray[]=$certificate_array;
      }else {
        return null;
      }
    }
//-------------------------------------------------------------------------
//get all the filter arrays.
$array=array();
// create array of arrays
$array[] = $searchcourses;
$array[] = $catcourses;
$array[] = $modcourses;
$array[] = $langcourses;
$array[] = $cmodcoursearray;
$array[] = $costarray;
$array[] = $company_name_array;
$array[] = $certificate_array;


//filter out empty array
$array = array_filter($array);
if(count($array) > 1){
  $lastarray=array_intersect(...$array);
}else{
  foreach ($array as $akey => $avalue) {
    $lastarray = $avalue;
  }
}
global $page;
return filter_pagination($lastarray,2,$page);
 
  
  }


function filter_pagination($array,$limit,$page){
global $DB,$CFG;
   $returnarray=[];



   
    $page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;

    $array = $array;
    $total = count( $array ); //total items in array    
    $limit = 6; //per page    
    $totalPages = ceil( $total/ $limit ); //calculate total pages

    $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
    $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
    $offset = ($page - 1) * $limit;
    if( $offset < 0 ){
      $offset = 0;
    } 

    $lastarray = array_slice($array,$offset,$limit);


    $previous_page = $page - 1;
    $next_page = $page + 1 ;


    
    $preiouspage_link =  $CFG->wwwroot.'/local/course_filter/'.basename($_SERVER['REQUEST_URI']).'&page='. $previous_page;
    $nextpage_link =  $CFG->wwwroot.'/local/course_filter/'.basename($_SERVER['REQUEST_URI']).'&page='. $next_page;

       ;


     $pagerContainer = ' <div class="row">';
        if( $totalPages != 0 ) 
        {
          $pagerContainer.='
                <div class="col-lg-12 col-md-12 col-sm-12">
                  
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                      <ul class="pagination p-center">';
                      if( $page == 1 ) 
                        { 
                          $pagerContainer .= ''; 
                        } 
                        else 
                        { 
                        $pagerContainer .= '<li class="page-item">
                          <a class="page-link" href="'.$preiouspage_link.'" aria-label="Previous">
                          <span class="ti-arrow-left"></span>
                          <span class="sr-only">Previous</span>
                          </a>
                        </li>';
                        }
                       
                        for($page1=1;$page1<=$totalPages;$page1++){
                       
                          $page_link = $CFG->wwwroot.'/local/course_filter/'.basename($_SERVER['REQUEST_URI']).'&page='. $page1;
                           $pagerContainer.='<li class="page-item"><a class="page-link" href="'.$page_link.'" >'.$page1.'</a></li>';
                          
                          }
                     
                         if($page == $totalPages ) 
                          { 
                            
                            $pagerContainer .= ''; 
                          }
                          else 
                          { 
                            
                            $pagerContainer.=' <li class="page-item">
                            <a class="page-link" href="'.$nextpage_link.'" aria-label="Next">
                            <span class="ti-arrow-right"></span>
                            <span class="sr-only">Next</span>
                            </a>
                          </li>';
                            
                          }      
                       
           $pagerContainer.='</ul>
                    </div>
                  </div>
                  
                </div>';
        }
      $pagerContainer.='</div>';
            

     foreach ($lastarray as $courseid) {
      $coursename=$DB->get_field('course','fullname',array('id'=>$courseid));
      //get the mode of engagement.
      $fieldid=$DB->get_field('customfield_field','id',array('shortname'=>'coursetype'));
      $fieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$fieldid,'instanceid'=>$courseid));
      $courseimage = filter_get_course_image($courseid);
      $customname='coursetype';
      $mode='';
      if(!empty($fieldvalue)){
        $valarray=filter_get_customfieldarray($customname);
        $mode=$valarray[$fieldvalue-1].'&nbsp;|&nbsp;';
      }
      //get the course mode.
      $cmfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'cost_type'));
      $cmfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$cmfieldid,'instanceid'=>$courseid));
      $coursemode='';
      if(!empty($cmfieldvalue)){
        $valarray=filter_get_customfieldarray('cost_type');
        $coursemode=$valarray[$cmfieldvalue-1];
      }
      //get the category name of the course.
      $coursecatid=$DB->get_field('course','category',array('id'=>$courseid));
      $coursecatname=$DB->get_field('course_categories','name',array('id'=>$coursecatid));
      //course language.
      $lgfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'language'));
      $lgfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$lgfieldid,'instanceid'=>$courseid));
      $courselang='';
      if(!empty($lgfieldvalue)){
        $valarray=filter_get_customfieldarray('language');
        $courselang=$valarray[$lgfieldvalue-1];
      }
      $courseduration='0.0hr';

        //get the delivery mode.
      $dtfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'delivery_type'));
      $dtfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$dtfieldid,'instanceid'=>$courseid));
      $deliverymode='';
      if(!empty($dtfieldvalue)){
        $valarray=filter_get_customfieldarray('delivery_type');
        $deliverymode=$valarray[$dtfieldvalue-1];
      }

       //get the Affliation name.
      $afnfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'affiliation_name'));
      $afnfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$afnfieldid,'instanceid'=>$courseid));
      $afname='';
      if(!empty($afnfieldvalue)){
        $valarray=filter_get_customfieldarray('affiliation_name');
        $afname=$valarray[$afnfieldvalue-1];
      }

      //Mihir do a check if the course is visible or not
      global $DB;
      $checkvisibility = $DB->get_record('course', array('id'=>$courseid));
      if (!empty($checkvisibility)) {
        if ($checkvisibility->visible == 1) {
          $returnarray[]=array('id'=>$courseid,
           'coursename'=>$coursename,
           // 'usercount'=>$enrolledusers,
           'mod_name'=>$mode,
           'total_duration'=>$courseduration,
           'courseimg'=>$courseimage,
           'coursemode'=>$coursemode,
           'coursecategory'=>$coursecatname,
            'courselang'=>$courselang,
          'pagination'=> $pagerContainer,
          'deliverymode'=> $deliverymode,
          'afname'=>$afname,
         'course_details_link'=>$CFG->wwwroot.'/local/course_info/course_details.php?cid='.$courseid);

        }
      }



    }

         return $returnarray;
}

  function filter_get_course_filter_list_old($request_data) {
    global $DB;
  //get all the courses from selected categories.
    $searchcourses=[];
    if(!empty($request_data['search'])){
      $searchkeyword = $request_data['search'];
      $searchsql="SELECT id from {course} WHERE fullname LIKE '%".$searchkeyword."%' OR shortname LIKE '%".$searchkeyword."%'";
      $searchedcourses=$DB->get_records_sql($searchsql);
      foreach ($searchedcourses as $svalue) {
        $searchcourses[]=$svalue->id;
      }
    }
    $catcourses=[];
    if(!empty($request_data['sectorid'])){
      foreach ($request_data['sectorid'] as $categoryid) {

        $sql="SELECT c.id FROM {course} c
        JOIN {course_categories} ct  ON ct.id=c.category
        WHERE c.category=$categoryid OR ct.path LIKE '%/".$categoryid."/%'";

        // $sql="SELECT c.id FROM {course} c
        // JOIN {course_categories} ct  ON ct.id=c.category
        // WHERE c.visible = 1 AND (c.category=$categoryid OR ct.path LIKE '%/".$categoryid."/%')";

        $course=$DB->get_records_sql($sql);
        foreach ($course as $cid) {
          $catcourses[] = $cid->id;
        }
      }
    }
    $modcourses=[];
    if(!empty($request_data['modid'])){

      $modes = $request_data['modid'];
      foreach ($modes as $mkey => $mvalue) {
        $marray=explode("-", $mvalue);
        $fieldid = $marray[0];
        $fieldvalue = $marray[1];

        $mquery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$fieldid.' AND value = '.$fieldvalue.'';
        $modcourse=$DB->get_records_sql($mquery);
        if(!empty($modcourse)){
          foreach ($modcourse as $mdvalue) {
            $modcourses[]=$mdvalue->instanceid;
          }
        }
      }
    }
    $langcourses=[];
    if(!empty($request_data['lang'])){

      $lang = $request_data['lang'];
      foreach ($lang as $lkey => $lvalue) {
        $larray=explode("-", $lvalue);
        $lfieldid = $larray[0];
        $lfieldvalue = $larray[1];

        $lquery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$lfieldid.' AND value = '.$lfieldvalue.'';
        $langcourse=$DB->get_records_sql($lquery);
        if(!empty($langcourse)){
          foreach ($langcourse as $lgvalue) {
            $langcourses[]=$lgvalue->instanceid;
          }
        }
      }
    }
    //course mode
    $cmodcoursearray=[];
    if(!empty($request_data['cmod'])){
      $cmod = $request_data['cmod'];
      foreach ($cmod as $ckey => $cvalue) {
        $carray=explode("-", $cvalue);
        $cfieldid = $carray[0];
        $cfieldvalue = $carray[1];

        $cquery='SELECT instanceid FROM {customfield_data} WHERE
        fieldid = '.$cfieldid.' AND value = '.$cfieldvalue.'';
        $cmodcourse=$DB->get_records_sql($cquery);
        if(!empty($cmodcourse)){
          foreach ($cmodcourse as $cmvalue) {
            $cmodcoursearray[]=$cmvalue->instanceid;
          }
        }
      }
    }


    $finalarray=[];
    $finalarray=array_unique(array_merge($searchcourses,$catcourses, $modcourses, $langcourses,$cmodcoursearray), SORT_REGULAR);
    $returnarray=[];
    foreach ($finalarray as $courseid) {
      $coursename=$DB->get_field('course','fullname',array('id'=>$courseid));
      $enrolledusers=count(filter_all_enrolled_usersdata($courseid));
      //get the mode of engagement.
      $fieldid=$DB->get_field('customfield_field','id',array('shortname'=>'coursetype'));
      $fieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$fieldid,'instanceid'=>$courseid));
      $courseimage = filter_get_course_image($courseid);
      $customname='coursetype';
      $mode='';
      if(!empty($fieldvalue)){
        $valarray=filter_get_customfieldarray($customname);
        $mode=$valarray[$fieldvalue-1].'&nbsp;|&nbsp;';
      }
      //get the course mode.
      $cmfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'coursemode'));
      $cmfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$cmfieldid,'instanceid'=>$courseid));
      $coursemode='';
      if(!empty($cmfieldvalue)){
        $valarray=filter_get_customfieldarray('coursemode');
        $coursemode= '&nbsp;|&nbsp;'.$valarray[$cmfieldvalue-1];
      }
      //get the category name of the course.
      $coursecatid=$DB->get_field('course','category',array('id'=>$courseid));
      $coursecatname=$DB->get_field('course_categories','name',array('id'=>$coursecatid));
      //course language.
      $lgfieldid=$DB->get_field('customfield_field','id',array('shortname'=>'cslanguage'));
      $lgfieldvalue=$DB->get_field('customfield_data','value',array('fieldid'=>$lgfieldid,'instanceid'=>$courseid));
      $courselang='';
      if(!empty($lgfieldvalue)){
        $valarray=filter_get_customfieldarray('cslanguage');
        $courselang=$valarray[$lgfieldvalue-1].'&nbsp;|&nbsp;';
      }
      $courseduration='0.0hr';


      //Mihir do a check if the course is visible or not
      global $DB,$CFG;
      $checkvisibility = $DB->get_record('course', array('id'=>$courseid));
      if (!empty($checkvisibility)) {
        if ($checkvisibility->visible == 1) {
          $returnarray[]=array('id'=>$courseid,
           'coursename'=>$coursename,
           'usercount'=>$enrolledusers,
           'mod_name'=>$mode,
           'total_duration'=>$courseduration,
           'courseimg'=>$courseimage,
           'coursemode'=>$coursemode,
           'coursecategory'=>$coursecatname,
            'courselang'=>$courselang,
         );

        }
      }


      // $returnarray[]=array('id'=>$courseid,
      //  'coursename'=>$coursename,
      //  'usercount'=>$enrolledusers,
      //  'mod_name'=>$mode,
      //  'total_duration'=>$courseduration,
      //  'courseimg'=>$courseimage,
      //  'coursemode'=>$coursemode,
      //  'coursecategory'=>$coursecatname,
      //   'courselang'=>$courselang);


    }
    return $returnarray;

  }
  // function filter_safe_params($array, $predicate) {
  //   return array_filter($array, $predicate);
  // }

  // function filter_all($array, $predicate) {
  //   return array_filter($array, $predicate) === $array;
  // }

  // function filter_get_encode($request_data) {
  // //return json_encode(get_course_filter_list($request_data));
  // }

  // function filter_render_page_content($request_data) {
  //   global $OUTPUT;
  //   $data_objects = get_course_filter_list($request_data);
  //   $data_array= [];
  //   foreach ($data_objects as $value) {
  //     $temp_data['id'] = $value->id;
  //     $temp_data['cid'] = $value->cid;
  //     $temp_data['sectorid'] = $value->sectorid;
  //     $temp_data['modid'] = $value->modid;
  //     $temp_data['total_duration'] = $value->total_duration;
  //     $temp_data['virtual_learning_duration'] = $value->virtual_learning_duration;
  //     $temp_data['self_learning_duration'] = $value->self_learning_duration;
  //     $temp_data['face_to_face_duration'] = $value->face_to_face_duration;
  //     $temp_data['course_nicename'] = $value->course_nicename;
  //     array_push($data_array, $temp_data);
  //   }
  //   $data['course_filter_list'] = $data_array;
  // // echo var_dump($data);
  // // exit;
  //   return $OUTPUT->render_from_template('local_course_filter/course_filter_list', $data);
  // }
