<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package
 * @copyright
 * @copyright
 * @license
**/
require_once('../../config.php');
defined('MOODLE_INTERNAL') || die();
global $CFG;
global $PAGE,$OUTPUT;
$context = context_system::instance();
$contextid = $context->contextlevel;
$title ="Trustee";
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_title($title);
$PAGE->set_url($CFG->wwwroot . '/local/contact/trustees.php');

echo $OUTPUT->header();
$html = "";
// $html  .= html_writer::start_tag('div',array('class'=>'container-fluid'));//container start//
// 	$html .=html_writer::start_tag('div',array('class'=>'row p-5'));//row start

$html .='<div class="about-area default-padding" style="margin-bottom: 50px;">
        <div class="container">
        <div class="row main-area">
          <div class="col-xs-12 col-md-8 about-info">
            <h2 style="text-align:left"><span> Our Trustees</span></h2>
            <div style="text-align:justify">
            <p class="text-justify p-4" style="line-height: 30px;">
          Dr. Avdhesh Prasad Singh <br>
             Dr. Devendra Pratap Singh <br>
          Dr. Murli Manohar Joshi <br>
             Dr. Anil Jain <br>
             Dr. Krishan Gopal ji <br>
             Shri Brahmdev Sharma "Bhai Ji" <br>
             Shri Om Prakash Ji Goyal <br>
           Shri Manoj Agarwal <br>
           Shri Rahul Singh <br>
             Shri Jitendra Agarwal <br>
             Shri Ram Awtar Kila <br>
            Shri Jaiprakash Agarwal <br>
           Shri Surya Kant Jalan <br>
            Shri Sanjay Garg <br>
          Shri Ashwani Kumar Gupta <br>
          Dr. Mahesh Sharma <br>
           Shri Ranjeev Tiwari <br>
           Shri O.P. Shrivastav <br>
          Shri Mohal Goel <br>
           Shri Kavish Jain <br>
      Shri Vijay Agarwal <br>
            </p>


</div>
  <a class="item-cta btn theme-btn-primary" href="http://bhaoraonyas.org/" target="_blank"><div class="text_to_html">Visit our website</div></a>
          </div>



        </div>
        </div>
    </div>';
$htmlxyz ='<section class="dcare__choose__us__area section-padding--lg bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">
							<h2 class="title__line">Little About Us</h2>
							<p>Service of downtrodden and exploited for centuries is basic to Nyas. Their emancipation through education, health and value inculcation by creating many teams of dedicated workers and centers. To inspire such workers to come forward from various streams of life especially from education, health technical fields and to prepare them for social service is the primary objective of the Nyas. Creation of Baharatiya culture and ethos based education and social institutes and helping the existing intitution working with similar aims is the mandate of Nyas. Trained and dedicated workers are expected to work in remote forest dwellings; inaccessible hilly regions and harsh deserts to help bring these centuries old uncared for masses to the national mainstream. </p>
						</div>
					</div>
				</div>
			</div>
		</section>';

    $htmlnnoo = '

            <div class="row mt--40">
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12">
    						<div class="dacre__choose__option">
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Language Lesson</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Meals Provided</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Transportation</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
    						<div class="dacre__choose__option">
    							<div class="choose__big__img">
    								<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/big-img/1.png" alt="choose images">
    							</div>
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12">
    						<div class="dacre__choose__option text__align--left">
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Professional Teacher</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Smart Education</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Toys &amp; Games</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    				</div>

            ';
  //   $html .=html_writer::end_tag('div');//row end
  // $html  .= html_writer::end_tag('div');//container start//



echo $html;

echo $OUTPUT->footer();
