<?php

/**

 */

require_once('../../config.php');
require_once('lib.php');
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/user_pages/profile.php');
$uid = required_param('id', PARAM_INT); //usrer id required for this page

global $CFG,$DB, $OUTPUT, $PAGE;
// $courseid = optional_param('cid', array(), PARAM_INT); //course id required for this page
$PAGE->requires->jquery();

echo $OUTPUT->header();
echo display_user_profile($uid);
echo $OUTPUT->footer();
