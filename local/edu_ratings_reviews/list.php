<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Handles uploading files
 *
 * @package    local_edu_ratings_reviews
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');

require_once('lib.php');
global $CFG;
global $PAGE,$OUTPUT;
require_login();
$context = context_system::instance();
$review_id = optional_param('review_id','',PARAM_INT);
$delete = optional_param('flag','',PARAM_INT);
$update = optional_param('update','',PARAM_INT);
$create = optional_param('create','',PARAM_INT);
$courseid = optional_param('cid','',PARAM_INT);
$local = get_string('local','local_edu_ratings_reviews');
$url = $CFG->wwwroot;
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($CFG->wwwroot .'/local/file_manager/list.php');
$title = get_string('manage_datas','local_edu_ratings_reviews');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$previewnode = $PAGE->navbar->add($local,$url);
$thingnode = $previewnode->add($title);
$thingnode->make_active();
echo $OUTPUT->header();
$list = list_datas($courseid);
if(!empty($delete))
{
	$id = $review_id;
	$delete = delete_review_data($id);
	$sucssmsg = get_string('messagedisplay','local_edu_ratings_reviews');
    echo $OUTPUT->notification($sucssmsg, 'notifysuccess');
    $list=list_datas($courseid);
    if(!empty($list))
    {
        $table= create_review_table($list,$courseid);
    }
    else
    {
        $prodismsg = get_string('promsgdisplay','local_edu_ratings_reviews');
        echo $OUTPUT->notification($prodismsg);
    }
}
else
{
	if(!empty($list))
	{
		$table = create_review_table($list,$courseid);
	}
	else
	{
		$prodismsg = get_string('promsgdisplay','local_edu_ratings_reviews');
        echo $OUTPUT->notification($prodismsg);
	}

}
if(!empty($create))
{
	$sucssmsg = get_string('createscss','local_edu_ratings_reviews');
	echo $OUTPUT->notification($sucssmsg,'notifysuccess');
}
if(!empty($update))
{
	$sucssmsg = get_string('upadatescss','local_edu_ratings_reviews');
	echo $OUTPUT->notification($sucssmsg,'notifysuccess');
}
global $DB,$USER;
$link =  new moodle_url('/local/edu_ratings_reviews/create_reviews.php',array('cid'=>$courseid));
$link1 =  new moodle_url('/local/edu_ratings_reviews/create_ratings.php',array('cid'=>$courseid));
$html = "";
$html .= html_writer::start_tag('a',array('role'=>'button','href'=>$link1,'style'=>'float:right;margin-left:5px;','class'=>'btn btn-primary'));
$html .='Add Rating';
$html .= html_writer::end_tag('a');
$html .= html_writer::start_tag('a',array('role'=>'button','href'=>$link,'style'=>'float:right;','class'=>'btn btn-primary'));
$html .='Add Review';
$html .= html_writer::end_tag('a');
echo $html;
echo "<br>";
echo "<br>";
echo "<br>";

if(!empty($table)){
	echo $table;
}
echo $OUTPUT->footer();
