<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Handles uploading files
 *
 * @package    local_bluetreesignup
 * @copyright  mallamma<mallamma@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('lib.php');
global $DB, $USER, $SESSION;  
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('login');
$PAGE->set_url($CFG->wwwroot . '/local/bluetreesignup/index.php');
$title = get_string('title','local_bluetreesignup');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/bluetreesignup/js/custom.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/bluetreesignup/js/createuser.js'), true);
echo $OUTPUT->header();
$setting = $PAGE->theme->setting_file_url('logo', 'logo');
$html ='';
$html.=html_writer::start_div('container');
$html.=html_writer::start_div('row');
$html.=html_writer::start_div('col-md-12');
$html.='
<div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
            <div class="text-center pb-2">
            <img style="width:20%" src="'.$setting.'">
            </div>
                <h2><strong>'.get_string('signuptitle','local_bluetreesignup').'</strong></h2>
                <p>'.get_string('fillallfield','local_bluetreesignup').'</p>
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>'.get_string('mobilenumber','local_bluetreesignup').'</strong></li>
                                <li id="personal"><strong>'.get_string('otpverify','local_bluetreesignup').'</strong></li>
                                <li id="payment"><strong>'.get_string('personalinfo','local_bluetreesignup').'</strong></li>
                                <li id="confirm"><strong>'.get_string('finish','local_bluetreesignup').'</strong></li> 
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">'.get_string('phonenumber','local_bluetreesignup').'</h2> <input type="text" name="phonenumber" placeholder="Enter Your Phone Number" required id="sign_phonenumber" onkeyup="check();" onkeypress="return isNumber(event)"/> 
                                </div> <input id="sign_next" type="button" name="next" class="next action-button" value="Next"  />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">'.get_string('enterotp','local_bluetreesignup').'</h2> 
                                    <input type="text" name="otp" placeholder="Enter Your OTP" id="sign_otp" onkeyup="ischeck();"/> 
                                </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input id="nextbutton" type="button" name="next" class="next action-button" value="Next" required />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Personal Information</h2>

                                    <label class="pay">'.get_string('username','local_bluetreesignup').'</label> <input  type="text" name="username" placeholder="Enter User Name" required id="sign_username"/ onkeyup="return forceLower(this);">
                                    <p>The password must have at least 8 characters, at least 1 digit(s), at least 1 lower case letter(s), at least 1 upper case letter(s), at least 1 non-alphanumeric character(s) such as as *, -, or #</p>
                                    <label class="pay">'.get_string('password','local_bluetreesignup').'</label> <input  type="password" name="password" placeholder="Enter Password" required id="sign_password" onblur ="passwordvalidation();" />
                                    <p style="color:red" id="passwordstatus"></p>
                                    <label class="pay">'.get_string('firstname','local_bluetreesignup').'</label> <input  type="text" name="firstname" placeholder="Enter First Name" required id="sign_firstname"/>
                                    <label class="pay">'.get_string('lastname','local_bluetreesignup').'</label> <input  type="text" name="lastname" placeholder="Enter Last Name" required id="sign_lastname"/>
                                    <label class="pay">'.get_string('email','local_bluetreesignup').'</label> <input  type="text" name="email" placeholder="Enter Your Email" required id="sign_email" onblur="validateemail();"/>
                                    <p style="color:red" id="emailstatus"></p>

                
                                </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" onclick="userCreate();" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card" id="ajaxresult">
                                    
                                </div>
                            </fieldset> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';

$html.=html_writer::end_div();
$html.=html_writer::end_div();
$html.=html_writer::end_div();
echo $html;

