<?php

/*
* Mallamma: This function will create the user from the bletreesignup page.
*@param $user is an user object from the ajax page.
*return true if the user is not created.
*/

function insert_new_user($user){
	global $DB,$CFG;
	require_once($CFG->dirroot.'/user/lib.php');
	require_once($CFG->dirroot.'/auth/email/auth.php');
	require_once($CFG->dirroot.'/lib/authlib.php');
	require_once($CFG->dirroot.'/lib/moodlelib.php');

	//check for the existance of email or username.
	$usernamecheck=$DB->record_exists('user', array('username'=>$user->username));
	$emailcheck=$DB->record_exists('user', array('email'=>$user->email));
	if($usernamecheck == 1 || $emailcheck == 1){
		return false;
	}else{
	//Here created object and push the data inside that object.
	$newuser = new stdclass();
	$newuser->username = $user->username;
	$newuser->password = hash_internal_user_password($user->password);
	$newuser->firstname = $user->firstname;
	$newuser->lastname = $user->lastname;
	$newuser->email = $user->email;
	$newuser->phone1 = $user->phonenumber;
	$newuser->confirmed   = 1;
    $newuser->lang        = current_language();
    $newuser->firstaccess = 0;
    $newuser->timecreated = time();
    $newuser->mnethostid  = $CFG->mnet_localhost_id;
    $newuser->secret      = random_string(15);
    $newuser->auth        = 'manual';
	$newid = user_create_user($newuser, false, false);
	//here getting userid.
	if($newid){
		//getting the last inserted user object.
		$userobject = $DB->get_record('user',array('id'=>$newid));
		complete_user_login($userobject);
		//getting admin details here..
		$sender = get_admin();
		//getting mail subject.
		$msgusersubject = get_string('subject','local_bluetreesignup');
		//here creating mail body.
		$messageuser = "Hi ".fullname($userobject).", 'Your Acoount is created Successfully'";
		$final = $messageuser;
		//this function is used to sending mail to user.
		email_to_user($userobject, $sender, $msgusersubject, $final, $messageuser,'','');
		return true;

	}
	}
}

