<?php
// This file keeps track of upgrades to
// the assignment module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installation to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the methods of database_manager class
//
// Please do not forget to use upgrade_set_timeout()
// before any action that may take longer time to finish.

defined('MOODLE_INTERNAL') || die();

 function xmldb_local_edu_ratings_reviews_upgrade($oldversion) {
     global $CFG,$DB;

    $dbman = $DB->get_manager();
   
    if ($oldversion < 2020060112) {

        $table = new xmldb_table('edupros_course_rating');
        $field1 = new xmldb_field('overall_ratings', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'courseid');
        $field2 = new xmldb_field('total_no_of_ratings', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'overall_ratings');
        
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        upgrade_plugin_savepoint(true, 2020060112,'local', 'local_edu_ratings_reviews');
    }



    return true;
}
