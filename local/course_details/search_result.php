<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_course_details
 * @copyright  Abhijit Sen<abhijitsen@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');
require_once('lib.php');    
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/course_details/search_result.php');
$title = get_string('searchcourse', 'local_course_details');
$PAGE->set_title($title);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/script.js'),true);

// $courseid = required_param('institution', PARAM_INT); //course id required for this page

// $course = $DB->get_record('course',array('id'=>$courseid)); //find the course details
//  below parameters comes from search bar
$subject = optional_param('subject','',PARAM_INT);//subject means tag of the course, here $subject is tag id 
$institution = optional_param('institution','',PARAM_INT);//institution means course category here this is the course catogery id
$course_text = optional_param('course_text','',PARAM_RAW);// this is the search text 

global $OUTPUT,$DB;

$param_arr = array(1);
$sql = 'SELECT c.id FROM {course} c ';

if (!empty($subject)) {
	$sql .= 'INNER JOIN {tag_instance} ti ON c.id=ti.itemid INNER JOIN {tag} t ON ti.tagid=t.id WHERE c.id!=1 && c.visible=? && t.id=?';
	array_push($param_arr, $subject);
}else{
	$sql .= 'WHERE c.id!=1 && c.visible=?';
}



if (!empty($institution)) {

	$sql .= ' && c.category=? ';

	array_push($param_arr, $institution);
	
}

if (!empty($course_text)) {

	$sql .= " && (c.fullname  LIKE '%".$course_text."%' || c.shortname  LIKE '%".$course_text."%' )";

}

$course_details = $DB->get_records_sql($sql, $param_arr);

$course_card = '';
if (!empty($course_details)) {
	foreach ($course_details as $key => $value) {
		$course_card .= get_course_card($value->id);  
	}
}else{
	$course_card .= html_writer::start_div('container', array());
	$course_card .= html_writer::start_div('alert alert-danger text-center', array());
	$course_card .= get_string('nomatchfound','local_course_details');
	$course_card .= html_writer::end_div();
	$course_card .= html_writer::end_div();
}




$html = html_writer::start_div('container-fluid pt-5 pb-5', array('id'=>'search_result_course'));
$html .= html_writer::start_div('container', array());
$html .= html_writer::start_div('row', array());
$html .= $course_card;
$html .= html_writer::end_div();
$html .= html_writer::end_div();
//Manju: For pagination.
$html.= html_writer::start_tag('nav',array('class'=>'pr-5 pt-4 pb-4'));
$html.= html_writer::start_tag('ul',array('class'=>'pagination justify-content-center pagination-sm'));
$html.= html_writer::end_tag('ul');
$html.= html_writer::end_tag('nav');
$html .= html_writer::end_div();

echo $OUTPUT->header(); 
echo search_box_section_display();
echo $html;
echo $OUTPUT->footer();
