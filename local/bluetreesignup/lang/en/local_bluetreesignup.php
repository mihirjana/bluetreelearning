<?php
$string['pluginname']='Blue Tree Signup';
$string['title']='Blue Tree Signup';
$string['signuptitle']='Sign Up Your User Account';
$string['fillallfield']='Fill all form field to go to next step';
$string['mobilenumber']='Mobile Number';
$string['otpverify']='OTP verification';
$string['personalinfo']='Personal Info';
$string['finish']='Finish';
$string['phonenumber']='Phone Number';
$string['enterotp']='Enter OTP';
$string['username']='User Name';
$string['password']='Password';
$string['firstname']='First Name';
$string['lastname']='Last Name';
$string['email']='Email';
$string['subject']='Welcome to';
$string['unabletocreate']='Unbale to Create Your Account Please Contact';
$string['useraccountcreated']='User Account Created Successfully';
$string['continue']='Continue';
$string['successfull']='Your Account is Created Successfully';
