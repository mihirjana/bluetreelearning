<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_course_details
 * @copyright  Abhijit Sen<abhijitsen@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');
require_once('lib.php');
// require_once($CFG->dirroot.'/local/other_pages/lib.php');
require_once('csslinks.php');
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$title = get_string('pluginname', 'local_course_details');
$PAGE->set_title($title);
global $DB,$USER;
$PAGE->requires->jquery();
// $PAGE->requires->jquery_plugin('ui');
// $PAGE->requires->jquery_plugin('ui-css');
require_once('jslink.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/course_details/script.js'));
// $PAGE->requires->jquery();
$courseid = required_param('cid', PARAM_INT); //course id required for this page
$enrolluser = optional_param('enrolluser', '', PARAM_RAW);

$course = $DB->get_record('course',array('id'=>$courseid)); //find the course details

if($enrolluser=='TRUE'){
	$PAGE->set_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$courseid,'enrolluser'=>'TRUE'));

	require_login();

	if (isloggedin()) {
		enroll_user_to_course($courseid,$USER->id);
	}
}else{
	$PAGE->set_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$courseid));
}

$coursedetails = '';
echo $OUTPUT->header();

if(!empty($course)){ // cheking is course exist
	echo course_details_page($courseid);
}else{ // if course not exist display error message

	$sucssmsg = get_string('coursedetailsnotfound','local_course_details');
	echo $pagecontaint = $OUTPUT->notification($errormsg);
}
echo $OUTPUT->footer();
