<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**

 */
function display_user_profile($uid){
	global $DB,$OUTPUT,$CFG,$PAGE,$USER;
	require_once("{$CFG->libdir}/completionlib.php");
    require_once ($CFG->dirroot . '/local/course_details/lib.php');

	$user_details = $DB->get_record('user',array('id'=>$uid));
	// print_object($user_details);
	if(!empty($user_details)){
		$course_teaching_arr = [];
		$course_completed_arr = [];
		$course_ongoing_arr = [];
		$user_picture= new user_picture($user_details);
		$src=$user_picture->get_url($PAGE);
		$user_img = $src;
		$courses = enrol_get_users_courses($uid,true,NULL);
		foreach ($courses as $cid => $course_value) {
			$course_object = $DB->get_record('course',array('id'=>$cid));
			$context = context_course::instance($cid);
			$roles = get_user_roles($context, $uid);
			foreach ($roles as $key => $value) {
				if ($value->shortname=='editingteacher' || $value->shortname=='teacher') {
					$course_teaching_arr[] = $cid;
				}else{
					$cinfo = new completion_info($course_object);
					$iscomplete = $cinfo->is_course_complete($uid);
					if ($iscomplete) {
					  $course_completed_arr[]=$cid;
					} else {
					  $course_ongoing_arr[]=$cid;					  
					}
				}
			}
			
		}

		// print_object($course_teaching_arr);
		// print_object($course_completed_arr);
		// print_object($course_ongoing_arr);
		// print_object($courses);
		$templatecontext = [
							'name'=>$user_details->firstname.' '.$user_details->lastname,
							'description'=>$user_details->description,
							'user_img'=>$user_img
						];
		if (isloggedin() && $USER->id==$uid) {
			$templatecontext['editprofile'] = 1;			
			$templatecontext['editprofile_link'] = new moodle_url($CFG->wwwroot . '/user/editadvanced.php',array('id'=>$uid));;			
		}
		if (!empty($user_details->department)) {
			$templatecontext['designation'] = $user_details->department;
		}
		if (!empty($user_details->yahoo)) {
			$templatecontext['facebooklink'] = $user_details->yahoo;
		}
		if (!empty($user_details->aim)) {
			$templatecontext['twitterlink'] = $user_details->aim;
		}
		if (!empty($user_details->msn)) {
			$templatecontext['linkedinlink'] = $user_details->msn;
		}
		if (!empty($course_teaching_arr)) {
			$course_teaching = '';
			foreach ($course_teaching_arr as $tcid) {
				$course_teaching .= display_course_card($tcid);
			}
			$templatecontext['course_teaching'] = $course_teaching;
		}

		if (!empty($course_completed_arr) && isloggedin()) {
			$course_completed = '';
			foreach ($course_completed_arr as $ccid) {
				$course_completed .= display_course_card($ccid);
			}
			$templatecontext['course_completed'] = $course_completed;
		}

		if (!empty($course_ongoing_arr) && isloggedin()) {
			$course_ongoing = '';
			foreach ($course_ongoing_arr as $ocid) {
				$course_ongoing .= display_course_card($ocid);
			}
			$templatecontext['course_ongoing'] = $course_ongoing;
		}

		return $OUTPUT->render_from_template('local_user_pages/profile', $templatecontext);
	}else{
		return false;
	}
	

}