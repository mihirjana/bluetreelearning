
function userCreate(){
	var username = $('#sign_username').val();
	var password = $('#sign_password').val();
	var firstname = $('#sign_firstname').val();
	var lastname = $('#sign_lastname').val();
	var email = $('#sign_email').val();
	var phonenumber = $('#sign_phonenumber').val();
	$.ajax({
    url: M.cfg.wwwroot+"/local/bluetreesignup/ajax.php",       
	data: {username:username, password:password, firstname:firstname, lastname:lastname, 
		email:email, phonenumber:phonenumber},     
	type: "POST",
        success:function(result){
        	$('#ajaxresult').html(result);
       }
     });
}


$( document ).ready(function() {
	//on the first loadof the page disable the next button.
	document.getElementById("sign_next").disabled = true;
	document.getElementById("nextbutton").disabled = true;
});
	//function to enable and disable the next button.
    function check(){
	//getting the phone number entered.
	var phonenumber = $('#sign_phonenumber').val();
	//checking the length of the number.
	if(phonenumber.length >= 10){
		//if the number is greater then 10 enable the next button
		document.getElementById("sign_next").disabled = false;
	}else{
		//if the number is less than 10 disable the button.
		document.getElementById("sign_next").disabled = true;
	}
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//function is used to check otp.
function ischeck(){
//here getting the value.
		var otp = $('#sign_otp').val();
		//here check the number is greater then or not.
		if(otp.length >=6){
			//here the number is greater then 6 enable the nextbutton.
			document.getElementById("nextbutton").disabled = false;

		}else{
			//here the number is less then 6 disable the nextbutton.
			document.getElementById("nextbutton").disabled = true;

		}

}


function passwordvalidation(){
	var str = document.getElementById("sign_password").value; 
	if (str.match(/[a-z]/g) && str.match( 
	        /[A-Z]/g) && str.match( 
	        /[0-9]/g) && str.match( 
	        /[^a-zA-Z\d]/g) && str.length >= 8){
		document.getElementById("passwordstatus").innerHTML = ""; 
	}else{
	    document.getElementById("passwordstatus").innerHTML = "Please check the password"; 
	}
	
}

function validateemail(){

    var email = document.getElementById('sign_email');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
    document.getElementById("emailstatus").innerHTML = "Please provide a valid email address"; 
    email.focus;
    return false;

	}else
	{
	    document.getElementById("emailstatus").innerHTML = ""; 
	}

}

function forceLower(strInput) 
{
strInput.value=strInput.value.toLowerCase();
}