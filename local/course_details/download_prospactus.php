<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_course_details
 * @copyright  Abhijit Sen<abhijitsen@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');
require_once('lib.php');    
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/course_details/download_prospactus.php');
$title = get_string('pluginname', 'local_course_details');
$PAGE->set_title($title);
global $DB,$PAGE,$OUTPUT,$CFG,$USER;
$rid = required_param('rid', PARAM_INT); //course id required for this page

$callback_data = $DB->get_record('request_callback',array('id'=>$rid,'status'=>1));

if (!empty($callback_data)) {
	$course_data = $DB->get_record('course',array('id'=>$callback_data->cid));
	if(!empty($course_data)){

		if ($USER->id!=0) {
	        $userid = $USER->id;
	        $context = context_course::instance($course_data->id);
	        $check_enrolled =  is_enrolled($context,$userid);
	        if(!empty($check_enrolled)){
	            $registerbutton_name = get_string('goto','local_course_details'); 
	            $registerredirect_url = new moodle_url ('/course/view.php',array('id'=>$course_data->id));
	        }else{
	            $registerbutton_name = get_string('buy','local_course_details');
	            $registerredirect_url = new moodle_url ('/local/edu_registration/index.php',array('succss'=>1,'step'=>4,'cid'=>$course_data->id,'userid'=>$userid));
	        }
	     }
	     else{
	       $registerredirect_url = new moodle_url($CFG->wwwroot .'/local/edu_registration/index.php',array('step'=>1,'cid'=>$course_data->id)); 
	        $registerbutton_name = get_string('register_now','local_course_details');

	     }


		$prospectus_link = new moodle_url($CFG->wwwroot . '/local/course_details/pdf/'.$course_data->id.'.pdf');
		$register_course_link = $registerredirect_url;
		$inst_details = $DB->get_record('local_course_details',array('catid'=>$course_data->category));
		$text = '"'.$course_data->fullname.'"  is a professional online course provided by '.$inst_details->catdisplay.'. Now you are one step closer to becoming part of the eduprosper community.<br>
			<b>ENROLL NOW FOR FREE!</b>';
		$templatecontext = [
								'cat_logo'=>course_cat_logo_cd($course_data->category),
								'course_name'=>$course_data->fullname,
								'text'=>$text,
								'prospectus_link'=>$prospectus_link,
								'register_course_link'=>$register_course_link,
								'registerbutton_name'=>$registerbutton_name
						   ];
		$page_body = $OUTPUT->render_from_template('local_course_details/download_prospactus', $templatecontext);
	}
	
} else {
	$errormsg = get_string('something_wrong','local_course_details');
	$page_body = $OUTPUT->notification($errormsg);
}

echo $OUTPUT->header();
// print_object(core_user::get_support_user()->email);
// print_object(get_admin());
// echo course_details_page_header($course_data->id); 
echo $page_body;
echo $OUTPUT->footer();