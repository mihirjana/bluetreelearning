<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_edu_ratings_reviews
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/reviews_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');

defined('MOODLE_INTERNAL') || die();
require_login();
$courseid = optional_param('cid','',PARAM_INT);
$context = context_course::instance($courseid);
$contextid = $context->contextlevel;
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($CFG->wwwroot . '/local/edu_ratings_reviews/create_reviews.php');
$title = get_string('create_reviews', 'local_edu_ratings_reviews');
$local = get_string('local','local_edu_ratings_reviews');
$url = $CFG->wwwroot;
$review_id = optional_param('review_id','',PARAM_INT);
$mform  = '';
global $DB;
if(!empty($review_id)){	
	$PAGE->set_title($title);
	$PAGE->set_heading($title);
	$previewnode = $PAGE->navbar->add($local,$url);
    $thingnode = $previewnode->add($title);
    $thingnode->make_active();
    $headingtext1 = get_string('edit_review_form','local_edu_ratings_reviews');
	$heading = get_local_plugin_heading($headingtext1,'','','');
	$mform  =  new local_reviews_form($CFG->wwwroot.'/local/edu_ratings_reviews/create_reviews.php',array('review_id'=>$review_id));
 }else{

	$PAGE->set_title($title);
	$PAGE->set_heading($title);
	$previewnode = $PAGE->navbar->add($local,$url);
    $thingnode = $previewnode->add($title);
    $thingnode->make_active();
	$headingtext1 = get_string('create_review_form','local_edu_ratings_reviews');
	$heading = get_local_plugin_heading($headingtext1,'','','');
	$mform  =  new local_reviews_form($CFG->wwwroot.'/local/edu_ratings_reviews/create_reviews.php');
}
$data = $mform->get_data();
$returnurl = new moodle_url('/local/edu_ratings_reviews/list.php',array('cid'=>$courseid));
if($mform->is_cancelled())
{
 	redirect($returnurl);
}
elseif($data){
	if(!empty($data->review_image)){
		$context = context_user::instance(2);
		$maxbytes = 5000000;
		$file_itemid = $data->review_image;
		file_save_draft_area_files($file_itemid,$context->id,'local_edu_ratings_reviews','review_image',$file_itemid,array('subdirs' => 0, 'maxbytes' =>$maxbytes, 'maxfiles' => 50));
	}
	if($data->editid == 0 || $data->editid == "")
	{ 
		global $CFG,$USER,$DB;
		$insert = insert_reviews_data($data);
		$list_url = new moodle_url ('/local/edu_ratings_reviews/list.php',array('create'=>1,'cid'=>$courseid));
		$redirect = redirect($list_url);
	}
	elseif(!empty($data->editid))
	{
		global $CFG,$USER,$DB;
		$update = update_reviews_data($data);
		$list_url = new moodle_url ('/local/edu_ratings_reviews/list.php',array('update'=>1,'cid'=>$courseid));
		$redirect = redirect($list_url);
	}
}
if(!empty($courseid)){
	$data = new stdClass();
	$data->cid = $courseid;
	$mform->set_data($data);
}
if(!empty($review_id)){
	$check_data = $DB->get_record('edupros_course_review',array('id'=>$review_id));
	$data = new stdClass();
	if(!empty($check_data->image)){
		$context = context_user::instance(2);
		$maxbytes = 5000000;
		$draftitemid = file_get_submitted_draft_itemid('review_image');
		file_prepare_draft_area($draftitemid, $context->id, 'local_edu_ratings_reviews', 'review_image', $check_data->image, array('subdirs' => 0, 'maxbytes' =>$maxbytes, 'maxfiles' => 50));
		$data->review_image = $draftitemid;
	}
	$data->name = $check_data->name_of_the_person;
	$data->title = $check_data->title;
	$data->review_text = $check_data->review_text;
	$data->review_rating = $check_data->review_rating;
	$data->editid = $check_data->id;
	$data->cid = $courseid;
	$mform->set_data($data);
}
echo $OUTPUT->header();

echo $heading;
echo '<hr>';
$mform->display();
echo $OUTPUT->footer();