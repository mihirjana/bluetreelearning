<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package
 * @copyright
 * @copyright
 * @license
**/
require_once('../../config.php');
require_once('lib.php');

defined('MOODLE_INTERNAL') || die();
global $CFG;
global $PAGE,$OUTPUT;
$context = context_system::instance();
$contextid = $context->contextlevel;
$title ="Refund Policy";
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_title($title);
$PAGE->set_url($CFG->wwwroot . '/local/contact/aboutus.php');

echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_contact/refund-policy',array());

echo $OUTPUT->footer();
