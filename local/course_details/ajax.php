<?php

require_once('../../config.php');
require_login();
global $DB;
$context = context_system::instance();
$contextid = $context->contextlevel;
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');

$userid = $_POST['userid'];
$cid = $_POST['cid'];
$type = $_POST['reg_type'];
if($type == 'online'){
	$msg = 'Online';
}elseif($type == 'offline'){
	$msg = 'Offline';
}else{
	$msg = 'E-learning';
}
//To store details in local_wishlist table
$insert_object = new stdClass();
$insert_object->userid = $userid;
$insert_object->course_id = $cid;
$insert_object->reg_data = $msg;
$insert_object->date = time();
$insert = $DB->insert_record('local_wishlist',$insert_object);
if($insert) {

		$user = $DB->get_record('user',array('id'=>$userid));
		$html = "";

		$html.='<div class="container">
					<div class="row">
						<div class="col-md-12">';
		$html.='<h3 class="text-center">"Thank you for showing interest in the course. Please fill up your registration deatils. In case of any query, please contact us at 8595887700"</h3>';


		$html.='</div>
					</div>
						</div>';

		$emailsubject = 'Wishlist information';
		$emailbody = $html;
		$admin_email = 'training@reskillingbharat.com';

		//mail to person
		$admin = get_admin();
		if(!empty($user)){
			 $sendemail = email_to_user($user, $admin, $emailsubject, html_to_text($emailbody));

		}
		//mail to email address
		if(!empty($admin)){
			 $sendemail = email_to_user($admin,$user, $emailsubject, html_to_text($emailbody));

		}

		if(!empty(core_user::get_support_user()->email)){
			$sender = core_user::get_support_user()->email;
		}else{
			$sender = get_admin();
		}
		if(!empty($admin_email)){
			 	$emailtouser = new \stdClass();
			    $emailtouser->id = -1;
			    $emailtouser->email = $admin_email;
			    $sendemail = email_to_user($emailtouser, $sender, $emailsubject, html_to_text($emailbody));


			}


		echo "succss";
	}
