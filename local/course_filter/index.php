<?php

/**

 */

require_once('../../config.php');
require_once('lib.php');
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/course_filter/index.php');

global $CFG,$DB, $OUTPUT, $PAGE;
$courseid = optional_param('cid', array(), PARAM_INT); //course id required for this page
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/contact/custom.js'));
$search = optional_param('search', array(), PARAM_RAW);
$sectorid = optional_param('stid', array(), PARAM_INT);
$modid = optional_param('modid', array(), PARAM_RAW);
$langid = optional_param('lang', array(), PARAM_RAW);
$costid = optional_param('costid', array(), PARAM_RAW);
$cnid = optional_param('cnid', array(), PARAM_RAW);//company name
$cmodid = optional_param('cmodid', array(), PARAM_RAW);//Delivery type
$caid = optional_param('caid', array(), PARAM_RAW);//Certificate available id
$onlysectorid = optional_param('onlysectorid', '', PARAM_INT);
$page = optional_param('page', '', PARAM_INT);
if (!empty($onlysectorid)) {
  $sectorid[99] = $onlysectorid;
}
$request_data = ['search'=>$search,'sectorid'=> $sectorid, 'modid' => $modid, 'lang'=>$langid,'costid'=>$costid,'cnid'=>$cnid,'cmodid'=>$cmodid,'caid'=>$caid,'page'=>$page];
  
if(!empty($page)){

  $new = filter_get_course_filter_list($request_data);
}

//$course = $DB->get_record('course',array('id'=>$courseid)); //find the course details
$form_url = new moodle_url($CFG->wwwroot.'/local/course_filter/index.php');
$time_icon = new moodle_url($CFG->wwwroot.'/local/course_filter/pics/icon1.png');
$banner_image = new moodle_url($CFG->wwwroot.'/local/course_filter/pics/banner.jpg');
//$title = $course->fullname;
//$PAGE->set_title($title);
$PAGE->set_title('Available Courses');

$data  = filter_get_course_filter_list($request_data);
if(!empty($data)){
  $data = $data;
}else{
  $data = $new;
}
$sector_data = filter_get_sectors();
$mod_data = filter_get_customfield_dropdown('level');
$languages = filter_get_customfield_dropdown('language');
$cmod = filter_get_customfield_dropdown('delivery_type');
$cost_data = filter_get_customfield_dropdown('cost_type');
$company_data = filter_get_customfield_dropdown('affiliation_name');
$certificate_data = filter_get_customfield_dropdown('certificate_available');
echo $OUTPUT->header();
?>
<style type="text/css">
  .form-control {
    height: 54px !important;
    border-radius: 4px !important;
    font-size: 15px !important;
    box-shadow: none !important;
    padding: .5rem .75rem !important;
    border: 1px solid #e6eaf3 !important;
        border-right-color: rgb(230, 234, 243) !important;
        border-right-style: solid !important;
        border-right-width: 1px !important;
    background-clip: initial !important;
}
</style>
<section class="page-title">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="breadcrumbs-wrap">
          <h1 class="breadcrumb-title">Courses Categories</h1>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Find Courses</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="pt-0">
  <div class="container">
    <div class="row">
            <div class="col-md-12 col-lg-12">
              <div id="filter-sidebar" class="filter-sidebar">
                <div class="filt-head">
                  <h4 class="filt-first">Advance Options</h4>
                  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">Close <i class="ti-close"></i></a>
                </div>
                <div class="show-hide-sidebar">
                  
                  <!-- Find New Property -->
                  <div class="sidebar-widgets">
                    
                     <form class="form-inline addons mb-3" action="" method="GET">
            <input class="form-control" name="search" type="search" placeholder="Search Courses" aria-label="Search">
            <button class="btn my-2 my-sm-0" type="submit"><i class="ti-search"></i></button>
          </form> 
         <form action="<?php echo $form_url;?>" method="GET">
            <!-- course categories start-->
            <h4 class="side_title">Course categories</h4>
            <ul class="no-ul-list mb-3">
              <?php $i = 1; ?>
              <?php foreach($sector_data as $row): ?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row->id, $sectorid))? 'checked' : '' ?>
                type="checkbox" value="<?php echo $row->id; ?>"
                name="stid[<?php echo $i; ?>]"
                >
                <label class="checkbox-custom-label" for="sector_id_<?php echo $i; ?>">
                  <?php echo $row->name; ?>
                </label>
              </li>
              <?php $i++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- course categories end-->
            <!-- level start-->
            <h4 class="side_title">Level</h4>
            <ul class="no-ul-list mb-3">
              <?php $j = 1; ?>
              <?php foreach($mod_data as $key => $row):?>
                <li>
                  <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $modid))? 'checked' : '' ?>
                  type="checkbox"
                  value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                  name="modid[<?php echo $j; ?>]"
               >
                  <label class="checkbox-custom-label" for="mod_id_<?php echo $j; ?>">
                    <?php echo $row['coursetypevalue']; ?>
                  </label>
                </li>
                <?php $j++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- level end-->
            <!-- language start-->
            <h4 class="side_title">Language</h4>
            <ul class="no-ul-list mb-3">
              <?php $k = 1; ?>
              <?php foreach($languages as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $langid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="lang[<?php echo $k; ?>]"
             >
                <label class="checkbox-custom-label" for="lang_id_<?php echo $k; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $k++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- language end-->
            <!-- cost type start-->
            <h4 class="side_title">Cost Type</h4>
            <ul class="no-ul-list mb-3">
              <?php $l = 1; ?>
              <?php foreach($cost_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $costid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="costid[<?php echo $l; ?>]"
              >
                <label class="checkbox-custom-label" for="cost_id_<?php echo $l; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $l++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- cost type end -->
            <!-- Related Company start -->
            <h4 class="side_title">Related Company Name</h4>
            <ul class="no-ul-list mb-3">
              <?php $m = 1; ?>
              <?php foreach($company_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $cnid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="cnid[<?php echo $m; ?>]"
             >
                <label class="checkbox-custom-label" for="cn_id<?php echo $m; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $m++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Related company end-->
            <!-- Delivery Type start -->
            <h4 class="side_title">Delivery Type</h4>
            <ul class="no-ul-list mb-3">
              <?php $o = 1; ?>
              <?php foreach($cmod as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $cmodid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="cmodid[<?php echo $o; ?>]"
             >
                <label class="checkbox-custom-label" for="cmod_id_<?php echo $o; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $o++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Delivery Type end -->
            <!-- Certificate Available start -->
            <h4 class="side_title">Certificate Available</h4>
            <ul class="no-ul-list mb-3">
              <?php $n = 1; ?>
              <?php foreach($certificate_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $caid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="caid[<?php echo $n; ?>]"
             >
                <label class="checkbox-custom-label" for="ca_id_<?php echo $n; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $n++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Certificate Available End -->
             <button type="submit" class="btn btn-primary custom-btn-padding orange-border-btn mt-2 text-uppercase btn-text-size inline-block">Search</button>
          </form>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
    <div class="row">
      <div class="col-lg-4 col-md-12 col-sm-12 order-2 order-lg-1 order-md-2">
        <div class="page_sidebar hide-23">
          <form class="form-inline addons mb-3" action="" method="GET">
            <input class="form-control" name="search" type="search" placeholder="Search Courses" aria-label="Search">
            <button class="btn my-2 my-sm-0" type="submit"><i class="ti-search"></i></button>
          </form> 
          <form action="<?php echo $form_url;?>" method="GET">
            <!-- course categories start-->
            <h4 class="side_title">Course categories</h4>
            <ul class="no-ul-list mb-3">
              <?php $i = 1; ?>
              <?php foreach($sector_data as $row): ?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row->id, $sectorid))? 'checked' : '' ?>
                type="checkbox" value="<?php echo $row->id; ?>"
                name="stid[<?php echo $i; ?>]"
                id="sector_id_<?php echo $i; ?>">
                <label class="checkbox-custom-label" for="sector_id_<?php echo $i; ?>">
                  <?php echo $row->name; ?>
                </label>
              </li>
              <?php $i++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- course categories end-->
            <!-- level start-->
            <h4 class="side_title">Level</h4>
            <ul class="no-ul-list mb-3">
              <?php $j = 1; ?>
              <?php foreach($mod_data as $key => $row):?>
                <li>
                  <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $modid))? 'checked' : '' ?>
                  type="checkbox"
                  value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                  name="modid[<?php echo $j; ?>]"
                  id="mod_id_<?php echo $j; ?>">
                  <label class="checkbox-custom-label" for="mod_id_<?php echo $j; ?>">
                    <?php echo $row['coursetypevalue']; ?>
                  </label>
                </li>
                <?php $j++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- level end-->
            <!-- language start-->
            <h4 class="side_title">Language</h4>
            <ul class="no-ul-list mb-3">
              <?php $k = 1; ?>
              <?php foreach($languages as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $langid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="lang[<?php echo $k; ?>]"
                id="lang_id_<?php echo $k; ?>">
                <label class="checkbox-custom-label" for="lang_id_<?php echo $k; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $k++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- language end-->
            <!-- cost type start-->
            <h4 class="side_title">Cost Type</h4>
            <ul class="no-ul-list mb-3">
              <?php $l = 1; ?>
              <?php foreach($cost_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $costid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="costid[<?php echo $l; ?>]"
                id="cost_id_<?php echo $l; ?>">
                <label class="checkbox-custom-label" for="cost_id_<?php echo $l; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $l++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- cost type end -->
            <!-- Related Company start -->
            <h4 class="side_title">Related Company Name</h4>
            <ul class="no-ul-list mb-3">
              <?php $m = 1; ?>
              <?php foreach($company_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $cnid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="cnid[<?php echo $m; ?>]"
                id="cn_id<?php echo $m ?>">
                <label class="checkbox-custom-label" for="cn_id<?php echo $m; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $m++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Related company end-->
            <!-- Delivery Type start -->
            <h4 class="side_title">Delivery Type</h4>
            <ul class="no-ul-list mb-3">
              <?php $o = 1; ?>
              <?php foreach($cmod as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $cmodid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="cmodid[<?php echo $o; ?>]"
                id="cmod_id_<?php echo $o; ?>">
                <label class="checkbox-custom-label" for="cmod_id_<?php echo $o; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $o++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Delivery Type end -->
            <!-- Certificate Available start -->
            <h4 class="side_title">Certificate Available</h4>
            <ul class="no-ul-list mb-3">
              <?php $n = 1; ?>
              <?php foreach($certificate_data as $key => $row):?>
              <li>
                <input class="checkbox-custom" <?php echo (in_array($row['fieldid'].'-'.$row['id'], $caid))? 'checked' : '' ?>
                type="checkbox"
                value="<?php echo $row['fieldid'].'-'.$row['id']; ?>"
                name="caid[<?php echo $n; ?>]"
                id="ca_id_<?php echo $n; ?>">
                <label class="checkbox-custom-label" for="ca_id_<?php echo $n; ?>">
                  <?php echo $row['coursetypevalue']; ?>
                </label>
              </li>
              <?php $n++; ?>
              <?php endforeach; ?>
            </ul>
            <!-- Certificate Available End -->
             <button type="submit" class="btn btn-primary custom-btn-padding orange-border-btn mt-2 text-uppercase btn-text-size inline-block">Search</button>
          </form>
        </div>
      </div>
      <div class="col-lg-8 col-md-12 col-sm-12 order-1 order-lg-2 order-md-1">

              <!-- Row -->
              <div class="row align-items-center mb-3">
                
                <div class="col-lg-6 col-md-6 col-sm-12 ordering">
                  <div class="filter_wraps">
                    <div class="dn db-991 mt30 mb0 show-23">
                      <div id="main2">
                        <a href="javascript:void(0)" class="btn btn-theme arrow-btn filter_open" onclick="openNav()" id="open2">Show Filter<span><i class="fas fa-arrow-alt-circle-right"></i></span></a>
                      </div>
                    </div>
                   
                  </div>
                </div>
              </div>
              <!-- /Row -->
        <div class="row">
          <?php foreach($data as $row): ?>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="education_block_grid style_2">
              <div class="education_block_thumb n-shadow">
                <a href="course-detail.html"><img src="<?php echo $row['courseimg']; ?>" style="width:100%" class="img-fluid" alt=""></a>
              </div>
              <div class="education_block_body">
                <h4 class="bl-title"><a href="<?php echo $row['course_details_link'] ?>"><?php echo $row['coursename']; ?></a></h4>
              </div>
              <div class="cources_facts">
                <ul class="cources_facts_list">
                  <li class="facts-1"><?php if(!empty($row['coursemode'])){ echo $row['coursemode'];}else{
                    echo 'Nil';
                  }?></li>
                  <li class="facts-2"><?php if(!empty($row['courselang'])){ echo $row['courselang'];}else{
                    echo 'Nil';
                  }?></li>
                   <li class="facts-5"><?php if(!empty($row['deliverymode'])){ echo $row['deliverymode'];}else{
                    echo 'Nil';
                  }?></li>
                  <li class="facts-5"><?php if(!empty($row['afname'])){ echo $row['afname'];}else{
                    echo 'Nil';
                  }?></li>
                  <!-- <li class="facts-2">Professional</li>
                  <li class="facts-5">Design</li> -->
                </ul>
              </div>
            </div>
          </div>
         
          <?php endforeach; ?>
         
        </div>
          <?php echo $data[0]['pagination'];?>
      </div>                  
    </div>
  </div>
</section>
<section class="newsletter theme-bg inverse-theme">
  <div class="container">
    <div class="text-center">
      <h2>Join Thousand of Happy Students!</h2>
      <p>Subscribe our newsletter & get latest news and updation!</p>
      
      <form id="contactus_form" class="sup-form">
        <div class="row ">
          <div class="col-md-4 col-sm-12">
            <input type="text" class="form-control w-75 sigmup-me" id="email" onblur="email_check();" placeholder="Email" name="email" >
            <span id="email_info"></span>
          </div>
          <div class="col-md-4 col-sm-12">
            <input type="text" class="form-control w-75 sigmup-me" placeholder="Name" id="name" onblur="name_check();" name="name">
             <span id="name_info" style="color:white;"></span>
          </div>
          <div class="col-md-4 col-sm-12">
            <input type="text" class="form-control w-75 sigmup-me" placeholder="Phone Number" name="phone_number">
            
          </div>
        </div>
       <div cass="row ">
            <input type="submit" class="btn btn-primary " name="submit" style="margin-top:55px;" value="submit">
            <span id="message_info" class="text-success"></span>
        </div>  
      </form>
    </div>
  </div>
</section>
<script>
      function openNav() {
       
        document.getElementById("filter-sidebar").style.width = "320px";
      }

      function closeNav() {
        document.getElementById("filter-sidebar").style.width = "0";
      }
    </script>
<?php
echo $OUTPUT->footer();
