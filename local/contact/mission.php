<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package
 * @copyright
 * @copyright
 * @license
**/
require_once('../../config.php');
defined('MOODLE_INTERNAL') || die();
global $CFG;
global $PAGE,$OUTPUT;
$context = context_system::instance();
$contextid = $context->contextlevel;
$title ="Our Mission";
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_title($title);
$PAGE->set_url($CFG->wwwroot . '/local/contact/aboutus.php');

echo $OUTPUT->header();
$html = "";
// $html  .= html_writer::start_tag('div',array('class'=>'container-fluid'));//container start//
// 	$html .=html_writer::start_tag('div',array('class'=>'row p-5'));//row start

$htmlabcd = '                        <div class="semester-apply">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 item">
                                    <h4>Covid 19 Situations</h4>
                                    <p>
                                        As we, all are aware that Covid 19 has affect the livelihood and employment of many of our brothers and sisters. Bhaorao Deoras Sewa Nyas, is now taking initiative to provide livelihood means to our brothers and sisters of Bharat through Skill Development trainings..
                                    </p>

                                </div>
                                <div class="col-md-6 col-sm-6 item">
                                    <h4></h4>
                                    <p>
                                        </p>
                                    <!-- <a class="btn btn-dark effect btn-sm" href="#"><i class="fa fa-angle-right"></i> Apply Now</a>  -->
                                </div>
                            </div>
                        </div>';

$html .='<section class="dcare__choose__us__area section-padding--lg bg--white" style="font-size: 18px;">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title ">
							<h2 class="title__line text-center missionhead">Our Mission</h2>
              <p>
              Our aim is to reach out to weaker and unprivileged persons of the society and to propagate aspirational values among youth.
              </p>
              <p class="text-justify">
              In our mission, we will rapidly expand skill development efforts in India by creating an end-to-end, outcome-focused implementation framework, which aligns the demands of the employers for a well-trained, skilled workforce with the aspirations of Indian citizens for sustainable livelihoods. Our website will create an end-to-end implementation framework for skill development, which provides opportunities for life-long learning and opportunities for quality long and short-term skill training that meets the aspirations of trainees. Outcome focused training will align employer/industry demand and workforce productivity with trainees’ for sustainable livelihoods. We will help the workers in organised and un-organised sectors to gain formal sector employment.
</p>
<p class="text-justify">
And, build capacity for skill development in organised and un-organized sectors, and provide pathways for re-skilling and up-skilling workers in these identified sectors, to enable them to transit into formal sector employment through a developed network of quality instructors. In turn, we will establish this ecosystem through high-quality teacher training institutions and leveraging existing public infrastructure and industry facilities.

</p>
                      </div>
					</div>
				</div>
			</div>
		</section>';

$html .='<div class="about-area default-padding-60 margin-60">
        <div class="container">
            <div class="row">

                    <div class="col-md-12 about-info">
                        <h2>Our <span> Objective</span></h2>
                        <blockquote style="border-left: 4px solid #e3000e;">
                            The main objective of Nyas is to provide 360-degree development of the economically and socially deprived class. Nyas achieves this by planning and conducting various services and programs through education, health, and enculturation centres. Furthermore, Nyas holds multiple social functions for mass awakening in individuals.  </blockquote>

                    </div>
            </div>

              <div class="row mt-4">

                    <div class="col-md-12 features about-info">

                        <h2>Our <span> Aim</span></h2>
                        <blockquote style="border-left: 4px solid #e3000e;">
                        As our Hon’ble Prime Minister said, “Corona crisis has also taught us the importance of local supply chains, local markets have stepped in to help us. Time has taught us that we need to start thinking about local and buying local. Many global brands once started locally. With the passage of time, and public demand and marketing, these brands became global. We need to be vocal about our local products by not only buying but also publicising them.”
Quoting from Vedas, PM explained the importance of self-reliance,” Sarvam Aatmam Vasham Sukham. That which is in your control gives you happiness. We have to move ahead with new energy. Only we can make India self-reliant.”
      </blockquote>

                    </div>

            </div>
        </div>
    </div>';

    $htmlnono2 = '                            <div class="equal-height col-md-6 col-sm-6" style="height: 200px;">
                                    <div class="item mariner">
                                        <a href="#">
                                            <div class="icon">
                                                <i class=" fa fa-user"></i>
                                            </div>
                                            <div class="info">
                                                <h2>12</h2>
                                                <h4>Expert faculty</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="equal-height col-md-6 col-sm-6" style="height: 200px;">
                                    <div class="item brilliantrose">
                                        <a href="#">
                                            <div class="icon">
                                                <i class=" fa fa-user"></i>
                                            </div>
                                            <div class="info">
                                                <h2>68</h2>
                                                <h4>Best Teachers</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="equal-height col-md-6 col-sm-6" style="height: 200px;">
                                    <div class="item casablanca">
                                        <a href="#">
                                            <div class="icon">
                                                <i class=" fa fa-user"></i>
                                            </div>
                                            <div class="info">
                                                <h2>120</h2>
                                                <h4>Online Courses</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="equal-height col-md-6 col-sm-6" style="height: 200px;">
                                    <div class="item malachite">
                                        <a href="#">
                                            <div class="icon">
                                                <i class=" fa fa-user"></i>
                                            </div>
                                            <div class="info">
                                                <h2>689</h2>
                                                <h4>Scholarship</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>';
    $htmlnnoo = '

            <div class="row mt--40">
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12">
    						<div class="dacre__choose__option">
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Language Lesson</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Meals Provided</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Transportation</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    							</div>
    							<!-- End Single Choose -->
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
    						<div class="dacre__choose__option">
    							<div class="choose__big__img">
    								<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/big-img/1.png" alt="choose images">
    							</div>
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    					<!-- Start Single Choose Option -->
    					<div class="col-lg-4 col-md-6 col-sm-12">
    						<div class="dacre__choose__option text__align--left">
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Professional Teacher</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Smart Education</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    							<!-- Start Single Choose -->
    							<div class="choose">
    								<div class="choose__icon">
    									<img src="https://d29u17ylf1ylz9.cloudfront.net/junior/images/choose/icon/1.png" alt="choose icon">
    								</div>
    								<div class="choose__inner">
    									<h4><a href="about-us.html">Toys &amp; Games</a></h4>
    									<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
    								</div>
    							</div>
    							<!-- End Single Choose -->
    						</div>
    					</div>
    					<!-- End Single Choose Option -->
    				</div>

            ';
  //   $html .=html_writer::end_tag('div');//row end
  // $html  .= html_writer::end_tag('div');//container start//



echo $html;

echo $OUTPUT->footer();
