<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Handles uploading files
 *
 * @package    local_contact
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page.
}
global $CFG,$USER;
/** Manoj
* Date : 11 JUNE 2020
*this function is used to dispay_team_section
*
**/
function display_team_section(){
	global $PAGE, $OUTPUT,$CFG;

	$array = maker_teacher_section_datas();

  //print_object($array);

	$section_string = "";


	foreach($array['teachers'] as $value){
		//if(!empty($value['hasteacher']) && !empty($value['teachername']) && !empty($value['hasteacher']) && !empty($value['teachermeta']) ){
		if(!empty($value['hasteacher']) && !empty($value['teachername']) && !empty($value['hasteacher'])) {
				$section_string .= "
									<div class='col-md-12 margin-60'>
										<div class='item-inner style='padding-right: 10px !important;'>
										<div class='row'>
											<div class='col-md-3'>
												<img class='img-fluid teamimage' src='".$value['teacherimage']."' style='height:100%; max-height: 350px;'' alt=''>
											</div>
											<div class='col-xs-12 col-md-9 p-4'>
												<h2>".$value['teachername']."</h2>
												<b>".$value['teachermeta']."</b>
												<div class='teacherbioteam'>".$value['hasteacher']."</div>
											</div>
										</div>
										</div>
									</div>
		                 		";
		}
	}


	$section_array = ['section'=>$section_string,'link'=>$CFG->wwwroot.'/local/contact/styles.css'];
	return $OUTPUT->render_from_template('local_contact/team_section',$section_array);
}

function maker_teacher_section_datas() {
        global $PAGE, $OUTPUT;

         $makertheme = \theme_config::load('skillsonline');

        $useteachers = $makertheme->settings->useteachers == 1;

        $teachersectiontitle = (empty($makertheme->settings->teachersectiontitle)) ? false : format_text($makertheme->settings->teachersectiontitle);

        $defaultimage = $OUTPUT->image_url('teacher-default', 'theme');

        //Teachers section CTA button
        $teachersbuttontext = (empty($makertheme->settings->teachersbuttontext)) ? false : format_text($makertheme->settings->teachersbuttontext);
        $teachersbuttonurl = (empty($makertheme->settings->teachersbuttonurl)) ? false : $makertheme->settings->teachersbuttonurl;
        $teachersbuttonurlopennew = (empty($makertheme->settings->teachersbuttonurlopennew)) ? false : $makertheme->settings->teachersbuttonurlopennew;


        //Teacher 1
        $teacher1content = (empty($makertheme->settings->teacher1content)) ? false : format_text($makertheme->settings->teacher1content,FORMAT_HTML);
        $teacher1image = (empty($makertheme->setting_file_url('teacher1image', 'teacher1image'))) ? false : $makertheme->setting_file_url('teacher1image', 'teacher1image');
        $teacher1name = (empty($makertheme->settings->teacher1name)) ? false : format_text($makertheme->settings->teacher1name);
        $teacher1meta = (empty($makertheme->settings->teacher1meta)) ? false : format_text($makertheme->settings->teacher1meta);

        //Teacher 2
        $teacher2content = (empty($makertheme->settings->teacher2content)) ? false : format_text($makertheme->settings->teacher2content,FORMAT_HTML);
        $teacher2image = (empty($makertheme->setting_file_url('teacher2image', 'teacher2image'))) ? false : $makertheme->setting_file_url('teacher2image', 'teacher2image');
        $teacher2name = (empty($makertheme->settings->teacher2name)) ? false : format_text($makertheme->settings->teacher2name);
        $teacher2meta = (empty($makertheme->settings->teacher2meta)) ? false : format_text($makertheme->settings->teacher2meta);

        //Teacher 3
        $teacher3content = (empty($makertheme->settings->teacher3content)) ? false : format_text($makertheme->settings->teacher3content,FORMAT_HTML);
        $teacher3image = (empty($makertheme->setting_file_url('teacher3image', 'teacher3image'))) ? false : $makertheme->setting_file_url('teacher3image', 'teacher3image');
        $teacher3name = (empty($makertheme->settings->teacher3name)) ? false : format_text($makertheme->settings->teacher3name);
        $teacher3meta = (empty($makertheme->settings->teacher3meta)) ? false : format_text($makertheme->settings->teacher3meta);

        //Teacher 4
        $teacher4content = (empty($makertheme->settings->teacher4content)) ? false : format_text($makertheme->settings->teacher4content,FORMAT_HTML);
        $teacher4image = (empty($makertheme->setting_file_url('teacher4image', 'teacher4image'))) ? false : $makertheme->setting_file_url('teacher4image', 'teacher4image');
        $teacher4name = (empty($makertheme->settings->teacher4name)) ? false : format_text($makertheme->settings->teacher4name);
        $teacher4meta = (empty($makertheme->settings->teacher4meta)) ? false : format_text($makertheme->settings->teacher4meta);

        //Teacher 5
        $teacher5content = (empty($makertheme->settings->teacher5content)) ? false : format_text($makertheme->settings->teacher5content,FORMAT_HTML);
        $teacher5image = (empty($makertheme->setting_file_url('teacher5image', 'teacher5image'))) ? false : $makertheme->setting_file_url('teacher5image', 'teacher5image');
        $teacher5name = (empty($makertheme->settings->teacher5name)) ? false : format_text($makertheme->settings->teacher5name);
        $teacher5meta = (empty($makertheme->settings->teacher5meta)) ? false : format_text($makertheme->settings->teacher5meta);

        //Teacher 6
        $teacher6content = (empty($makertheme->settings->teacher6content)) ? false : format_text($makertheme->settings->teacher6content,FORMAT_HTML);
        $teacher6image = (empty($makertheme->setting_file_url('teacher6image', 'teacher6image'))) ? false : $makertheme->setting_file_url('teacher6image', 'teacher6image');
        $teacher6name = (empty($makertheme->settings->teacher6name)) ? false : format_text($makertheme->settings->teacher6name);
        $teacher6meta = (empty($makertheme->settings->teacher6meta)) ? false : format_text($makertheme->settings->teacher6meta);

        //Teacher 7
        $teacher7content = (empty($makertheme->settings->teacher7content)) ? false : format_text($makertheme->settings->teacher7content,FORMAT_HTML);
        $teacher7image = (empty($makertheme->setting_file_url('teacher7image', 'teacher7image'))) ? false : $makertheme->setting_file_url('teacher7image', 'teacher7image');
        $teacher7name = (empty($makertheme->settings->teacher7name)) ? false : format_text($makertheme->settings->teacher7name);
        $teacher7meta = (empty($makertheme->settings->teacher7meta)) ? false : format_text($makertheme->settings->teacher7meta);

        //Teacher 8
        $teacher8content = (empty($makertheme->settings->teacher8content)) ? false : format_text($makertheme->settings->teacher8content,FORMAT_HTML);
        $teacher8image = (empty($makertheme->setting_file_url('teacher8image', 'teacher8image'))) ? false : $makertheme->setting_file_url('teacher8image', 'teacher8image');
        $teacher8name = (empty($makertheme->settings->teacher8name)) ? false : format_text($makertheme->settings->teacher8name);
        $teacher8meta = (empty($makertheme->settings->teacher8meta)) ? false : format_text($makertheme->settings->teacher8meta);

        //Teacher 9
        $teacher9content = (empty($makertheme->settings->teacher9content)) ? false : format_text($makertheme->settings->teacher9content,FORMAT_HTML);
        $teacher9image = (empty($makertheme->setting_file_url('teacher9image', 'teacher9image'))) ? false : $makertheme->setting_file_url('teacher9image', 'teacher9image');
        $teacher9name = (empty($makertheme->settings->teacher9name)) ? false : format_text($makertheme->settings->teacher9name);
        $teacher9meta = (empty($makertheme->settings->teacher9meta)) ? false : format_text($makertheme->settings->teacher9meta);

        //Teacher 10
        $teacher10content = (empty($makertheme->settings->teacher10content)) ? false : format_text($makertheme->settings->teacher10content,FORMAT_HTML);
        $teacher10image = (empty($makertheme->setting_file_url('teacher10image', 'teacher10image'))) ? false : $makertheme->setting_file_url('teacher10image', 'teacher10image');
        $teacher10name = (empty($makertheme->settings->teacher10name)) ? false : format_text($makertheme->settings->teacher10name);
        $teacher10meta = (empty($makertheme->settings->teacher10meta)) ? false : format_text($makertheme->settings->teacher10meta);

        //Teacher 11
        $teacher11content = (empty($makertheme->settings->teacher11content)) ? false : format_text($makertheme->settings->teacher11content,FORMAT_HTML);
        $teacher11image = (empty($makertheme->setting_file_url('teacher11image', 'teacher11image'))) ? false : $makertheme->setting_file_url('teacher11image', 'teacher11image');
        $teacher11name = (empty($makertheme->settings->teacher11name)) ? false : format_text($makertheme->settings->teacher11name);
        $teacher11meta = (empty($makertheme->settings->teacher11meta)) ? false : format_text($makertheme->settings->teacher11meta);

        //Teacher 12
        $teacher12content = (empty($makertheme->settings->teacher12content)) ? false : format_text($makertheme->settings->teacher12content,FORMAT_HTML);
        $teacher12image = (empty($makertheme->setting_file_url('teacher12image', 'teacher12image'))) ? false : $makertheme->setting_file_url('teacher12image', 'teacher12image');
        $teacher12name = (empty($makertheme->settings->teacher12name)) ? false : format_text($makertheme->settings->teacher12name);
        $teacher12meta = (empty($makertheme->settings->teacher12meta)) ? false : format_text($makertheme->settings->teacher12meta);

        //Teacher 13
        $teacher13content = (empty($makertheme->settings->teacher13content)) ? false : format_text($makertheme->settings->teacher13content,FORMAT_HTML);
        $teacher13image = (empty($makertheme->setting_file_url('teacher13image', 'teacher13image'))) ? false : $makertheme->setting_file_url('teacher13image', 'teacher13image');
        $teacher13name = (empty($makertheme->settings->teacher13name)) ? false : format_text($makertheme->settings->teacher13name);
        $teacher13meta = (empty($makertheme->settings->teacher13meta)) ? false : format_text($makertheme->settings->teacher13meta);

        //Teacher 14
        $teacher14content = (empty($makertheme->settings->teacher14content)) ? false : format_text($makertheme->settings->teacher14content,FORMAT_HTML);
        $teacher14image = (empty($makertheme->setting_file_url('teacher14image', 'teacher14image'))) ? false : $makertheme->setting_file_url('teacher14image', 'teacher14image');
        $teacher14name = (empty($makertheme->settings->teacher14name)) ? false : format_text($makertheme->settings->teacher14name);
        $teacher14meta = (empty($makertheme->settings->teacher14meta)) ? false : format_text($makertheme->settings->teacher14meta);

        //Teacher 15
        $teacher15content = (empty($makertheme->settings->teacher15content)) ? false : format_text($makertheme->settings->teacher15content,FORMAT_HTML);
        $teacher15image = (empty($makertheme->setting_file_url('teacher15image', 'teacher15image'))) ? false : $makertheme->setting_file_url('teacher15image', 'teacher15image');
        $teacher15name = (empty($makertheme->settings->teacher15name)) ? false : format_text($makertheme->settings->teacher15name);
        $teacher15meta = (empty($makertheme->settings->teacher15meta)) ? false : format_text($makertheme->settings->teacher15meta);

        //Teacher 16
        $teacher16content = (empty($makertheme->settings->teacher16content)) ? false : format_text($makertheme->settings->teacher16content,FORMAT_HTML);
        $teacher16image = (empty($makertheme->setting_file_url('teacher16image', 'teacher16image'))) ? false : $makertheme->setting_file_url('teacher16image', 'teacher16image');
        $teacher16name = (empty($makertheme->settings->teacher16name)) ? false : format_text($makertheme->settings->teacher16name);
        $teacher16meta = (empty($makertheme->settings->teacher16meta)) ? false : format_text($makertheme->settings->teacher16meta);

        //Teacher 17
        $teacher17content = (empty($makertheme->settings->teacher17content)) ? false : format_text($makertheme->settings->teacher17content,FORMAT_HTML);
        $teacher17image = (empty($makertheme->setting_file_url('teacher17image', 'teacher17image'))) ? false : $makertheme->setting_file_url('teacher17image', 'teacher17image');
        $teacher17name = (empty($makertheme->settings->teacher17name)) ? false : format_text($makertheme->settings->teacher17name);
        $teacher17meta = (empty($makertheme->settings->teacher17meta)) ? false : format_text($makertheme->settings->teacher17meta);

        //Teacher 18
        $teacher18content = (empty($makertheme->settings->teacher18content)) ? false : format_text($makertheme->settings->teacher18content,FORMAT_HTML);
        $teacher18image = (empty($makertheme->setting_file_url('teacher18image', 'teacher18image'))) ? false : $makertheme->setting_file_url('teacher18image', 'teacher18image');
        $teacher18name = (empty($makertheme->settings->teacher18name)) ? false : format_text($makertheme->settings->teacher18name);
        $teacher18meta = (empty($makertheme->settings->teacher18meta)) ? false : format_text($makertheme->settings->teacher18meta);

        //Teacher 19
        $teacher19content = (empty($makertheme->settings->teacher19content)) ? false : format_text($makertheme->settings->teacher19content,FORMAT_HTML);
        $teacher19image = (empty($makertheme->setting_file_url('teacher19image', 'teacher19image'))) ? false : $makertheme->setting_file_url('teacher19image', 'teacher19image');
        $teacher19name = (empty($makertheme->settings->teacher19name)) ? false : format_text($makertheme->settings->teacher19name);
        $teacher19meta = (empty($makertheme->settings->teacher19meta)) ? false : format_text($makertheme->settings->teacher19meta);

        //Teacher 20
        $teacher20content = (empty($makertheme->settings->teacher20content)) ? false : format_text($makertheme->settings->teacher20content,FORMAT_HTML);
        $teacher20image = (empty($makertheme->setting_file_url('teacher20image', 'teacher20image'))) ? false : $makertheme->setting_file_url('teacher20image', 'teacher20image');
        $teacher20name = (empty($makertheme->settings->teacher20name)) ? false : format_text($makertheme->settings->teacher20name);
        $teacher20meta = (empty($makertheme->settings->teacher20meta)) ? false : format_text($makertheme->settings->teacher20meta);

        $fp_teachers = [

        'useteachers' => $useteachers,
        'teachersectiontitle' => $teachersectiontitle,
        'defaultimage' => $defaultimage,

        'teachersbuttontext' => $teachersbuttontext,
        'teachersbuttonurl' => $teachersbuttonurl,
        'teachersbuttonurlopennew' => $teachersbuttonurlopennew,



        'teachers' => array(

            array(
                'hasteacher' => $teacher1content,
                'teacherbio' => $teacher1content,
                'teacherimage' => $teacher1image,
                'teachername' => $teacher1name,
                'teachermeta' => $teacher1meta,
            ),

            array(
                'hasteacher' => $teacher2content,
                'teacherbio' => $teacher2content,
                'teacherimage' => $teacher2image,
                'teachername' => $teacher2name,
                'teachermeta' => $teacher2meta,
            ),

            array(
                'hasteacher' => $teacher3content,
                'teacherbio' => $teacher3content,
                'teacherimage' => $teacher3image,
                'teachername' => $teacher3name,
                'teachermeta' => $teacher3meta,
            ),

            array(
                'hasteacher' => $teacher4content,
                'teacherbio' => $teacher4content,
                'teacherimage' => $teacher4image,
                'teachername' => $teacher4name,
                'teachermeta' => $teacher4meta,
            ),

            array(
                'hasteacher' => $teacher5content,
                'teacherbio' => $teacher5content,
                'teacherimage' => $teacher5image,
                'teachername' => $teacher5name,
                'teachermeta' => $teacher5meta,
            ),

            array(
                'hasteacher' => $teacher6content,
                'teacherbio' => $teacher6content,
                'teacherimage' => $teacher6image,
                'teachername' => $teacher6name,
                'teachermeta' => $teacher6meta,
            ),

             array(
                'hasteacher' => $teacher7content,
                'teacherbio' => $teacher7content,
                'teacherimage' => $teacher7image,
                'teachername' => $teacher7name,
                'teachermeta' => $teacher7meta,
            ),

            array(
                'hasteacher' => $teacher8content,
                'teacherbio' => $teacher8content,
                'teacherimage' => $teacher8image,
                'teachername' => $teacher8name,
                'teachermeta' => $teacher8meta,
            ),

            array(
                'hasteacher' => $teacher9content,
                'teacherbio' => $teacher9content,
                'teacherimage' => $teacher9image,
                'teachername' => $teacher9name,
                'teachermeta' => $teacher9meta,
            ),

            array(
                'hasteacher' => $teacher10content,
                'teacherbio' => $teacher10content,
                'teacherimage' => $teacher10image,
                'teachername' => $teacher10name,
                'teachermeta' => $teacher10meta,
            ),

            array(
                'hasteacher' => $teacher11content,
                'teacherbio' => $teacher11content,
                'teacherimage' => $teacher11image,
                'teachername' => $teacher11name,
                'teachermeta' => $teacher11meta,
            ),

            array(
                'hasteacher' => $teacher12content,
                'teacherbio' => $teacher12content,
                'teacherimage' => $teacher12image,
                'teachername' => $teacher12name,
                'teachermeta' => $teacher12meta,
            ),

            array(
                'hasteacher' => $teacher13content,
                'teacherbio' => $teacher13content,
                'teacherimage' => $teacher13image,
                'teachername' => $teacher13name,
                'teachermeta' => $teacher13meta,
            ),

            array(
                'hasteacher' => $teacher14content,
                'teacherbio' => $teacher14content,
                'teacherimage' => $teacher14image,
                'teachername' => $teacher14name,
                'teachermeta' => $teacher14meta,
            ),

            array(
                'hasteacher' => $teacher15content,
                'teacherbio' => $teacher15content,
                'teacherimage' => $teacher15image,
                'teachername' => $teacher15name,
                'teachermeta' => $teacher15meta,
            ),

            array(
                'hasteacher' => $teacher16content,
                'teacherbio' => $teacher16content,
                'teacherimage' => $teacher16image,
                'teachername' => $teacher16name,
                'teachermeta' => $teacher16meta,
            ),

            array(
                'hasteacher' => $teacher17content,
                'teacherbio' => $teacher17content,
                'teacherimage' => $teacher17image,
                'teachername' => $teacher17name,
                'teachermeta' => $teacher17meta,
            ),

            array(
                'hasteacher' => $teacher18content,
                'teacherbio' => $teacher18content,
                'teacherimage' => $teacher18image,
                'teachername' => $teacher18name,
                'teachermeta' => $teacher18meta,
            ),

            array(
                'hasteacher' => $teacher19content,
                'teacherbio' => $teacher19content,
                'teacherimage' => $teacher19image,
                'teachername' => $teacher19name,
                'teachermeta' => $teacher19meta,
            ),

            array(
                'hasteacher' => $teacher20content,
                'teacherbio' => $teacher20content,
                'teacherimage' => $teacher20image,
                'teachername' => $teacher20name,
                'teachermeta' => $teacher20meta,
            ),


        ),

        ];


	return $fp_teachers;
 }
