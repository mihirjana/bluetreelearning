<?php
require_once('../../config.php');
require_once('lib.php');
global $DB, $USER, $SESSION;  
$context = context_system::instance();
$PAGE->set_context($context);
//Mallamma: Getting all the values from the url.
$username = optional_param('username','',PARAM_RAW);
$password = optional_param('password','',PARAM_RAW);
$firstname = optional_param('firstname','',PARAM_RAW);
$lastname = optional_param('lastname','',PARAM_RAW);
$email = optional_param('email','',PARAM_RAW);
$phonenumber = optional_param('phonenumber','',PARAM_RAW);
//Mallamma: creating an user object from all the values.
$user = new stdclass();
$user->username =$username;
$user->password =$password;
$user->firstname =$firstname;
$user->lastname =$lastname;
$user->email =$email;
$user->phonenumber=$phonenumber;
$result = insert_new_user($user);
$redirectlink = $CFG->wwwroot.'/my/';
$html='';
$html.=html_writer::start_div('row justify-content-center');
$html.=html_writer::start_div('col-12 text-center');
$html.=html_writer::start_tag('img',array('src'=>'https://img.icons8.com/color/96/000000/ok--v2.png'));
$html.=html_writer::start_div();
$html.=html_writer::start_div();
$html.=html_writer::start_div('row justify-content-center');
$html.=html_writer::start_div('col-12 text-center');
$html.=html_writer::start_tag('h2');
$html.=get_string('successfull','local_bluetreesignup');
$html.=html_writer::end_tag('h2');
$html.=html_writer::start_tag('a',array('href'=>$redirectlink));
$html.=get_string('continue','local_bluetreesignup');
$html.=html_writer::end_tag('a');
$html.=html_writer::start_div();
$html.=html_writer::start_div();
if($result == true){
	echo $html;
}else if($result == false){
$indexpage = $CFG->wwwroot.'/local/bluetreesignup/index.php';
$data='';
$data.=html_writer::start_div('row justify-content-center');
$data.=html_writer::start_div('col-12 text-center');
$data.=html_writer::start_tag('img',array('src'=>'https://img.icons8.com/color/48/000000/poor-quality--v1.png'));
$data.=html_writer::start_div();
$data.=html_writer::start_div();
$data.=html_writer::start_div('row justify-content-center');
$data.=html_writer::start_div('col-12 text-center');
$data.=html_writer::start_tag('h2');
$data.=get_string('unabletocreate','local_bluetreesignup');
$data.=html_writer::end_tag('h2');
$data.=html_writer::start_tag('a',array('href'=>$indexpage));
$data.=get_string('continue','local_bluetreesignup');
$data.=html_writer::end_tag('a');
$data.=html_writer::start_div();
$data.=html_writer::start_div();
echo $data;
}




