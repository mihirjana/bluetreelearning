<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_edu_ratings_reviews
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');
require_once('form/ratings_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once ('lib.php');
// require_once($CFG->dirroot.'/course/lib.php');
// require_once($CFG->libdir.'/coursecatlib.php');
defined('MOODLE_INTERNAL') || die();
require_login();
$courseid = required_param('cid', PARAM_INT); //course id required for this page
$flag = optional_param('flag','',PARAM_ALPHANUM);

// $courseid = optional_param('cid','',PARAM_INT);
$context = context_course::instance($courseid);
$contextid = $context->contextlevel;
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($CFG->wwwroot . '/local/edu_ratings_reviews/create_ratings.php');
$title1 = get_string('create_ratings', 'local_edu_ratings_reviews');
$rating_id = optional_param('rating_id','',PARAM_INT);
$mform  = '';
$returnurl = new moodle_url('/local/organization/create_ratings.php');
global $DB,$OUTPUT;
// display the heading of page
$PAGE->set_title($title1);
$PAGE->set_heading($title1);
$PAGE->navbar->ignore_active();
$previewnode = $PAGE->navbar->add($title1);
$headingtext = get_string('pluginheading1','local_edu_ratings_reviews');
$heading = get_local_plugin_heading($headingtext,'','','');
$mform = new local_ratings_form($CFG->wwwroot .'/local/edu_ratings_reviews/create_ratings.php?cid='.$courseid);
$data = $mform->get_data();
$flag = '';



$returndashbordurl = new moodle_url('/local/edu_ratings_reviews/list.php',array('cid'=>$courseid));
if ($mform->is_cancelled()){
    // click on cancle button then page redirect to home page...
    redirect($returndashbordurl);
}else if($data){
    global $DB,$USER,$CFG;
    $data->cid = $courseid;

    if(isset($data->id)){
		$update_datas = update_ratings_data($data);
	}else{
		$insert_datas = insert_ratings_data($data);

	}

	if($update_datas || $insert_datas){
		$flag = 'SUCCESS';
	}else{
		$flag = 'UNSUCCESS';	
	}

	$url = new moodle_url ('/local/edu_ratings_reviews/create_ratings.php',array('cid'=>$courseid,'flag'=>$flag));
	$redirect = redirect($url);
 //    $insert_datas = insert_ratings_data($data);
 //   	$list_url = new moodle_url ('/local/edu_ratings_reviews/list.php',array('create'=>1,'cid'=>$courseid));
	// $redirect = redirect($list_url);
}
$ratings_data = $DB->get_record('edupros_course_rating', array('courseid'=>$courseid));

if(!empty($ratings_data)){

	$check_data = new stdClass();
	
	$check_data->overall_ratings = $ratings_data->overall_ratings;
	$check_data->total_no_of_ratings = $ratings_data->total_no_of_ratings;
	$check_data->five_star = $ratings_data->five_star;
	$check_data->four_star = $ratings_data->four_star;
	$check_data->three_star = $ratings_data->three_star;
	$check_data->two_star = $ratings_data->two_star;
	$check_data->one_star = $ratings_data->one_star;
	$mform->set_data($check_data);
}


if(!empty($ratings_data)){
	$data = new stdClass();
	$data->cid = $courseid;
	$mform->set_data($data);
}
echo $OUTPUT->header();
echo $heading;
echo '<hr>';
if($flag){
	if($flag=='SUCCESS'){
		
		$sucssmsg = get_string('data_save_success_msg','local_course_details');
	    echo $OUTPUT->notification($sucssmsg, 'notifysuccess');
	}else if($flag=='UNSUCCESS'){
		$sucssmsg = get_string('data_not_save_success_msg','local_course_details');
	    echo $OUTPUT->notification($sucssmsg);
	}
}
$mform->display();
echo $OUTPUT->footer();