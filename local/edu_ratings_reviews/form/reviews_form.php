<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_edu_ratings_reviews
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2019 or later
 */

if (!defined('MOODLE_INTERNAL')) 
{
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');
?>

<?php

class local_reviews_form extends moodleform
{
	public function definition()
	{
		global $CFG,$OUTPUT,$DB,$USER,$PAGE;
		$context = context_system::instance();
		//this array is used for file manager//
		$filemanageropts = array('subdirs' => 0, 'maxbytes' =>0, 'maxfiles' => 50);
		//this array is used for file manager//
		$mform = $this->_form;
		$editid = $this->_customdata['review_id'];
		$attributes = array('size'=>'40');
		
		//review_image start here//
		$mform->addElement('filemanager','review_image',get_string('review_image','local_edu_ratings_reviews'),null,$filemanageropts);
		$mform->addRule('review_image',null,'required',null,'client');
		//review_image end here//


		//name of person field start here//
		$mform->addElement('text','name',get_string('name','local_edu_ratings_reviews'));
		$mform->settype('name', PARAM_RAW);
		// $mform->addRule('file_name',null,'required',null,'client');
		//name of person field end here//

		//title field start here//
		$mform->addElement('text','title',get_string('title','local_edu_ratings_reviews'));
		$mform->settype('title', PARAM_RAW);
		// $mform->addRule('file_name',null,'required',null,'client');
		//title field end here//

		//review text field start here//
		$mform->addElement('textarea','review_text',get_string('review_text','local_edu_ratings_reviews'),'wrap="virtual" rows="5" cols="40" ');
		$mform->settype('review_text', PARAM_RAW);
		// $mform->addRule('file_name',null,'required',null,'client');
		//review text field end here//

		//review rating field start here//

		$review_rating_options = [
										5 => get_string('five_star','local_edu_ratings_reviews'),
										4 => get_string('four_star','local_edu_ratings_reviews'),
										3 => get_string('three_star','local_edu_ratings_reviews'),
										2 => get_string('two_star','local_edu_ratings_reviews'),
										1 => get_string('one_star','local_edu_ratings_reviews')
									];
        $select = $mform->addElement('select', 'review_rating', get_string('review_rating','local_edu_ratings_reviews'), $review_rating_options);

        
		// $mform->addElement('text','review_rating',get_string('review_rating','local_edu_ratings_reviews'));
		// $mform->settype('review_rating', PARAM_RAW);
		// $mform->addRule('file_name',null,'required',null,'client');
		//review rating field end here//

		$mform->addElement('hidden','editid');
		$mform->settype('editid', PARAM_INT);

		$mform->addElement('hidden','cid');
		$mform->settype('cid', PARAM_INT);




		//review text field end here//

		//action buttons start here//
		$buttonarray = array();
		$buttonarray[] = $mform->createElement('submit','submitbutton',get_string('savebutton','local_edu_ratings_reviews'));
		$buttonarray[] = $mform->createElement('cancel');

		$mform->addGroup($buttonarray,'buttonarray','','',false);
		//action buttons end here//
	}

	public function validation($data,$files)
	{	
	// 	global $DB;
	// 	$errors = [];
	// 	if($data['folder_type'] == 0){
	// 		$errors['folder_type'] = get_string('error_folder_type','local_file_manager');
	// 	}
	// 	if($data['status'] == 0){
	// 		$errors['status'] = get_string('error_status','local_file_manager');
	// 	}
	// 	return $errors;
	}

}

?>
