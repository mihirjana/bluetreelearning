<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Handles uploading files
 *
 * @package    local_edu_registration
 * @author  	Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2020 or later
 */
/**
* for dispalying country flag in input box here 
*/

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/intlTelInput.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/jquery-3.5.1.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/jquery.dataTables.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/dataTables.buttons.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/buttons.flash.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/jszip.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/pdfmake.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/vfs_fonts.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/buttons.html5.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/buttons.print.min.js'), true);


$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/jquery-ui.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/script.js'), true);

// $PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_details/js/stripe.js'));




