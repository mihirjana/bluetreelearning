<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_course_details
 * @copyright  Abhijit Sen<abhijitsen@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page.
}

// Function for disply the search box section
function search_box_section_display(){
    global $PAGE,$CFG,$OUTPUT,$DB;
    $use_search_box_section = $PAGE->theme->settings->showsearchbar== 1;

    $searchbar_heading = (empty($PAGE->theme->settings->searchbar_heading)) ? false : format_text($PAGE->theme->settings->searchbar_heading);


    $searchbar_subheading = (empty($PAGE->theme->settings->searchbar_subheading)) ? false : format_text($PAGE->theme->settings->searchbar_subheading);


    $searchborbgimage = (empty($PAGE->theme->setting_file_url('searchborbgimage', 'searchborbgimage'))) ? false : $PAGE->theme->setting_file_url('searchborbgimage', 'searchborbgimage');

    $html = html_writer::start_div('course-header-image', array(
            'style' => 'background-image: url("' . $searchborbgimage . '"); -webkit-background-size:cover; -moz-background-size:cover; -o-background-size:cover; background-size:cover; background-repeat: no-repeat;  background-position: center; width:100%; height: 100%;'
        ));
    $html .= html_writer::end_div();

    $sql = 'SELECT cs.id, lcd.catdisplay FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=?';
    $institutions_details = $DB->get_records_sql($sql, array(0,1));

    $inst_arr = array();
    foreach ($institutions_details as $key => $value) {
       $ins_arr = [
                    'catid'=>$value->id,
                    'catname'=>$value->catdisplay,
                  ];
        array_push($inst_arr, $ins_arr);
    }

    $sql1 = 'SELECT DISTINCT t.id,t.name FROM {tag} t
            INNER JOIN {tag_instance} ti ON ti.tagid=t.id
            WHERE itemtype=?';

    $tag_details = $DB->get_records_sql($sql1, array('course'));

    $tag_arr = array();
    foreach ($tag_details as $key1 => $value1) {
       $t_arr = [
                    'tid'=>$value1->id,
                    'tname'=>$value1->name,
                  ];
        array_push($tag_arr, $t_arr);
    }


    $templatecontext = [

        'use_search_box_section' => $use_search_box_section,
        'searchbar_heading' => $searchbar_heading,
        'searchbar_subheading' => $searchbar_subheading,
        'searchborbgimage' => $html,
        'c_cat_list' => $inst_arr,
        'subject_list' => $tag_arr,
        'search_action_link' => $CFG->wwwroot.'/local/course_details/search_result.php',
    ];
    return $OUTPUT->render_from_template('local_course_details/searchbox', $templatecontext);
}

// Abhijit : function for display card...
function get_card($card_header_img,$card_center_logo,$card_title,$card_link,$institution_color){
	global $OUTPUT,$DB;


    $templatecontext = [

					        'card_header_img' => $card_header_img,
					        'card_center_logo' => $card_center_logo,
					        'card_title' => $card_title,
					        'card_link' => $card_link,
                            'card-color'=> $institution_color,
					        'link_text' => get_string('view_details','local_course_details'),

					    ];
	return $OUTPUT->render_from_template('local_course_details/card', $templatecontext);
}

// Abhijit: function for get course image
function get_course_image_new($course) {
    global $CFG;
    $courseinlist = new \core_course_list_element($course);
    foreach ($courseinlist->get_course_overviewfiles() as $file) {
        if ($file->is_valid_image()) {
            $pathcomponents = [
                '/pluginfile.php',
                $file->get_contextid(),
                $file->get_component(),
                $file->get_filearea() . $file->get_filepath() . $file->get_filename()
            ];
            $path = implode('/', $pathcomponents);
            return (new moodle_url($path))->out();
        }
    }
    return false;
}

// Abhijit: function for get course image NEW.........
function course_image_cd($course_detail) {
	global $CFG, $PAGE, $OUTPUT;
	// Get course overview files.
	if (empty($CFG->courseoverviewfileslimit)) {
	    return '';
	}
	require_once ($CFG->libdir . '/filestorage/file_storage.php');
	require_once ($CFG->dirroot . '/course/lib.php');
	$fs = get_file_storage();
	$context = context_course::instance($course_detail->id);
	$files = $fs->get_area_files($context->id, 'course', 'overviewfiles', false, 'filename', false);
	if (count($files)) {
	    $overviewfilesoptions = course_overviewfiles_options($course_detail->id);
	    $acceptedtypes = $overviewfilesoptions['accepted_types'];
	    if ($acceptedtypes !== '*') {
	        // Filter only files with allowed extensions.
	        require_once ($CFG->libdir . '/filelib.php');
	        foreach ($files as $key => $file) {
	            if (!file_extension_in_typegroup($file->get_filename() , $acceptedtypes)) {
	                unset($files[$key]);
	            }
	        }
	    }
	    if (count($files) > $CFG->courseoverviewfileslimit) {
	        // Return no more than $CFG->courseoverviewfileslimit files.
	        $files = array_slice($files, 0, $CFG->courseoverviewfileslimit, true);
	    }
	}

	// Get course overview files as images - set $courseimage.
	// The loop means that the LAST stored image will be the one displayed if >1 image file.
	$courseimage = '';
	foreach ($files as $file) {
	    $isimage = $file->is_valid_image();
	    if ($isimage) {
	        $courseimage = file_encode_url("$CFG->wwwroot/pluginfile.php", '/' . $file->get_contextid() . '/' . $file->get_component() . '/' . $file->get_filearea() . $file->get_filepath() . $file->get_filename() , !$isimage);
	    }
	}

	if($courseimage!=''){
		return $courseimage;
	}else{
		return $CFG->wwwroot . '/local/course_details/pix/course-defoult-image.jpg';
	}


}

// Abhijit : function for get course category logo
function course_cat_logo_cd($course_cat_id){
	global $OUTPUT,$DB,$CFG;

    $sql = 'SELECT cs.id, lcd.catdisplay, lcd.catimage,lcd.catlogo,lcd.catdescription FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=? && cs.id=?';
    $institutions_detail = $DB->get_record_sql($sql, array(0,1,$course_cat_id));
    // print_object($institutions_detail);
    if(!empty($institutions_detail)){
        $institutions_logo = local_course_details_image($institutions_detail->catlogo,'catlogo');
        if($institutions_logo!=''){
            $institutionslogo =$institutions_logo;
        }else{
            $institutionslogo = $CFG->wwwroot . '/local/course_details/pix/course-cat-defoult-logo.png';
        }
    }else{
        $institutionslogo = $CFG->wwwroot . '/local/course_details/pix/course-cat-defoult-logo.png';
    }


	return $institutionslogo;
}

// Abhijit : function for display the Featured tab section----------------
function display_featured_tab_section(){
    global $OUTPUT,$PAGE,$CFG;

    $use_featured_tab_section = $PAGE->theme->settings->enablefcourse== 1;


    $featured_tab_count = (empty($PAGE->theme->settings->fctabcount)) ? 0 : $PAGE->theme->settings->fctabcount;
    $templatecontext['use_featured_tab_section'] = $use_featured_tab_section;
    $featured_tab_data = array();

    // print_object($PAGE->theme->settings->fctabname4);
        for ($i = 1; $i <= 4; $i++) {

            $fctabname_field = "fctabname{$i}";
            $fctabcontent_field = "fctabcontent{$i}";
            $fcourses_field = "fcourses{$i}";

            $fctabname = (empty($PAGE->theme->settings->$fctabname_field)) ? false : $PAGE->theme->settings->$fctabname_field;
            $fctabcontent = (empty($PAGE->theme->settings->$fctabcontent_field)) ? false : $PAGE->theme->settings->$fctabcontent_field;
            $fcourses = $PAGE->theme->settings->$fcourses_field;
            $fcourses_id = explode(",",$fcourses);
            $course_card = '';
            // print_object($fcourses_id);
            if(!empty($fcourses_id)){
                if($i!=1){
                    foreach ($fcourses_id as $cid) {
                        $course_card .= get_course_card($cid);
                        $tabviewalllink =   new moodle_url($CFG->wwwroot . '/local/course_details/search_result.php',array());
                        $tabviewalltext = get_string('view_all_courses1','local_course_details');


                    }
                }else{
                    foreach ($fcourses_id as $catid) {
                        $course_card .= get_institutions_card($catid);
                        $tabviewalllink =   new moodle_url($CFG->wwwroot . '/course/',array());
                        $tabviewalltext = get_string('view_all_institutions','local_course_details');

                    }

                }


          	}else{
				$course_card = html_writer::div(get_string('nocourse','local_course_details'), 'container p-4 text-danger text-center');
			}

            $ftdataarr = array(
                            'fctabname' => $fctabname,
                            'fctabcontent' => $fctabcontent,
                            'fcourses' => $course_card,
                            'tabid' => 'ftab'.$i,
                            'tabviewalllink' => $tabviewalllink,
                            'tabviewalltext' => $tabviewalltext,
                        );
            if($i==1){
               $ftdataarr['firsttab'] = 1;
            }

            array_push($featured_tab_data, $ftdataarr);
        }

        $templatecontext['featured_tab_data'] = $featured_tab_data;
        // print_object($templatecontext);

    return $OUTPUT->render_from_template('local_course_details/featured_tabs_section', $templatecontext);
}


// Abhijit : function for display the Online Short Course tab section----------------
function display_online_short_course_tab_section(){
    global $OUTPUT,$PAGE,$CFG;

    $use_online_short_tab_section = $PAGE->theme->settings->enable_online_short== 1;


    // $featured_tab_count = (empty($PAGE->theme->settings->fctabcount)) ? 0 : $PAGE->theme->settings->fctabcount;
    $templatecontext['use_online_short_course_tab_section'] = $use_online_short_tab_section;
    $online_short_course_tab_data = array();

    // print_object($PAGE->theme->settings->fctabname4);
        for ($i = 1; $i <= 3; $i++) {

            $online_short_tabname_field = "online_short_tabname{$i}";
            $online_short_tabcontent_field = "online_short_tabcontent{$i}";
            $online_short_courses_field = "online_short_courses{$i}";

            $online_short_tabname = (empty($PAGE->theme->settings->$online_short_tabname_field)) ? false : $PAGE->theme->settings->$online_short_tabname_field;
            $online_short_tabcontent = (empty($PAGE->theme->settings->$online_short_tabcontent_field)) ? false : $PAGE->theme->settings->$online_short_tabcontent_field;
            $online_short_courses = $PAGE->theme->settings->$online_short_courses_field;
            $online_short_courses_id = explode(",",$online_short_courses);
            $course_card = '';
            // print_object($fcourses_id);
            if(!empty($online_short_courses_id)){
                // if($i!=1){
                    foreach ($online_short_courses_id as $cid) {
                        $course_card .= get_course_card($cid);
                    }
                // }else{
                //     foreach ($fcourses_id as $catid) {
                //         $course_card .= get_institutions_card($catid);
                //     }

                // }


            }else{
                $course_card = html_writer::div(get_string('nocourse','local_course_details'), 'container p-4 text-danger text-center');
            }

            $ftdataarr = array(
                            'online_short_tabname' => $online_short_tabname,
                            'online_short_tabcontent' => $online_short_tabcontent,
                            'online_short_courses' => $course_card,
                            'tabid' => 'ftab'.$i,
                            'tabviewalllink' => new moodle_url($CFG->wwwroot . '/local/course_details/search_result.php',array()),
                            'tabviewalltext' => get_string('view_all_courses1','local_course_details'),
                        );
            if($i==1){
               $ftdataarr['firsttab'] = 1;
            }

            array_push($online_short_course_tab_data, $ftdataarr);
        }

        $templatecontext['online_short_course_tab_data'] = $online_short_course_tab_data;
        // print_object($templatecontext);

    return $OUTPUT->render_from_template('local_course_details/online_shortcourses_tabs_section', $templatecontext);
}

// Abhijit : function get course card............
function get_course_card($cid){
	global $DB,$PAGE,$OUTPUT,$CFG;

	$course_detail = $DB->get_record('course',array('id'=>$cid));

	if(!empty($course_detail)){

		$courseimage = course_image_cd($course_detail);
		$course_cat_logo = course_cat_logo_cd($course_detail->category);
		$coursename = $course_detail->fullname;
		$course_details_page_link = new moodle_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$cid));
        //Added by manoj for course cat color 01/04/2020
        $course_cat_color = course_cat_color($course_detail->category);

        $course_card = get_card($courseimage,$course_cat_logo,$coursename,$course_details_page_link,$course_cat_color);

	}else{
		$course_card = false;
	}

	return $course_card;
}

//  Abhijit : function for course details page header
function course_details_page_header($cid){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;
    $course_detail = $DB->get_record('course', array('id'=>$cid,'visible'=>1));
    if ($course_detail) {
        $sql = "SELECT cd.intvalue FROM {customfield_field} cf INNER JOIN {customfield_data} cd ON cf.id=cd.fieldid WHERE instanceid=? && shortname=?";
        $custom_course_type = $DB->get_record_sql($sql,array($course_detail->id,'coursetype'));
        $coursename = $course_detail->fullname;
        $coursestartdate = userdate($course_detail->startdate,  '%d %B %Y');
        $course_cat_logo = course_cat_logo_cd($course_detail->category);
        $courscourse_image = course_image_cd($course_detail);
        $download_prospectus_link = '#';
        if ($custom_course_type->intvalue==1) {
            $download_prospectus_button_name = get_string('download_brochure','local_course_details');

        } else {
            $download_prospectus_button_name = get_string('download_procpectus','local_course_details');
        }


        if ($USER->id!=0) {
                $userid = $USER->id;
                $context = context_course::instance($cid);
                $check_enrolled =  is_enrolled($context,$userid);
                if(!empty($check_enrolled)){
                    $button_name = get_string('goto','local_course_details');
                    $redirect_url = new moodle_url ('/course/view.php',array('id'=>$cid));
                    //Added by manoj 26 june 2020
                    $enrolled = "display:block";
                    $register_via_online = "display:none";
                    $register_via_offline = "display:none";
                    $register_via_elearning = "display:none";
                    $not_loggedin = 'display:none';
                    $register_block = 'display:none';
                }else{
                    $redirect_url = new moodle_url ('/login/signup.php');
                    $button_name = get_string('buy','local_course_details');
                    // if ($custom_course_type->intvalue==1) {
                    //     $button_name = get_string('enroll_now','local_course_details');
                    // } else {
                    //     $button_name = get_string('register_now','local_course_details');
                    // }

                   $reg_online_button_name = get_string('reg_online_button_name','local_course_details');
                   $reg_offline_button_name = get_string('reg_offline_button_name','local_course_details');
                   $reg_elearning_button_name = get_string('reg_elearning_button_name','local_course_details');
                     //Added by manoj 26 june 2020
                    //check whether if the paticular user already wishlist the course or not
                    $check_wishlist = $DB->get_record('local_wishlist',array('userid'=>$USER->id,'course_id'=>$cid));
                    if($check_wishlist->reg_data == 'Online'){
                        $msg = get_string('reg_online_msg','local_course_details');
                    }elseif($check_wishlist->reg_data == 'Offline'){
                         $msg = get_string('reg_offline_msg','local_course_details');
                    }else{
                         $msg = get_string('reg_elearning_msg','local_course_details');
                    }
                    $register_success_msg = $msg;
                    if(!empty($check_wishlist)){
                        $enrolled = "display:none";
                        $register_via_online = "display:none";
                        $register_via_offline = "display:none";
                        $register_via_elearning = "display:none";
                        $not_loggedin = 'display:none';
                        $register_block = 'display:block';

                    }else{
                       $enrolled = "display:none";
                       $register_via_online = "display:block";
                       $register_via_offline = "display:block";
                       $register_via_elearning = "display:block";
                       $not_loggedin = 'display:none';
                       $register_block = 'display:none';
                    }
                }
             }
             else{

               $redirect_url = new moodle_url($CFG->wwwroot .'/login/signup.php?');
                if ($custom_course_type->intvalue==1) {
                    $button_name = get_string('enroll_now','local_course_details');
                } else {
                    $button_name = get_string('register_now','local_course_details');
                }
                 //Added by manoj 26 june 2020
                  $enrolled = "display:none";
                  $register_via_online = "display:none";
                  $register_via_offline = "display:none";
                  $register_via_elearning = "display:none";
                  $not_loggedin = 'display:block';
                  $register_block = 'display:none';
             }



        $request_call_link = '#';
        // $request_call_button_name = get_string();
        $templatecontext = ['coursename'=>$coursename,'coursestartdate'=>$coursestartdate,'course_cat_logo'=>$course_cat_logo,'courscourse_image'=>$courscourse_image,'download_prospectus_link'=>$download_prospectus_link,'download_prospectus_button_name'=>$download_prospectus_button_name,'request_call_link'=>$request_call_link,
        'enrolled'=> $enrolled,'register_via_online'=>$register_via_online,
        'register_via_offline'=>$register_via_offline,'register_via_elearning'=>$register_via_elearning,'not_loggedin'=>$not_loggedin,
        'cid'=>$cid,'userid'=>$USER->id,'register_success_msg'=>$register_success_msg,'register_block'=>$register_block,'reg_online_button_name'=>$reg_online_button_name,'reg_offline_button_name'=>$reg_offline_button_name,'reg_elearning_button_name'=>$reg_elearning_button_name

    ];


            $templatecontext['register_pg_link'] = $redirect_url;
            $templatecontext['button_name'] = strtoupper($button_name);
        if ($custom_course_type->intvalue==1) {
            $templatecontext['free_course'] = 1;

        }

        // for registration cv form Mihir
        $register_cv = new moodle_url('/local/trainee_registration/create_registration.php');
        $templatecontext['register_cv'] = $register_cv;

        return $OUTPUT->render_from_template('local_course_details/course_details_page_header', $templatecontext);
    }
}

// Manoj Date:01/04/2020 : function for get course category color
function course_cat_color($course_cat_id){
    global $OUTPUT,$DB,$CFG;

    $sql = 'SELECT cs.id,lcd.std_login_pg_color,lcd.catdisplay,lcd.catimage,lcd.catlogo,lcd.catdescription FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=? && cs.id=?';
    $institutions_detail = $DB->get_record_sql($sql, array(0,1,$course_cat_id));
    if(!empty($institutions_detail)){
        $institutions_color = $institutions_detail->std_login_pg_color;

    }

return $institutions_color;
}

// Abhijit : function for course details page tab
function course_details_page_tab($cid){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;
    require_once('../../config.php');
    require_once ($CFG->dirroot . '/local/edu_ratings_reviews/lib.php');

    $sql = "SELECT cd.intvalue FROM {customfield_field} cf INNER JOIN {customfield_data} cd ON cf.id=cd.fieldid WHERE instanceid=? && shortname=?";
        $custom_course_type = $DB->get_record_sql($sql,array($cid,'coursetype'));

        if ($custom_course_type->intvalue==1) {
            $download_prospectus_button_name = get_string('download_brochure','local_course_details');
        } else {
            $download_prospectus_button_name = get_string('download_procpectus','local_course_details');
        }


    if ($USER->id!=0) {
        $userid = $USER->id;
        $context = context_course::instance($cid);
        $check_enrolled =  is_enrolled($context,$userid);
        if(!empty($check_enrolled)){
            $button_name = get_string('goto','local_course_details');
            $redirect_url = new moodle_url ('/course/view.php',array('id'=>$cid));
            //Added by manoj 26 june 2020
            $enrolled = "display:block";
            $register_via_online = "display:none";
            $register_via_offline = "display:none";
            $register_via_elearning = "display:none";
            $not_loggedin = 'display:none';
            $register_block = 'display:none';
        }else{

            $redirect_url = new moodle_url ('/login/signup.php');
            $button_name = get_string('buy','local_course_details');
             $reg_online_button_name = get_string('reg_online_button_name','local_course_details');
           $reg_offline_button_name = get_string('reg_offline_button_name','local_course_details');
           $reg_elearning_button_name = get_string('reg_elearning_button_name','local_course_details');
             //Added by manoj 26 june 2020
            //check whether if the paticular user already wishlist the course or not
            $check_wishlist = $DB->get_record('local_wishlist',array('userid'=>$USER->id,'course_id'=>$cid));
            if($check_wishlist->reg_data == 'Online'){
                $msg = get_string('reg_online_msg','local_course_details');
            }elseif($check_wishlist->reg_data == 'Offline'){
                 $msg = get_string('reg_offline_msg','local_course_details');
            }else{
                 $msg = get_string('reg_elearning_msg','local_course_details');
            }
            $register_success_msg = $msg;
            if(!empty($check_wishlist)){
                $enrolled = "display:none";
                $register_via_online = "display:none";
                $register_via_offline = "display:none";
                $register_via_elearning = "display:none";
                $not_loggedin = 'display:none';
                $register_block = 'display:block';

            }else{
               $enrolled = "display:none";
               $register_via_online = "display:block";
               $register_via_offline = "display:block";
               $register_via_elearning = "display:block";
               $not_loggedin = 'display:none';
               $register_block = 'display:none';
            }


        }
     }
     else{
       //$redirect_url = new moodle_url($CFG->wwwroot .'/local/edu_registration/index.php',array('step'=>1,'cid'=>$cid));
       $redirect_url = new moodle_url ('/login/signup.php');

        if ($custom_course_type->intvalue==1) {
            $button_name = get_string('enroll_now','local_course_details');
        } else {
            $button_name = get_string('register_now','local_course_details');
        }
         //Added by manoj 26 june 2020
          $enrolled = "display:none";
          $register_via_online = "display:none";
          $register_via_offline = "display:none";
          $register_via_elearning = "display:none";
          $not_loggedin = 'display:block';
          $register_block = 'display:none';
     }
    $course_detail = $DB->get_record('course', array('id'=>$cid,'visible'=>1));
    $course_cat_detail = $DB->get_record('local_course_details', array('catid'=>$course_detail->category));
    $sql = 'SELECT cf.shortname,cf.name,cd.value FROM {customfield_data} cd
            INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id
            WHERE cd.instanceid=?';
    $course_custom_data = $DB->get_records_sql($sql,array($cid));
    // print_object($course_custom_data);
    //Mihr just initialising Univ
    $cextra_templatecontext['university_name'] = '';
    $cextra_templatecontext['language'] = '';

    if (!empty($course_detail) && !empty($course_custom_data)) {

       foreach ($course_custom_data as $key => $value) {
            $cextra_templatecontext[$key] = $value->value;
        }
        //data validation to check whether the date is empty or not//
        if($cextra_templatecontext['reg_close'] == 0){
            $reg_closedate = '-';
        }else{
            $reg_closedate = userdate($cextra_templatecontext['reg_close'],  '%d %B %Y');
        }
        $cextra_templatecontext['reg_close'] =  $reg_closedate;
        if($course_detail->startdate == 0){

            $cour_startdate = '-';
        }else{

             $cour_startdate = userdate($course_detail->startdate,  '%d %B %Y');
        }

        // print_object($cextra_templatecontext['course_curriculum']);
        $cextra_templatecontext['course_startdate'] = $cour_startdate;
        $cextra_templatecontext['university_name'] = $course_cat_detail->catdisplay;
        $cextra_templatecontext['img_url'] = $CFG->wwwroot.'/local/course_details/pix/pic.jpg';

            $cextra_templatecontext['register_pg_link'] = $redirect_url;
            $cextra_templatecontext['button_name'] = strtoupper($button_name);
        if ($custom_course_type->intvalue==1) {
            $cextra_templatecontext['free_course'] = 1;
        }


             $cextra_templatecontext['enrolled']=  $enrolled;
             $cextra_templatecontext['not_loggedin']=$not_loggedin;
             $cextra_templatecontext['cid']= $cid;
             $cextra_templatecontext['userid']= $USER->id;
             $cextra_templatecontext['userid']= $USER->id;
             $cextra_templatecontext['register_via_online']= $register_via_online;
             $cextra_templatecontext['register_via_offline']= $register_via_offline;
             $cextra_templatecontext['register_via_elearning']= $register_via_elearning;
             $cextra_templatecontext['register_success_msg']= $register_success_msg;
             $cextra_templatecontext['register_block']= $register_block;
             $cextra_templatecontext['reg_online_button_name']= $reg_online_button_name;
             $cextra_templatecontext['reg_offline_button_name']= $reg_offline_button_name;
             $cextra_templatecontext['reg_elearning_button_name']= $reg_elearning_button_name;



        $course_custom_field_area = $OUTPUT->render_from_template('local_course_details/course_details_page_tabs_extra_fields', $cextra_templatecontext);
        $context = context_course::instance($cid);
        $users = get_enrolled_users($context);
        $users1 = get_enrolled_users($context, 'moodle/course:update');

        $admins = get_users_by_capability($context, 'moodle/course:doanything');
        $admins_id = array();
        foreach ($admins as $id => $admin) {
        $admins_id[] = $admin->id;
        }

        $heads = get_users_by_capability($context, 'moodle/course:update');
        $heads_id = array();
        foreach ($heads as $id => $head) {
        $heads_id[] = $head->id;
        }

		$teachers_id = array_diff($heads_id, $admins_id);

		$teachersdata_arr = array();
		foreach ($teachers_id as $teachersid) {
			$teacherdata = $DB->get_record('user', array('id'=>$teachersid));
			$user_picture= new user_picture($teacherdata);
			$src=$user_picture->get_url($PAGE);
			$teacher_img = $src;
			$teacherdata_arr = [
									'instructor_name'=> $teacherdata->firstname.' '.$teacherdata->lastname,
									'instructor_desc'=> $teacherdata->description,
									'instructor_image' => $teacher_img
								];
			array_push($teachersdata_arr, $teacherdata_arr);
		}
		$instructor_templatecontext = ['instructor_data'=>$teachersdata_arr];

    // Mihir no need to show instructor tab skills

  $instructor = $OUTPUT->render_from_template('local_course_details/course_details_page_tabs_instructor', $instructor_templatecontext);




        $ratings_data = $DB->get_record('edupros_course_rating', array('courseid'=>$cid));

        $star_html = '';
        for ($i=1; $i<=5; $i++) {

            $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));

            $star_html .= html_writer::tag('span', $star);
        }
        $total_no_of_ratings = 0;
        $five_star = 0;
        $four_star = 0;
        $three_star = 0;
        $two_star = 0;
        $one_star = 0;
        $overall_ratings = 0;

        if (!empty($ratings_data)) {
            $overall_ratings = $ratings_data->overall_ratings;
            $star_html = '';
            for ($i=1; $i<=5; $i++) {
                if($i<=$overall_ratings){
                    $star = html_writer::tag('i', '', array('class'=>'fa fa-star'));
                }else{
                    $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
                }
                $star_html .= html_writer::tag('span', $star);
            }
            $total_no_of_ratings = $ratings_data->total_no_of_ratings;
            $five_star = $ratings_data->five_star;
            $four_star = $ratings_data->four_star;
            $three_star = $ratings_data->three_star;
            $two_star = $ratings_data->two_star;
            $one_star = $ratings_data->one_star;


        }

        $review_ratings_temp_cobtext = [
                                            'overall_ratings'=>$overall_ratings,
                                            'overall_ratings_star'=>$star_html,
                                            'total_no_of_ratings'=>$total_no_of_ratings,
                                            'five_star'=>$five_star,
                                            'four_star'=>$four_star,
                                            'three_star'=>$three_star,
                                            'two_star'=>$two_star,
                                            'one_star'=>$one_star
                                        ];

        $reviews_data = $DB->get_records('edupros_course_review', array('courseid'=>$cid));

        $review_context_array = array();

        if (!empty($reviews_data)) {

            foreach ($reviews_data as $key_id => $review_data) {
                $user_image_itemid = $review_data->image;
                $user_img = local_edu_ratings_reviews_image($user_image_itemid,'review_image');
                $name_of_the_person = $review_data->name_of_the_person;
                $title = $review_data->title;
                $review_text = $review_data->review_text;
                $review_rating = $review_data->review_rating;
                $star_html1 = '';
                for ($i=1; $i<=5; $i++) {
                    if($i<=$review_rating){
                        $star1 = html_writer::tag('i', '', array('class'=>'fa fa-star'));
                    }else{
                        $star1 = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
                    }
                    $star_html1 .= html_writer::tag('span', $star1);
                }
                $review_rating_star = $star_html1;

                $review_context = [
                                        'user_img'=>$user_img,
                                        'name_of_the_person'=>$name_of_the_person,
                                        'title'=>$title,
                                        'review_text'=>$review_text,
                                        'review_rating'=>$review_rating_star
                                    ];
                array_push($review_context_array, $review_context);

            }

            $review_ratings_temp_cobtext['reviews_data'] = $review_context_array;

        }



        $review_ratings = $OUTPUT->render_from_template('local_course_details/course_details_page_tabs_review_ratings', $review_ratings_temp_cobtext);

        $pdf = '.pdf';
        $course_prospectus_link = new moodle_url($CFG->wwwroot .'/local/course_details/pix/'.$course_detail->shortname.$pdf);

  // Mihir changing as for this we will take the extra field overview 'overview'=>$course_detail->summary,

        $templatecontext = [

                                'overview'=>$cextra_templatecontext['courseoverview'],
                                'curriculum'=>$cextra_templatecontext['course_curriculum'],
                                'instructor'=>$instructor,
                                'reviews'=>$review_ratings,
                                'cextrafields'=>$course_custom_field_area,
                                'register_pg_link'=>$redirect_url,
                                'course_prospectus_link'=>$course_prospectus_link,
                                'download_prospectus_button_name'=>$download_prospectus_button_name,
                                'button_name'=>strtoupper($button_name)
                                                   ];
        if ($custom_course_type->intvalue==1) {
            $templatecontext['free_course'] = 1;

        }
        return $OUTPUT->render_from_template('local_course_details/course_details_page_tabs', $templatecontext);
    }


}

// Abhijit : function get institution card............
function get_institutions_card($catid){
    global $DB,$PAGE,$OUTPUT,$CFG;

    $sql = 'SELECT cs.id, lcd.catdisplay,lcd.std_login_pg_color,lcd.catimage,lcd.catlogo,lcd.catdescription FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=? && cs.id=?';
    $institutions_detail = $DB->get_record_sql($sql, array(0,1,$catid));
    $institutions_card = '';

    if(!empty($institutions_detail)){

        $institutionsimage = local_course_details_image($institutions_detail->catimage,'catimage');
        $institutions_logo = local_course_details_image($institutions_detail->catlogo,'catlogo');
        $institutionsname = $institutions_detail->catdisplay;
        $institutions_details_page_link = new moodle_url($CFG->wwwroot . '/course/index.php',array('categoryid'=>$institutions_detail->id));
        //Added by manoj 01/05/2020 for institution color
        $institution_color = $institutions_detail->std_login_pg_color;
        $institutions_card = get_card($institutionsimage,$institutions_logo,$institutionsname,$institutions_details_page_link,$institution_color);

    }else{
        $institutions_card = false;
    }

    return $institutions_card;
}

//manju:function for displaying contactform.
function gs_contactform($courseid){
    global $OUTPUT,$DB,$CFG;
    $course_details = $DB->get_record('course',array('id'=>$courseid));
    $course_cat_details = $DB->get_record('local_course_details',array('catid'=>$course_details->category));
    $form_action = new moodle_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$courseid));
    $templatecontext = [
                            'textline1'=>get_string('textline1','local_course_details',$course_details->fullname),
                            'textline2'=>get_string('textline2','local_course_details',$course_cat_details->catdisplay),
                            'form_action'=> $form_action

                        ];
    return $OUTPUT->render_from_template('local_course_details/contact',$templatecontext);
}
//manju:function for displaying newsletter form.
function gs_newsletter(){
    global $OUTPUT;
    return $OUTPUT->render_from_template('local_course_details/newsletter',true);
}
//manju:function for displaying corporate contactform.
function gs_corporatecontact(){
    global $OUTPUT;
    echo $OUTPUT->render_from_template('local_course_details/moreinformation',true);
}
//manju:function for displaying testimonial section.
function gs_testimonial(){
    global $PAGE, $OUTPUT;
        $usetestimonials = $PAGE->theme->settings->usetestimonials == 1;

        $testimonialsectiontitle = (empty($PAGE->theme->settings->testimonialsectiontitle)) ? false : format_text($PAGE->theme->settings->testimonialsectiontitle);

        $defaultimage = $OUTPUT->image_url('default-profile', 'theme');

        //Testimonial 1
        $testimonial1content = (empty($PAGE->theme->settings->testimonial1content)) ? false : format_text($PAGE->theme->settings->testimonial1content);
        $testimonial1image = (empty($PAGE->theme->setting_file_url('testimonial1image', 'testimonial1image'))) ? false : $PAGE->theme->setting_file_url('testimonial1image', 'testimonial1image');
        $testimonial1name = (empty($PAGE->theme->settings->testimonial1name)) ? false : format_text($PAGE->theme->settings->testimonial1name);
        $testimonial1meta = (empty($PAGE->theme->settings->testimonial1meta)) ? false : format_text($PAGE->theme->settings->testimonial1meta);

        //Testimonial 2
        $testimonial2content = (empty($PAGE->theme->settings->testimonial2content)) ? false : format_text($PAGE->theme->settings->testimonial2content);
        $testimonial2image = (empty($PAGE->theme->setting_file_url('testimonial2image', 'testimonial2image'))) ? false : $PAGE->theme->setting_file_url('testimonial2image', 'testimonial2image');
        $testimonial2name = (empty($PAGE->theme->settings->testimonial2name)) ? false : format_text($PAGE->theme->settings->testimonial2name);
        $testimonial2meta = (empty($PAGE->theme->settings->testimonial2meta)) ? false : format_text($PAGE->theme->settings->testimonial2meta);
        $dataarray = [
                'testimonial' => $testimonial1content,
                'testimonialimage' => $testimonial1image,
                'testimonialname' => $testimonial1name,
                'testimonialmeta' => $testimonial1meta,
                'testimonial1' => $testimonial2content,
                'testimonialimage1' => $testimonial2image,
                'testimonialname1' => $testimonial2name,
                'testimonialmeta1' => $testimonial2meta,
            ];

        $testimonials = [

        'usetestimonials' => $dataarray,
        ];
    return $OUTPUT->render_from_template('local_course_details/testimonial2', $testimonials);
}

// Abhijit : display our features section

function display_our_features_section(){
	global $PAGE, $OUTPUT;

    $useourfeatures = $PAGE->theme->settings->useourfeatures== 1;
    $of_heading = (empty($PAGE->theme->settings->of_heading))?false : format_text($PAGE->theme->settings->of_heading);
    $of_maincontent = (empty($PAGE->theme->settings->of_maincontent))? false : format_text($PAGE->theme->settings->of_maincontent);

    $of_section_iconone1 = (empty($PAGE->theme->setting_file_url('of_section_iconone1', 'of_section_iconone1')))? false : $PAGE->theme->setting_file_url('of_section_iconone1','of_section_iconone1');
    $of_section_content1 = (empty($PAGE->theme->settings->of_section_content1))? false : format_text($PAGE->theme->settings->of_section_content1);

    $of_section_iconone2 = (empty($PAGE->theme->setting_file_url('of_section_iconone2', 'of_section_iconone2')))? false : $PAGE->theme->setting_file_url('of_section_iconone2','of_section_iconone2');
    $of_section_content2 = (empty($PAGE->theme->settings->of_section_content2))? false : format_text($PAGE->theme->settings->of_section_content2);

    $of_section_iconone3 = (empty($PAGE->theme->setting_file_url('of_section_iconone3', 'of_section_iconone3')))? false : $PAGE->theme->setting_file_url('of_section_iconone3','of_section_iconone3');
    $of_section_content3 = (empty($PAGE->theme->settings->of_section_content3))? false : format_text($PAGE->theme->settings->of_section_content3);


    $array_value = [
        'useourfeatures'=>$useourfeatures,
        'of_heading'=>$of_heading,
        'of_maincontent'=>$of_maincontent,
        'of_section_iconone1'=>$of_section_iconone1,
        'of_section_content1'=>$of_section_content1,
        'of_section_iconone2'=>$of_section_iconone2,
        'of_section_content2'=>$of_section_content2,
        'of_section_iconone3'=>$of_section_iconone3,
        'of_section_content3'=>$of_section_content3
    ];

    return $OUTPUT->render_from_template('local_course_details/our_features_section', $array_value);

}

//Abhijit :function for displaying institution header.
function display_institutions_page_header_section(){
    global $OUTPUT,$CFG;
    $templatecontext = ['header_bg_img'=>$CFG->wwwroot.'/local/course_details/pix/Institutions_header_bg.png'];
    return $OUTPUT->render_from_template('local_course_details/Institutions_page_header_section',$templatecontext);
}
//Abhijit :function for displaying institution header.
function display_login_page_header_section(){
    global $OUTPUT;
    return $OUTPUT->render_from_template('local_course_details/login_page_header_section',true);
}


//Abhijit :function for displaying institution page list.
function display_institutions_page_list_section(){
    global $OUTPUT,$DB,$CFG;

    $sql = 'SELECT cs.id, lcd.catdisplay,lcd.std_login_pg_color,lcd.catimage,lcd.catlogo FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=?';
    $institutions_details = $DB->get_records_sql($sql, array(0,1));
    $institution_list = '';

    foreach ($institutions_details as $key => $institutions_detail) {
        $institutionsimage = local_course_details_image($institutions_detail->catimage,'catimage');
        $institutions_logo = local_course_details_image($institutions_detail->catlogo,'catlogo');
        $institutionsname = $institutions_detail->catdisplay;
        $institutions_details_page_link = new moodle_url($CFG->wwwroot . '/course/index.php',array('categoryid'=>$institutions_detail->id));
        //Added by manoj 01/05/2020 for institution color
        $institution_color = $institutions_detail->std_login_pg_color;
        $institution_list .= get_card($institutionsimage,$institutions_logo,$institutionsname,$institutions_details_page_link,$institution_color);
    }
    $templatecontext = [
                           'section_title' => get_string('institution_list_section_title','local_course_details'),
                           'section_description' => get_string('institution_list_section_desc','local_course_details'),
                           'institution_card_list' => $institution_list,
                           'all_course_page_link' => $CFG->wwwroot.'/local/course_details/search_result.php',
                        ];
    return $OUTPUT->render_from_template('local_course_details/Institutions_page_list_section',$templatecontext);
}

//Abhijit :function for displaying institution page details.
function display_institutions_details_page($catid){
    global $OUTPUT,$DB,$CFG;

    $sql = 'SELECT cs.id, lcd.catdisplay, lcd.catimage,lcd.catlogo,lcd.catdescription FROM {course_categories} cs
            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
            WHERE parent=? && visible=? && cs.id=?';
    $institutions_detail = $DB->get_record_sql($sql, array(0,1,$catid));
    $course_details = $DB->get_records('course',array('category'=>$catid,'visible'=>1));
    $course_list = '';


        $institutionsimage = local_course_details_image($institutions_detail->catimage,'catimage');
        $institutions_logo = local_course_details_image($institutions_detail->catlogo,'catlogo');
        $institutionsname = $institutions_detail->catdisplay;

        if(!empty($institutions_detail->catdescription)){
            $context = \context_system::instance();
            $institutionsdescription = file_rewrite_pluginfile_urls($institutions_detail->catdescription, 'pluginfile.php',$context->id, 'local_course_details', 'catdescription', null);
        }


        // $institutionsdescription= $institutions_detail->catdescription;
        $institutions_details_page_link = new moodle_url($CFG->wwwroot . '/local/course_details/institutions_details.php',array('id'=>$institutions_detail->id));


        foreach ($course_details as $course_detail) {
            $course_list .= get_course_card($course_detail->id);

        }


    $templatecontext = [
                           'catimage' => $institutionsimage,
                           'catlogo'=>$institutions_logo,
                           'catname'=>$institutionsname,
                           'catdesc'=>$institutionsdescription,
                           'coursecardlist'=>$course_list,
                        ];
    return $OUTPUT->render_from_template('local_course_details/course_cat_details',$templatecontext);
}

function get_institutions_extra_fields_custom_url(){
    global $OUTPUT,$DB,$CFG;

    $abc = $_SERVER['HTTP_HOST'];

	$sql = 'SELECT cs.id, lcd.catlink FROM {course_categories} cs
	    INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
	    WHERE lcd.catlink  LIKE "%'.$abc.'%" && cs.parent=? && cs.visible=?';

	$institutionsdetail = $DB->get_record_sql($sql, array(0,1));
	if (!empty($institutionsdetail)) {

        $catid = $institutionsdetail->id;

	    $sql = 'SELECT cs.id, lcd.catlogo, lcd.catimage, lcd.catdisplay, lcd.std_login_pg_title, lcd.std_login_pg_desc,lcd.std_login_pg_logo,lcd.std_login_pg_bgimg,lcd.std_login_pg_color FROM {course_categories} cs
	            INNER JOIN {local_course_details} lcd ON lcd.catid=cs.id
	            WHERE parent=? && visible=? && cs.id=?';
	    $institutions_detail = $DB->get_record_sql($sql, array(0,1,$catid));
	    $institutions_extra_fields = array();
	    if (!empty($institutions_detail)) {
	        $institutions_logo = local_course_details_image($institutions_detail->catlogo,'catlogo');
            $institutions_image = local_course_details_image($institutions_detail->catimage,'catimage');

	        $institutions_name = $institutions_detail->catdisplay;
	        $std_login_pg_title = $institutions_detail->std_login_pg_title;
	        $std_login_pg_desc = $institutions_detail->std_login_pg_desc;
            $std_login_pg_color = $institutions_detail->std_login_pg_color;

	        $std_login_pg_logo = local_course_details_image($institutions_detail->std_login_pg_logo,'login_pg_logo');
	        $std_login_pg_bgimg = local_course_details_image($institutions_detail->std_login_pg_bgimg,'login_pg_bgimg');
            $institutions_extra_fields = array('institutions_logo'=>$institutions_logo,'institutions_image'=>$institutions_image, 'institutions_name'=>$institutions_name,'std_login_pg_title'=>$std_login_pg_title,'std_login_pg_desc'=>$std_login_pg_desc,'std_login_pg_logo'=>$std_login_pg_logo,'std_login_pg_bgimg'=>$std_login_pg_bgimg,'std_login_pg_color'=>$std_login_pg_color);
	    }


	    return $institutions_extra_fields;

	}else{
		return false;
	}

}

//manju:functions for getiing category images and category logo.
function local_course_details_image($itemid,$filearea){
    //*************************************** old code | comented by Abhijit************************
    // global $DB,$CFG;
    // if(!empty($itemid)){
    //     $context = context_system::instance();
    //     $contextid = $context->contextlevel;
    //     global $USER;
    //     $component = 'local_course_details';
    //     $fs = get_file_storage();
    //     $files = $fs->get_area_files($contextid, $component, $filearea, $itemid);

    //     if(!empty($files)){
    //         $url2 ='';
    //         foreach($files as $file) {
    //             $file->get_filename();
    //             $url2 = moodle_url::make_pluginfile_url(
    //                 $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
    //             );
    //         }
    //         return $url2;
    //     }
    // }
    //*******************************************************************************************************

     // global $DB,$CFG;
    if(!empty($itemid)){

        global $USER;

     // $context = context_system::instance();
     //    $contextid = $context->contextlevel;
        $context = context_user::instance(2);
        $component = 'local_course_details';

        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, $component, $filearea, $itemid,'sortorder', false);

        if(!empty($files)){
            $count = count($files);
            if($count>1)
            {
                  $url2 =[];
                foreach($files as $file) {
                    $file->get_filename();
                    $url2[] = moodle_url::make_pluginfile_url(
                        $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
                    );

                }
            }
            else
            {
                 $url2 ="";
                foreach($files as $file) {
                    $file->get_filename();
                    $url2 = moodle_url::make_pluginfile_url(
                        $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
                    );

                }
            }

            return $url2;
        }
    }
}

//************************************ Old pluginfile function || commented by ABHIJIT *****************************
// function local_course_details_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {


//     // // Make sure the filearea is one of those used by the plugin.
//     // if ($filearea !== 'catimage') {
//     //     return false;
//     // }
//     //     // Make sure the filearea is one of those used by the plugin.
//     // if ($filearea !== 'catlogo') {
//     //     return false;
//     // }

//     // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
//     // require_login();


//     // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
//     $itemid = array_shift($args); // The first item in the $args array.

//     // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
//     // user really does have access to the file in question.

//     // Extract the filename / filepath from the $args array.
//     $filename = array_pop($args); // The last item in the $args array.
//     if (!$args) {
//         $filepath = '/'; // $args is empty => the path is '/'
//     } else {
//         $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
//     }

//     // Retrieve the file from the Files API.
//     $fs = get_file_storage();
//     $file = $fs->get_file($context->id, 'local_course_details', $filearea, $itemid, $filepath, $filename);
//     if (!$file) {
//         return false; // The file does not exist.
//     }

//     // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
//     // From Moodle 2.3, use send_stored_file instead.
//     //send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!

//     $forcedownload = true;

//     send_file($file, $file->get_filename(), true, $forcedownload, $options);

// }
//**********************************************************************************************************************


function local_course_details_pluginfile($course, $birecordorcm, $context, $filearea, $args, $forcedownload, array $options = array()) {

    if ($filearea !== 'catdescription') {

        $itemid = array_shift($args); // The first item in the $args array.

        $fs = get_file_storage();

        $filename = array_pop($args);
        $filepath = $args ? '/' . implode('/', $args) . '/' : '/';

        if (!$file = $fs->get_file($context->id, 'local_course_details', $filearea, $itemid, $filepath, $filename) or $file->is_directory()) {
            send_file_not_found();
        }

        \core\session\manager::write_close();
        send_stored_file($file, null, 0, $forcedownload, $options);


    }else{
        $fs = get_file_storage();

        $filename = array_pop($args);
        $filepath = $args ? '/' . implode('/', $args) . '/' : '/';

        if (!$file = $fs->get_file($context->id, 'local_course_details', $filearea, 0, $filepath, $filename) or $file->is_directory()) {
            send_file_not_found();
        }

        \core\session\manager::write_close();
        send_stored_file($file, null, 0, $forcedownload, $options);

    }

}


//manju:for student login links page.
function gs_catlinks(){
    global $OUTPUT,$CFG,$DB;
    $returnarray =[];
    $filearea="catlogo";
    $catdata = $DB->get_records('local_course_details');

    foreach ($catdata as $value) {
        $clogo = local_course_details_image($value->catlogo,$filearea);
        if(empty($value->std_login_pg_color)){
            $color = '#a78a60!important';
        }else{
            $color = $value->std_login_pg_color;
        }
        $returnarray[] =['catlink'=>$value->catlink,
                            'catlogo'=>$clogo,
                            'catcolor'=>$color,
                            'catname'=>$value->catdisplay];
    }
    $data = ['catlinks'=>$returnarray,
            'arrow'=>$CFG->wwwroot.'/local/course_details/pix/fast-forward.png'];

    return $OUTPUT->render_from_template('local_course_details/catlogin_links', $data);
}
/** Manoj
* Date : 16 JUNE 2020
*funtion name : display_wish_list();
*
**/
 function display_wish_list(){
    global $DB,$CFG,$USER,$OUTPUT;
    $systemcontext = context_system::instance();
    $cap1 = has_capability('local/course_details:viewallwishlist',$systemcontext);
    $cap2 = has_capability('local/course_details:wishlist',$systemcontext);
    if($cap1){
        $get_wishlist = $DB->get_records('local_wishlist');
        if(!empty($get_wishlist)){
            $i = 1;
            $wishlist_string = "";
            foreach($get_wishlist as $row){
                $get_user = $DB->get_record('user',array('id'=>$row->userid));
                $get_course = $DB->get_record('course',array('id'=>$row->course_id));
                $fullname = fullname($get_user);
                $email = $get_user->email;
                $coursename = $get_course->fullname;
                $date = date('d-m-Y',$row->date);
                $userprofile = $CFG->wwwroot.'/user/profile.php?id='.$row->userid;
                $course = $CFG->wwwroot.'/course/view.php?id='.$row->course_id;
                $wishlist_string.='<tr>
                                    <td>'.$i.'</td>
                                    <td><a href='.$userprofile.'>'.$fullname.'</a></td>
                                    <td>'.$email.'</td>
                                    <td><a href='.$course.'>'.$coursename.'</a></td>
                                    <td>'.$row->reg_data.'</td>
                                    <td>'.$date.'</td>
                                    </tr>';

                $i++;
            }
        }else{
          $wishlist_string = "";
          $wishlist_string.='<tr>
                                <td colspan="5" class="text-center">-----No Records Found-----</td>
                                </tr>';
        }

        $data_array  = ['wishlist_string'=>$wishlist_string];

        return $OUTPUT->render_from_template('local_course_details/wishlist',$data_array);
    }
    if($cap2){
      $get_wishlist = $DB->get_records('local_wishlist',array('userid'=>$USER->id));
      if(!empty($get_wishlist)){
        $i = 1;
        $wishlist_string = "";
        foreach($get_wishlist  as $row){
            $get_user = $DB->get_record('user',array('id'=>$row->userid));
            $get_course = $DB->get_record('course',array('id'=>$row->course_id));
            $fullname = fullname($get_user);
            $email = $get_user->email;
            $coursename = $get_course->fullname;
            $date = date('d-m-Y',$row->date);
            $userprofile = $CFG->wwwroot.'/user/profile.php?id='.$row->userid;
            $course = $CFG->wwwroot.'/course/view.php?id='.$row->course_id;
            $wishlist_string.='<tr>
                                    <td>'.$i.'</td>
                                    <td><a href='.$userprofile.'>'.$fullname.'</a></td>
                                    <td>'.$email.'</td>
                                    <td><a href='.$course.'>'.$coursename.'</a></td>
                                    <td>'.$row->reg_data.'</td>
                                    <td>'.$date.'</td>
                                    </tr>';

                $i++;
        }

      }else{
          $wishlist_string = "";
          $wishlist_string.='<tr>
                                <td colspan="5" class="text-center">-----No Records Found-----</td>
                                </tr>';
        }

        $data_array  = ['wishlist_string'=>$wishlist_string];

        return $OUTPUT->render_from_template('local_course_details/wishlist',$data_array);
    }
 }
 /** Manoj
* Date : 30 JUNE 2020 
*funtion name : display_wishlist_report();
*
**/
function display_wishlist_report(){
    global $DB,$CFG,$USER,$OUTPUT;
    $systemcontext = context_system::instance();
    $cap1 = has_capability('local/course_details:viewallwishlist',$systemcontext);
    if($cap1){
        $string = "";
        $query = "select * from {course_categories} where id!=1 and visible=1"; 
        $get_category = $DB->get_records_sql($query);
        $i = 1;
        foreach($get_category as $row){
            $get_course = $DB->get_records('course',array('category'=>$row->id,'visible'=>1));
            foreach($get_course as $record){
                $course_name = $record->fullname;
                $cat_name = $row->name;
                $cid = $record->id;
                $sql = "select * from {local_wishlist} where course_id='".$cid."' and reg_data='Online' "; 
                $sql1 = "select * from {local_wishlist} where course_id='".$cid."' and reg_data='Offline' "; 
                $sql2 = "select * from {local_wishlist} where course_id='".$cid."' and reg_data='E-learning' ";
                $check_online = $DB->get_records_sql($sql);
                $check_offline  = $DB->get_records_sql($sql1);
                $check_elearning = $DB->get_records_sql($sql2);
                $online_count = count($check_online);
                $offline_count = count($check_offline);
                $elearning_count = count($check_elearning);
                $course = $CFG->wwwroot.'/course/view.php?id='.$cid;
                if($online_count!=0){
                    $online_count = $online_count;
                }else{
                    $online_count = '-----';
                }
                 if($offline_count!=0){
                    $offline_count = $offline_count;
                }else{
                    $offline_count = '-----';
                }
                 if($elearning_count!=0){
                    $elearning_count = $elearning_count;
                }else{
                    $elearning_count = '-----';
                }
                $string.="
                        <tr>
                        <td>".$i."</td>
                        <td>".$cat_name."</td>
                        <td><a href=".$course.">".$course_name."</a></td>
                        <td>".$online_count."</td>
                        <td>".$offline_count."</td>
                        <td>".$elearning_count."</td>
                        </tr>
                         ";
                         $i++;
            }
        }
    }
    $data_array  = ['report_string'=>$string,'date'=>date('d-m-Y')];
    $dataarray['link'] =  $CFG->wwwroot.'/local/course_details/styles.css';
    return $OUTPUT->render_from_template('local_course_details/wishlist_report',$data_array);
}

/*
function fp_benefits_catdetail() {
        global $PAGE,$OUTPUT;

        $usebenefits = $PAGE->theme->settings->usebenefits == 1;

        $benefit_title = (empty($PAGE->theme->settings->benefitsectiontitle)) ? false : format_text($PAGE->theme->settings->benefitsectiontitle);
        $benefit_desc = (empty($PAGE->theme->settings->benefitsectiondesc)) ? false : format_text($PAGE->theme->settings->benefitsectiondesc);

        $benefit1icon = (empty($PAGE->theme->settings->benefit1icon)) ? false : $PAGE->theme->settings->benefit1icon;
        $benefit1title = (empty($PAGE->theme->settings->benefit1title)) ? false : format_text($PAGE->theme->settings->benefit1title);
        $benefit1content = (empty($PAGE->theme->settings->benefit1content)) ? false : format_text($PAGE->theme->settings->benefit1content);


        $benefit2icon = (empty($PAGE->theme->settings->benefit2icon)) ? false : $PAGE->theme->settings->benefit2icon;
        $benefit2title = (empty($PAGE->theme->settings->benefit2title)) ? false : format_text($PAGE->theme->settings->benefit2title);
        $benefit2content = (empty($PAGE->theme->settings->benefit2content)) ? false : format_text($PAGE->theme->settings->benefit2content);

        $benefit3icon = (empty($PAGE->theme->settings->benefit3icon)) ? false : $PAGE->theme->settings->benefit3icon;
        $benefit3title = (empty($PAGE->theme->settings->benefit3title)) ? false : format_text($PAGE->theme->settings->benefit3title);
        $benefit3content = (empty($PAGE->theme->settings->benefit3content)) ? false : format_text($PAGE->theme->settings->benefit3content);

        $benefit4icon = (empty($PAGE->theme->settings->benefit4icon)) ? false : $PAGE->theme->settings->benefit4icon;
        $benefit4title = (empty($PAGE->theme->settings->benefit4title)) ? false : format_text($PAGE->theme->settings->benefit4title);
        $benefit4content = (empty($PAGE->theme->settings->benefit4content)) ? false : format_text($PAGE->theme->settings->benefit4content);

        $benefit5icon = (empty($PAGE->theme->settings->benefit5icon)) ? false : $PAGE->theme->settings->benefit5icon;
        $benefit5title = (empty($PAGE->theme->settings->benefit5title)) ? false : format_text($PAGE->theme->settings->benefit5title);
        $benefit5content = (empty($PAGE->theme->settings->benefit5content)) ? false : format_text($PAGE->theme->settings->benefit5content);

        $benefit6icon = (empty($PAGE->theme->settings->benefit6icon)) ? false : $PAGE->theme->settings->benefit6icon;
        $benefit6title = (empty($PAGE->theme->settings->benefit6title)) ? false : format_text($PAGE->theme->settings->benefit6title);
        $benefit6content = (empty($PAGE->theme->settings->benefit6content)) ? false : format_text($PAGE->theme->settings->benefit6content);


        $fp_benefits = [

        'usebenefits' => $usebenefits,
        'benefit_title' => $benefit_title,
        'benefit_desc' => $benefit_desc,

        'benefits' => array(

            array(
                'hasbenefit' => $benefit1title,
                'benefiticon' => $benefit1icon,
                'benefittitle' => $benefit1title,
                'benefitcontent' => $benefit1content,
            ) ,

            array(
                'hasbenefit' => $benefit2title,
                'benefiticon' => $benefit2icon,
                'benefittitle' => $benefit2title,
                'benefitcontent' => $benefit2content,
            ) ,

            array(
                'hasbenefit' => $benefit3title,
                'benefiticon' => $benefit3icon,
                'benefittitle' => $benefit3title,
                'benefitcontent' => $benefit3content,
            ) ,

            array(
                'hasbenefit' => $benefit4title,
                'benefiticon' => $benefit4icon,
                'benefittitle' => $benefit4title,
                'benefitcontent' => $benefit4content,
            ) ,

            array(
                'hasbenefit' => $benefit5title,
                'benefiticon' => $benefit5icon,
                'benefittitle' => $benefit5title,
                'benefitcontent' => $benefit5content,
            ) ,

            array(
                'hasbenefit' => $benefit6title,
                'benefiticon' => $benefit6icon,
                'benefittitle' => $benefit6title,
                'benefitcontent' => $benefit6content,
            ) ,

        ),

        ];

        return $OUTPUT->render_from_template('theme_maker/fp_benefits', $fp_benefits);
    }
*/




function course_details_page($cid){
    global $DB,$USER,$CFG,$OUTPUT;
    require_once ($CFG->dirroot . '/local/edu_ratings_reviews/lib.php');
    $course_details = $DB->get_record('course',array('id'=>$cid,'visible'=>1));
    $course_cat_details = $DB->get_record('course_categories',array('id'=>$course_details->category));
    $course_image = course_image_cd($course_details);

    $sql = 'SELECT cf.shortname,cf.name,cf.type,cd.value FROM {customfield_data} cd
            INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id
            WHERE cd.instanceid=?';
    $course_custom_data = $DB->get_records_sql($sql,array($cid));
    $cextra_templatecontext = array();
    if (!empty($course_details) && !empty($course_custom_data)) {

        $context = context_course::instance($cid);
        $users_count = count_enrolled_users($context);

       foreach ($course_custom_data as $key => $value) {

            if($value->type=='select'){
                $field_option_arr = filter_get_customfieldarray1($value->shortname);
                if(!empty($value->value)){
                    $cextra_templatecontext[$key] = $field_option_arr[$value->value];                
                }
            }else{
                $cextra_templatecontext[$key] = $value->value;
            }
        }
        $cextra_templatecontext['users_count'] = $users_count;
        $course_extra_fields = $OUTPUT->render_from_template('local_course_details/course_details_page_extra_fields', $cextra_templatecontext);
    }
    
    $related_course_card = get_related_course_card($course_details->category,4);
    $teachers = get_teachers($cid,1);
    $course_cost = get_course_cost($cid);
    $announcement = get_course_announcement($cid);
    $reviews_ratings = display_reviews_ratings($cid);
    // print_object($announcement);
    $courseraings = get_ratings($cid);
    $course_FAQ = fp_faq_course();
    $ratings_data = $DB->get_record('edupros_course_rating', array('courseid'=>$cid));
    if(!empty($ratings_data)){
        $total_no_of_ratings = $ratings_data->total_no_of_ratings;
    }else{        
        $total_no_of_ratings = 0;
    }

    $course_payment_btn = get_course_payment_btn($cid);
    $templatecontext = [
                            'teachers'=>$teachers,
                            'course_extra_fields'=>$course_extra_fields,
                            'related_course_card'=>$related_course_card,
                            // 'knowledge_partner'=>$cextra_templatecontext['knowledge_partner'],
                            // 'content_provider'=>$cextra_templatecontext['content_provider'],
                            'course_cost'=>$course_cost,
                            'course_payment_btn'=>$course_payment_btn,
                            'coursename'=>$course_details->fullname,
                            'coursecatid'=>$course_cat_details->id,
                            'coursecatname'=>$course_cat_details->name,
                            'courseimage'=>$course_image,
                            'description'=>$course_details->summary,
                            'curriculum'=>$cextra_templatecontext['curriculum'],
                            'announcement'=>$announcement,
                            'reviews_ratings'=>$reviews_ratings,
                            'overall_ratings'=>$courseraings['overall_ratings'],
                            'rating_star'=>$courseraings['rating_star'],
                            'total_no_of_ratings'=>$total_no_of_ratings,
                            'course_faq'=>$course_FAQ,

                        ];
    if(!empty($cextra_templatecontext['knowledge_partner'])){
        $templatecontext['knowledge_partner'] = $cextra_templatecontext['knowledge_partner'];
    }
    if(!empty($cextra_templatecontext['content_provider'])){
        $templatecontext['content_provider'] = $cextra_templatecontext['content_provider'];
    }
    return $OUTPUT->render_from_template('local_course_details/course_details_page', $templatecontext);


}

// enroll user to course
function enroll_user_to_course($cid,$userid){
    global $DB;
    $enrolplugin = enrol_get_plugin('manual');
    $plugininstance = $DB->get_record("enrol", array("courseid" =>$cid , "enrol" => "manual"));
    $timestart = time();
    $timeend   = 0;
    $enrolplugin->enrol_user($plugininstance, $userid, $plugininstance->roleid, $timestart, $timeend);
}

// get course payment button
function get_course_payment_btn($cid){
    global $DB,$CFG,$USER;
    $context = context_course::instance($cid);
    // $context = get_context_instance(CONTEXT_COURSE, $cid);
    
    $sql = 'SELECT cf.shortname,cf.name,cf.type,cd.value FROM {customfield_data} cd
            INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id
            WHERE cd.instanceid=? AND cf.shortname=?';
    $course_custom_data = $DB->get_record_sql($sql,array($cid,'cost_type'));
    // print_object($course_custom_data->value);
    // print_object($USER->id);

    if(isloggedin() && is_enrolled($context, $USER->id)){
        $url = new moodle_url($CFG->wwwroot . '/course/view.php',array('id'=>$cid));
        $btntxt = 'Continue Course';
    }else{
        if (!empty($course_custom_data) && $course_custom_data->value==2) {
            $url = new moodle_url($CFG->wwwroot . '/enrol/index.php',array('id'=>$cid));
            $course_cost = get_course_cost($cid);
            $btntxt = 'Get Course '.$course_cost;

        }else{
            $url = new moodle_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$cid,'enrolluser'=>'TRUE'));
            $course_cost = get_course_cost($cid);
            $btntxt = 'Get Course '.$course_cost;
        }
    }

    $btn = html_writer::tag('a', $btntxt, array('href'=>$url,'class'=>'btn btn-primary rounded'));

    return $btn;
}

// get course ratings star
function get_ratings($cid){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;

    $ratings_data = $DB->get_record('edupros_course_rating', array('courseid'=>$cid));

    $star_html = '';
    for ($i=1; $i<=5; $i++) { 
        
        $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
        
        $star_html .= html_writer::tag('span', $star);
    }
    
    $overall_ratings = 0;

    if (!empty($ratings_data)) {
        $overall_ratings = $ratings_data->overall_ratings;
        $star_html = '';
        for ($i=1; $i<=5; $i++) { 
            if($i<=$overall_ratings){
                $star = html_writer::tag('i', '', array('class'=>'fa fa-star'));
            }else{
                $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
            }
            $star_html .= html_writer::tag('span', $star);
        }
        

    }
    $ratings1 = ['overall_ratings'=>$overall_ratings,'rating_star'=>$star_html];
    return $ratings1;
}


// Display reviews and ratings for a course
function display_reviews_ratings($cid){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;

    $ratings_data = $DB->get_record('edupros_course_rating', array('courseid'=>$cid));

        $star_html = '';
        for ($i=1; $i<=5; $i++) { 
            
            $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
            
            $star_html .= html_writer::tag('span', $star);
        }
        $total_no_of_ratings = 0;
        $five_star = 0;
        $four_star = 0;
        $three_star = 0;
        $two_star = 0;
        $one_star = 0;
        $overall_ratings = 0;

        if (!empty($ratings_data)) {
            $overall_ratings = $ratings_data->overall_ratings;
            $star_html = '';
            for ($i=1; $i<=5; $i++) { 
                if($i<=$overall_ratings){
                    $star = html_writer::tag('i', '', array('class'=>'fa fa-star'));
                }else{
                    $star = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
                }
                $star_html .= html_writer::tag('span', $star);
            }
            $total_no_of_ratings = $ratings_data->total_no_of_ratings;
            $five_star = $ratings_data->five_star;
            $four_star = $ratings_data->four_star;
            $three_star = $ratings_data->three_star;
            $two_star = $ratings_data->two_star;
            $one_star = $ratings_data->one_star;


        }

        $review_ratings_temp_cobtext = [
                                            'overall_ratings'=>$overall_ratings,
                                            'overall_ratings_star'=>$star_html,
                                            'total_no_of_ratings'=>$total_no_of_ratings,
                                            'five_star'=>$five_star,
                                            'four_star'=>$four_star,
                                            'three_star'=>$three_star,
                                            'two_star'=>$two_star,
                                            'one_star'=>$one_star
                                        ];

        $reviews_data = $DB->get_records('edupros_course_review', array('courseid'=>$cid));

        $review_context_array = array();

        if (!empty($reviews_data)) {

            foreach ($reviews_data as $key_id => $review_data) {
                $user_image_itemid = $review_data->image;
                $user_img = local_edu_ratings_reviews_image($user_image_itemid,'review_image');
                $name_of_the_person = $review_data->name_of_the_person;
                $title = $review_data->title;
                $review_text = $review_data->review_text;
                $review_rating = $review_data->review_rating;
                $star_html1 = '';
                for ($i=1; $i<=5; $i++) { 
                    if($i<=$review_rating){
                        $star1 = html_writer::tag('i', '', array('class'=>'fa fa-star'));
                    }else{
                        $star1 = html_writer::tag('i', '', array('class'=>'fa fa-star-o'));
                    }
                    $star_html1 .= html_writer::tag('span', $star1);
                }
                $review_rating_star = $star_html1;

                $review_context = [
                                        'user_img'=>$user_img,
                                        'name_of_the_person'=>$name_of_the_person,
                                        'title'=>$title,
                                        'review_text'=>$review_text,
                                        'review_rating'=>$review_rating_star
                                    ];
                array_push($review_context_array, $review_context);

            }

            $review_ratings_temp_cobtext['reviews_data'] = $review_context_array;

        }

        

        $review_ratings = $OUTPUT->render_from_template('local_course_details/course_details_page_tabs_review_ratings', $review_ratings_temp_cobtext);
        return $review_ratings;
}
// get course cost
function get_course_cost($cid){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;
    $course_cost = '';
    $course_details = $DB->get_record('course',array('id'=>$cid,'visible'=>1));
    $sql = 'SELECT cf.shortname,cf.name,cf.type,cd.value FROM {customfield_data} cd
            INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id
            WHERE cd.instanceid=?';
    $course_custom_data = $DB->get_records_sql($sql,array($cid));
    if(!empty($course_custom_data) && !empty($course_custom_data['cost_type']) && $course_custom_data['cost_type']->value==1){
        $course_cost = 'Free';
    }else if(!empty($course_custom_data['price'])){
        $course_cost = $course_custom_data['price']->value;
    }
    return $course_cost;
}
// get list of teachers for perticuler course

function get_teachers($cid,$no_of_teachers){
    global $DB,$PAGE,$OUTPUT,$CFG,$USER;
    require_once('../../config.php');
    $context = context_course::instance($cid);
    $users = get_enrolled_users($context);
    $teachersdata_arr = array();
    foreach ($users as $uid=>$user_details) {
        if ($roles = get_user_roles($context, $uid)) {
                // print_object($roles);

            foreach ($roles as $role) {
                $role_shortname = $role->shortname;
            }
            // if($role_shortname=='teacher' || $role_shortname=='editingteacher'){
            if($role_shortname=='teacher' || $role_shortname=='editingteacher'){
                $teacherdata = $DB->get_record('user', array('id'=>$uid));
                $user_picture= new user_picture($teacherdata);
                $src=$user_picture->get_url($PAGE);
                $teacher_img = $src;
                $profile_url = new moodle_url($CFG->wwwroot . '/local/user_pages/profile.php',array('id'=>$uid));
                $teacherdata_arr = [
                                        'instructor_name'=> $teacherdata->firstname.' '.$teacherdata->lastname,
                                        'instructor_desc'=> $teacherdata->description,
                                        'instructor_image' => $teacher_img,
                                        'profile_url' => $profile_url
                                    ];
                array_push($teachersdata_arr, $teacherdata_arr);
            }
        }
    }

    $teachersdata_arr_new = array_slice($teachersdata_arr,0,$no_of_teachers);
    if(!empty($teachersdata_arr_new)){
        $instructor_templatecontext = ['instructor_data'=>$teachersdata_arr_new];
        $instructor = $OUTPUT->render_from_template('local_course_details/course_details_page_tabs_instructor', $instructor_templatecontext);
    }else{
       $instructor = ''; 
    }

    return $instructor;

}

// get course announcements
function get_course_announcement($cid){
    global $DB,$OUTPUT;

    $sql = 'SELECT f.intro
            FROM mdl_course_modules cm 
            INNER JOIN mdl_modules m ON m.id=cm.module
            INNER JOIN mdl_forum f ON f.id=cm.instance
            WHERE m.name=? AND cm.course=?';
    $announcement = $DB->get_record_sql($sql,array('forum',$cid));
    return $announcement->intro;

}



// get related course card
function get_related_course_card($course_catid,$no_of_cards){
    global $DB,$OUTPUT;
    $course_card = '';
    $course_details = $DB->get_records('course',array('category'=>$course_catid,'visible'=>1),'','*',0,$no_of_cards);

    foreach ($course_details as $cid => $course_detail) {
        $course_image = course_image_cd($course_detail);
        $course_cost = get_course_cost($cid);
        $courseraings = get_ratings($cid);
        $templatecontext = [
                                'cid'=>$course_detail->id,
                                'coursename'=>$course_detail->fullname,
                                'courseimage'=>$course_image,
                                'course_cost'=>$course_cost,
                                'rating_star'=>$courseraings['rating_star'],
                            ];
        $course_card .= $OUTPUT->render_from_template('local_course_details/cd_related_course_card', $templatecontext);
    }
    return $course_card;
}

//get customfield values as an array.
function filter_get_customfieldarray1($customname){
  global $DB;
  // $course_extradetail->shortname = $customname;

  $getconfigdata = $DB->get_record('customfield_field', array('shortname' => $customname));

  if (!empty($getconfigdata)) {
    $data['configdata']['options'] = $getconfigdata->configdata;

      $datafieldid = $getconfigdata->id; // this is the field id important we need for faster query once user submits the form

      $explode = explode(":",$getconfigdata->configdata);
      $exp3 = $explode[3];

      $explode22 = explode(",",$exp3);

      $finalstr = $explode22[0];
      $mystr =  str_replace('"',' ',$explode22[0]);
      $skuList = str_replace("\\r\\n", "\n", $mystr);
      $skuList = str_replace("\\n\\r", "\n", $skuList);
      $skuList = preg_split("/\n/", $skuList);
      $i = 1;
    }
    foreach ($skuList as $key => $value) {
        $skuList_new[$key+1]=$value;
    }
    return $skuList_new;
  }

// Display Course card

  function display_course_card($cid){
    global $DB,$OUTPUT,$CFG;
    $course_detail = $DB->get_record('course',array('id'=>$cid,'visible'=>1));

    if (!empty($course_detail)) {
        $course_cat_details = $DB->get_record('course_categories',array('id'=>$course_detail->category));
        $course_image = course_image_cd($course_detail);
        $course_cost = get_course_cost($cid);
        $course_link = new moodle_url($CFG->wwwroot . '/local/course_details/course_details.php',array('cid'=>$cid));
        $sql = 'SELECT cf.shortname,cd.value FROM {customfield_data} cd
            INNER JOIN {customfield_field} cf ON cd.fieldid=cf.id
            WHERE cd.instanceid=? AND cf.shortname=?';
        $duration_data = $DB->get_record_sql($sql,array($cid,'duration'));

        $templatecontext = [
                                'coursename'=>$course_detail->fullname,
                                'coursecatid'=>$course_cat_details->id,
                                'coursecatname'=>$course_cat_details->name,
                                'courseimage'=>$course_image,  
                                'course_link'=>$course_link,  
                            ];
        if (!empty($duration_data->value)) {
            $templatecontext['course_duration'] = $duration_data->value;
        }
        if (!empty($course_cost)) {
            $templatecontext['course_cost'] = $course_cost;
        }
        return $OUTPUT->render_from_template('local_course_details/course_card', $templatecontext);

    }else{
        return false;
    }
  }

  // get FAQ for the courseimage
 function fp_faq_course() {
        global $PAGE,$OUTPUT;

        $usefaq = $PAGE->theme->settings->usefaq == 1;
        
        $faqsectiontitle = (empty($PAGE->theme->settings->faqsectiontitle)) ? false : format_text($PAGE->theme->settings->faqsectiontitle);


        $faq1title = (empty($PAGE->theme->settings->faq1title)) ? false : format_text($PAGE->theme->settings->faq1title);
        $faq1content = (empty($PAGE->theme->settings->faq1content)) ? false : format_text($PAGE->theme->settings->faq1content);
        
        
        $faq2title = (empty($PAGE->theme->settings->faq2title)) ? false : format_text($PAGE->theme->settings->faq2title);
        $faq2content = (empty($PAGE->theme->settings->faq2content)) ? false : format_text($PAGE->theme->settings->faq2content);
        
        $faq3title = (empty($PAGE->theme->settings->faq3title)) ? false : format_text($PAGE->theme->settings->faq3title);
        $faq3content = (empty($PAGE->theme->settings->faq3content)) ? false : format_text($PAGE->theme->settings->faq3content);
        
        $faq4title = (empty($PAGE->theme->settings->faq4title)) ? false : format_text($PAGE->theme->settings->faq4title);
        $faq4content = (empty($PAGE->theme->settings->faq4content)) ? false : format_text($PAGE->theme->settings->faq4content);
        
        $faq5title = (empty($PAGE->theme->settings->faq5title)) ? false : format_text($PAGE->theme->settings->faq5title);
        $faq5content = (empty($PAGE->theme->settings->faq5content)) ? false : format_text($PAGE->theme->settings->faq5content);
        
        $faq6title = (empty($PAGE->theme->settings->faq6title)) ? false : format_text($PAGE->theme->settings->faq6title);
        $faq6content = (empty($PAGE->theme->settings->faq6content)) ? false : format_text($PAGE->theme->settings->faq6content);
        
        $faq7title = (empty($PAGE->theme->settings->faq7title)) ? false : format_text($PAGE->theme->settings->faq7title);
        $faq7content = (empty($PAGE->theme->settings->faq7content)) ? false : format_text($PAGE->theme->settings->faq7content);
        
        $faq8title = (empty($PAGE->theme->settings->faq8title)) ? false : format_text($PAGE->theme->settings->faq8title);
        $faq8content = (empty($PAGE->theme->settings->faq8content)) ? false : format_text($PAGE->theme->settings->faq8content);
        
        $faq9title = (empty($PAGE->theme->settings->faq9title)) ? false : format_text($PAGE->theme->settings->faq9title);
        $faq9content = (empty($PAGE->theme->settings->faq9content)) ? false : format_text($PAGE->theme->settings->faq9content);
        
        $faq10title = (empty($PAGE->theme->settings->faq10title)) ? false : format_text($PAGE->theme->settings->faq10title);
        $faq10content = (empty($PAGE->theme->settings->faq10content)) ? false : format_text($PAGE->theme->settings->faq10content);
        
        $faqsectionbuttontext = (empty($PAGE->theme->settings->faqsectionbuttontext)) ? false : format_text($PAGE->theme->settings->faqsectionbuttontext);
        $faqsectionbuttonurl = (empty($PAGE->theme->settings->faqsectionbuttonurl)) ? false : $PAGE->theme->settings->faqsectionbuttonurl;
        $faqsectionbuttonurlopennew = $PAGE->theme->settings->faqsectionbuttonurlopennew== 1;


        $fp_faq = [

        'usefaq' => $usefaq,
        'faqsectiontitle' => $faqsectiontitle,
        
        
        'faq5title' => $faq5title,
        'faq6title' => $faq6title,
        'faq7title' => $faq7title,
        'faq8title' => $faq8title,
        'faq9title' => $faq9title,
        'faq10title' => $faq10title,
        
        
        'faq5content' => $faq5content,
        'faq6content' => $faq6content,
        'faq7content' => $faq7content,
        'faq8content' => $faq8content,
        'faq9content' => $faq9content,
        'faq10content' => $faq10content,
       


        ];

        return $OUTPUT->render_from_template('theme_maker/fp_faq', $fp_faq);
    }

// display course filter page
function display_course_filter_page($catid=null){
    global $DB,$OUTPUT,$CFG;
    $custom_course_filter_option = '';
    $custom_course_filter_option .= get_custom_course_filter_option('course_categories',$catid);
    $custom_course_filter_option .= get_custom_course_filter_option('level');
    $custom_course_filter_option .= get_custom_course_filter_option('language');
    $custom_course_filter_option .= get_custom_course_filter_option('cost_type');
    $custom_course_filter_option .= get_custom_course_filter_option('affiliation_name');
    $custom_course_filter_option .= get_custom_course_filter_option('delivery_type');
    $custom_course_filter_option .= get_custom_course_filter_option('certificate_available');
    $templatecontext = [
                            'custom_course_filter_option'=>$custom_course_filter_option
                        ];
    return $OUTPUT->render_from_template('local_course_details/course_filter_page', $templatecontext);
}

// get custom filter area
function get_custom_course_filter_option($shortname,$catid=null){
    global $DB,$OUTPUT,$CFG;
    if($shortname!='course_categories'){
        $custom_field_detail = $DB->get_record('customfield_field',array('shortname'=>$shortname));
        if (!empty($custom_field_detail) && $custom_field_detail->type=='select') {
            $carray = array();
            $customfieldarray = filter_get_customfieldarray1($shortname);
            foreach ($customfieldarray as $key => $value) {
                $cfa = array('field_value'=>$key,'field_lable'=>$value,'fieldname'=>$shortname);
                array_push($carray, $cfa);
            }
            $templatecontext = [
                                    'shortname'=>$shortname,
                                    'filter_name'=>$custom_field_detail->name,
                                    'custom_field_data'=>$carray
                                ];
            // print_object($templatecontext);

            return $OUTPUT->render_from_template('local_course_details/custom_course_filter_option', $templatecontext);
        }else{
            return false;
        }
    }else{
        $course_categories = $DB->get_records('course_categories',array('visible'=>1));
        if (!empty($course_categories)) {
            $carray = array();
            foreach ($course_categories as $key => $value) {
                $cfa = array('field_value'=>$key,'field_lable'=>$value->name,'fieldname'=>$shortname);
                if ($catid==$key) {
                    $cfa['checked']=1;
                }
                array_push($carray, $cfa);
            }
            $templatecontext = [
                                    'shortname'=>$shortname,
                                    'filter_name'=>'Course Categories ',
                                    'custom_field_data'=>$carray
                                ];
            // print_object($templatecontext);

            return $OUTPUT->render_from_template('local_course_details/custom_course_filter_option', $templatecontext);
        }else{
            return false;
        }
    }
    
}