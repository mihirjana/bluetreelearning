<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_course_details
 * @copyright  Abhijit Sen<abhijitsen@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2019 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['pluginname'] = 'Course Details';
$string['coursedetailsnotfound'] = '<strong>Oops!</strong> Course details not found.';
$string['nocourse'] = 'No course available.';
$string['view_details'] = 'VIEW DETAILS';
$string['view_all_courses'] = 'VIEW ALL INSTITUTIONS';
$string['view_all_courses1'] = 'VIEW ALL COURSES';
$string['view_all_institutions'] = 'VIEW ALL INSTITUTIONS';
$string['add_inst_extra_settings'] = 'Add Institutions Extra Settings';
$string['update_inst_extra_settings'] = 'Update Institutions Extra Settings';
$string['institutions_inst_extra_settings'] = 'Institutions Extra Settings';
$string['loginheader'] = 'Login Page Settings';
$string['std_login_pg_title'] = 'Student Login Page Title';
$string['std_login_pg_color'] = 'Student Login Page Color';
$string['std_login_pg_desc'] = 'Student Login Page Short Description';
$string['std_login_pg_logo'] = 'Student Login Page Logo';
$string['std_login_pg_bgimg'] = 'Student Login Page Background Image';

// newsletter
$string['subscribe'] = 'Join';
$string['toournewsletter'] = 'Join Our Community';
$string['youremail'] = 'Email';

//manju: Added for category update page.
$string['pleaseselectcategory'] = 'Please Select Institution';
$string['selectcategory'] = 'Select Institution';
$string['selectcategorydesc'] = 'Institution Description';
$string['selectcategoryimage'] = 'Institution Image';
$string['selectcategorylink'] = 'Institution Link';
$string['categorydisplay'] = 'Institution Display Name';
$string['selectcategorylogo'] = 'Institution Logo';
$string['submit'] = 'Submit';
$string['slno'] = 'Sl No';
$string['categoryname'] = 'Institution Name';
$string['catdescription'] = 'Description';
$string['catimage'] = 'Image';
$string['catlink'] = 'Link';
$string['catlogo'] = 'Logo';
$string['edit'] = 'Edit';
$string['updatesuccess'] = 'Successfully Updated!';
$string['delete'] = 'Delete';
$string['deleteheadding'] = 'Delete Institution Info';
$string['deletecatinfo'] = 'Delete Institution Info';
$string['delsuccessmsg'] = 'Deleted Successfully';
$string['delerrormsg'] = 'Not able to delete the info';
$string['catsupportemail'] = 'Institution Support Email Id';

// Abhijit: institution page
$string['institution_list_section_title'] = 'Partner Institutions ';
$string['institution_list_section_desc'] = 'View the institutions for further information.';
$string['studentlogin'] = 'Student Login';


//  corporate contact form
$string['getmoreinformation'] = 'GET MORE INFORMATION';
$string['getmoreinformationdesc']='Enter your information below to view the course prospectus';
$string['required'] = '<span class="text-danger">*</span> Required fields.';
$string['firstname'] = 'First name <span class="text-danger">*</span>';
$string['lastname'] = 'Last name <span class="text-danger">*</span>';
$string['email'] = 'Email <span class="text-danger">*</span>';
$string['continue'] = 'Continue';
$string['consenting']='By consenting to receive communications, you agree to the use of your data as described in our privacy policy. You may opt out of receiving communications at any time.';
$string['contactcorporate'] = 'CONTACT OUR CORPORATE ACCOUNTS TEAM';
$string['contactcorporatedesc'] = 'Get in touch and start the process of investing in your organisation\'s';
$string['contactcorporatephone'] = 'Tel: +1-613-800-5579';
$string['contactnumber'] = 'Contact Number <span class="text-danger">*</span>';
$string['corporatename'] = 'Corporate Name <span class="text-danger">*</span>';
$string['upskills'] = 'Number of staff to upskill <span class="text-danger">*</span>';
$string['additionalcomments'] = 'Additional Comments <span class="text-danger">*</span>';
$string['textline1'] = 'Yes. I want to receive additional information about  {$a} . Please, contact me.';
$string['textline2'] = 'Learn more about other educational program that  {$a} offers that may be of interest to you. ';

// Abhijit : For Course details page
$string['overview'] = 'OVERVIEW';
$string['curriculum'] = 'CURRICULUM';
$string['instructor'] = 'INSTRUCTOR';
$string['reviews'] = 'REVIEWS';
$string['c_info'] = 'Course information';
$string['reg_close'] = 'REGISTRATION CLOSES:';
$string['c_length'] = 'LENGTH:';
$string['c_start'] = 'COURSE STARTS:';
$string['c_effort'] = 'EFFORT:';
$string['c_price'] = 'PRICE:';
$string['c_univercity'] = 'INSTITUTION:';
$string['c_language'] = 'LANGUAGE:';
$string['c_level'] = 'LEVEL:';
$string['request_a_call'] = 'REQUEST A CALL';
$string['when_do_start'] = 'When do I start?';
$string['next_available'] = 'Our next available course starts';
$string['download_procpectus'] = 'DOWNLOAD COURSE PROSPECTUS';
$string['register_now'] = 'JOIN NOW';
$string['nomatchfound'] = '<strong>Oops!</strong> No match found.';
$string['searchcourse'] = 'Search Courses';
$string['buy'] = 'Buy Course';
$string['goto'] = 'Go To Course';
$string['enroll_now'] = 'Enroll now';
$string['now_open'] = 'NOW OPEN!';
$string['download_brochure'] = 'DOWNLOAD COURSE BROCHURE';
$string['something_wrong'] = 'Something went wrong, please try again!';
$string['download_prospactus_text'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when';
$string['get_moreinfo_email_subject'] = 'New Get More Info Request From {$a}';


//added by manoj for wishlist
$string['reg_online_button_name'] = 'REGISTER FOR ONLINE';
$string['reg_offline_button_name'] = 'REGISTER FOR OFFLINE';
$string['reg_elearning_button_name'] = 'REGISTER FOR E-LEARNING';
$string['register_cv_button_name'] = 'Fill up registration details';

$string['reg_online_msg'] = 'Already registered for Online, any query regarding registration please contact: 8595887700';
$string['reg_offline_msg'] = 'Already registered for Offline, any query regarding registration please contact: 8595887700';
$string['reg_elearning_msg'] = 'Already registered for E-learning, any query regarding registration please contact: 8595887700';
$string['course_details:viewallwishlist'] = 'Viewallwishlist';
$string['course_details:wishlist'] = 'Wishlist';
$string['title'] = 'Wishlist';
$string['sno'] = 'Sno';
$string['fullname'] = 'FullName';
$string['email'] = 'Email';
$string['course_name'] = 'Course Name';
$string['wishlist_date'] = 'Wishlist Date';
$string['modal_succss'] = 'Successfully added to wish list. Our Team will contact you soon.';
$string['wishlist_view'] = 'View Wishlist';
$string['reg_type'] = 'Register Type';

//wishlist Report//
$string['date'] = 'Date Of Report';
$string['cat'] = 'Category Name';
$string['course'] = 'Course Name';
$string['online'] = 'No Of Person Online';
$string['offline'] = 'No Of Person Offline';
$string['elearning'] = 'No Of Person E-learning';
$string['wishlist_report_title'] = 'Wishlist Report';

