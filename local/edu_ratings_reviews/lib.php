<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Handles uploading files
 *
 * @package    local_edu_ratings_reviews
 * @copyright  Manoj Prabahar<manojprabahar@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */


if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page.
}
global $CFG,$USER;
/** Manoj
* Date : 11 MAY 2020
*this function is used to insert_ratings_data
*
**/
function insert_ratings_data($data){
    global $CFG,$USER,$DB;
    $insert_ratings_object = new stdClass();
    $insert_ratings_object->courseid = $data->cid;
    $insert_ratings_object->overall_ratings = $data->overall_ratings;
    $insert_ratings_object->total_no_of_ratings = $data->total_no_of_ratings;
    $insert_ratings_object->five_star = $data->five_star;
    $insert_ratings_object->four_star = $data->four_star;
    $insert_ratings_object->three_star = $data->three_star;
    $insert_ratings_object->two_star = $data->two_star;
    $insert_ratings_object->one_star = $data->one_star;
    $insert_ratings_object->timecreated = time();
    $insert_datas = $DB->insert_record('edupros_course_rating',$insert_ratings_object);
    return $insert_datas;
}

function update_ratings_data($data){
    global $CFG,$USER,$DB;
    $insert_ratings_object = new stdClass();
    $insert_ratings_object->id = $data->id;
    $insert_ratings_object->courseid = $data->cid;
    $insert_ratings_object->overall_ratings = $data->overall_ratings;
    $insert_ratings_object->total_no_of_ratings = $data->total_no_of_ratings;
    $insert_ratings_object->five_star = $data->five_star;
    $insert_ratings_object->four_star = $data->four_star;
    $insert_ratings_object->three_star = $data->three_star;
    $insert_ratings_object->two_star = $data->two_star;
    $insert_ratings_object->one_star = $data->one_star;
    $insert_ratings_object->timecreated = time();
    $insert_datas = $DB->update_record('edupros_course_rating',$insert_ratings_object);
    return $insert_datas;
}
/** Manoj
* Date : 11 MAY 2020
*this function is used to insert_reviews_data
*
**/
function insert_reviews_data($data){
    global $CFG,$USER,$DB;
    $insert_reviews_object = new stdClass();
    $insert_reviews_object->courseid = $data->cid;
    $insert_reviews_object->image = $data->review_image;
    $insert_reviews_object->name_of_the_person = $data->name;
    $insert_reviews_object->title = $data->title;
    $insert_reviews_object->review_text = $data->review_text;
    $insert_reviews_object->review_rating = $data->review_rating;
    $insert_reviews_object->timecreated = time();
    $insert_datas = $DB->insert_record('edupros_course_review',$insert_reviews_object);
    return $insert_datas;
}
/** Manoj
* Date : 11 MAY 2020
*this function is used to update_reviews_data
*
**/
function update_reviews_data($data){
    global $CFG,$USER,$DB;
    $update_reviews_object = new stdClass();
    $update_reviews_object->id = $data->editid;
    $update_reviews_object->courseid = $data->cid;
    $update_reviews_object->image = $data->review_image;
    $update_reviews_object->name_of_the_person = $data->name;
    $update_reviews_object->title = $data->title;
    $update_reviews_object->review_text = $data->review_text;
    $update_reviews_object->review_rating = $data->review_rating;
    $update_reviews_object->timecreated = time();
    $update_datas = $DB->update_record('edupros_course_review',$update_reviews_object


);
    return $update_datas;
}
/** Manoj
* Date : 11 MAY 2020
*this function is used for print the heading of page
*
**/
function get_local_plugin_heading($headingtext,$subheadingtext,$buttonlink,$buttontext){
    global $CFG;
    $headingdetails = html_writer::start_tag('div',  array('class' => 'row'));
    $headingdetails .= html_writer::start_tag('div',  array('class' => 'col-md-6'));
    $headingdetails .= html_writer::start_tag('h4');
    $headingdetails .= $headingtext;
    $headingdetails .= html_writer::end_tag('h4');
    $headingdetails .= html_writer::start_tag('p');
    $headingdetails .= $subheadingtext;
    $headingdetails .= html_writer::end_tag('p');
    $headingdetails .= html_writer::end_tag('div');
    $headingdetails .= html_writer::end_tag('div');
    return $headingdetails;
}
/** Manoj
* Date : 12 MAY 2020 
*function name : list_datas($courseid);
*return an $getdetailes object 
*
**/
function list_datas($courseid){
    global $DB;
    $sql = 'SELECT * from {edupros_course_review} where courseid="'.$courseid.'" ';
    $getdetailes = $DB->get_records_sql($sql);
    return $getdetailes;
}
/** Manoj
* Date : 12 MAY 2020 
*funtion name : create_table();
*parameter : $data means form object
*return an $tabledisplay object 
*
**/
function create_review_table($data){
    global $DB;
    $tabledisplay = '';
    $tabledisplay .= html_writer::start_tag('div',  array('class' => 'table-responsive'));
    $table = new html_table();
    $table->head = (array) get_strings(array('sno','course_name','name','title','date','actionbtn'),'local_edu_ratings_reviews');
    $table->id =  'example1';
    $i = 1;
    foreach ($data as $row) {
       $actiontbtn = create_review_action_button($row->id,$row->courseid);
       $get_course = $DB->get_record('course',array('id'=>$row->courseid));
       $data = date('Y-m-d',$row->timecreated);
        $table->data[] = array($i++,$get_course->fullname,$row->name_of_the_person,$row->title,$data,$actiontbtn);
    }
    $tabledisplay .= html_writer::table($table);
    $tabledisplay .= html_writer::end_tag('div');

    return $tabledisplay;
}
/** Manoj
* Date : 12 MAY 2020  
*funtion name : create_review_action_button();
*parameter : $data means form object
*return an $array object 
*
**/
function create_review_action_button($data,$cid)
{
    global $CFG;
    $array = "";
    $array .= html_writer::start_tag('div',array('class'=>'dropdown menushow'));
    $array.=html_writer::start_tag('a',array('href' =>  '#','class'=>'btn btn-primary dropdown-toggle','role'=>"button", 'id'=>"dropdownMenuLink" ,'data-toggle'=>"dropdown", 'aria-haspopup'=>"true" ,'aria-expanded'=>"false"));
    $array .= get_string('actionbtn','local_edu_ratings_reviews');
    $array.=html_writer::end_tag('a');

    $array .= html_writer::start_tag('div', array('class' => 'dropdown-menu','aria-labelledby'=>"dropdownMenuLink"));

    $array .= html_writer::start_tag('a',array('href' => $CFG->wwwroot.'/local/edu_ratings_reviews/create_reviews.php?review_id='.$data.'&cid='.$cid, 'class'=>"dropdown-item"));
    $array .= html_writer::start_tag('i',  array('class' => 'fa fa-edit iconspad'));
    $array .= html_writer::end_tag('i');
    $array .= get_string('edit','local_edu_ratings_reviews');
    $array .= html_writer::end_tag('a'); 


    $array .= html_writer::start_tag('a',array('href' => $CFG->wwwroot.'/local/edu_ratings_reviews/delete.php?review_id='.$data.'&flag=1&cid='.$cid, 'class'=>"dropdown-item"));
    $array .= html_writer::start_tag('i',  array('class' => 'fa fa-edit iconspad'));
    $array .= html_writer::end_tag('i');
    $array .= get_string('delete','local_edu_ratings_reviews');
    $array .= html_writer::end_tag('a'); 
    $array .= html_writer::end_tag('div');
    $array .= html_writer::end_tag('div');

    return $array;
}
/** Manoj
* Date : 12 MAY 2020  
*funtion name : delete_file_data();
*parameter : $data means form paticular id
*return an $delete
*
**/
function delete_review_data($data)
{
    global $DB;
    $sql = 'delete from {edupros_course_review} where id='.$data.' ';
    $delete = $DB->execute($sql);
    return $delete;
}

function local_edu_ratings_reviews_extend_settings_navigation(settings_navigation $nav, $context) {
    global $CFG;

    if ($context->contextlevel >= CONTEXT_COURSE and ($branch = $nav->get('courseadmin'))
        and has_capability('moodle/course:update', $context)) {
 
        $url = new moodle_url($CFG->wwwroot . '/local/edu_ratings_reviews/create_ratings.php', array('cid' => $context->instanceid));
        $branch->add(get_string('add_rating', 'local_edu_ratings_reviews'), $url, $nav::TYPE_CONTAINER, null, 'add_rating' . $context->instanceid, new pix_icon('i/settings', ''));
        $url1 = new moodle_url($CFG->wwwroot . '/local/edu_ratings_reviews/create_reviews.php', array('cid' => $context->instanceid));
        $branch->add(get_string('add_review', 'local_edu_ratings_reviews'), $url1, $nav::TYPE_CONTAINER, null, 'add_review' . $context->instanceid, new pix_icon('i/settings', ''));
        $url2 = new moodle_url($CFG->wwwroot . '/local/edu_ratings_reviews/list.php', array('cid' => $context->instanceid));
        $branch->add(get_string('review_list', 'local_edu_ratings_reviews'), $url2, $nav::TYPE_CONTAINER, null, 'review_list' . $context->instanceid, new pix_icon('i/settings', ''));
    }
 }



    function local_edu_ratings_reviews_image($itemid,$filearea){
    // global $DB,$CFG;
    if(!empty($itemid)){
        
        global $USER;

     // $context = context_system::instance();
     //    $contextid = $context->contextlevel;
$context = context_user::instance(2);
        $component = 'local_edu_ratings_reviews';

        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, $component, $filearea, $itemid,'sortorder', false);

        if(!empty($files)){
            $count = count($files);
            if($count>1)
            {
                  $url2 =[];
                foreach($files as $file) {
                    $file->get_filename();
                    $url2[] = moodle_url::make_pluginfile_url(
                        $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
                    );

                }
            }
            else
            {
                 $url2 ="";
                foreach($files as $file) {
                    $file->get_filename();
                    $url2 = moodle_url::make_pluginfile_url(
                        $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
                    );

                }
            }

            return $url2;
        }
    }
}



    // function local_course_details_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
function local_edu_ratings_reviews_pluginfile($course, $birecordorcm, $context, $filearea, $args, $forcedownload, array $options = array()) {

    if ($filearea == 'review_image') {
        
        // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.
 
   $fs = get_file_storage();

    $filename = array_pop($args);
    $filepath = $args ? '/' . implode('/', $args) . '/' : '/';

    if (!$file = $fs->get_file($context->id, 'local_edu_ratings_reviews', $filearea, $itemid, $filepath, $filename) or $file->is_directory()) {
        send_file_not_found();
    }

    \core\session\manager::write_close();
    send_stored_file($file, null, 0, $forcedownload, $options);


    }

}


//  function local_edu_ratings_reviews_image($itemid){
//     global $DB,$CFG;
//     if(!empty($itemid)){
//          $context = context_system::instance();
//         $contextid = $context->contextlevel; 
//         global $USER;
//         $component = 'local_edu_ratings_reviews';
//         $filearea = 'review_image';   
//         $fs = get_file_storage();
//         $files = $fs->get_area_files($contextid, $component, $filearea, $itemid);
//         if(!empty($files)){
//             $url2 ='';
//             foreach($files as $file) {
//                 $file->get_filename();
//                 $url2 = moodle_url::make_pluginfile_url(
//                 $file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
//                 );
//             }
//             return $url2;
//         }    
//     }  
// }
// function local_edu_ratings_reviews_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {

 
//     // Make sure the filearea is one of those used by the plugin.
//     if ($filearea !== 'review_image') {
//         return false;
//     }
 
//     // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
//     require_login();

 
//     // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
//     $itemid = array_shift($args); // The first item in the $args array.
 
//     // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
//     // user really does have access to the file in question.
 
//     // Extract the filename / filepath from the $args array.
//     $filename = array_pop($args); // The last item in the $args array.
//     if (!$args) {
//         $filepath = '/'; // $args is empty => the path is '/'
//     } else {
//         $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
//     }
 
//     // Retrieve the file from the Files API.
//     $fs = get_file_storage();
//     $file = $fs->get_file($context->id, 'local_edu_ratings_reviews', $filearea, $itemid, $filepath, $filename);
//     if (!$file) {
//         return false; // The file does not exist.
//     }
 
//     // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering. 
//     // From Moodle 2.3, use send_stored_file instead.
     
//      // send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
    
//     $forcedownload = 1;
//     send_file($file, $file->get_filename(), true, $forcedownload, $options);
    
// }


