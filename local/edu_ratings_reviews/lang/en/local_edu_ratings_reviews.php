<?php 
$string['pluginname'] = 'Ratings And Reviews';
$string['create_ratings'] = 'Create Rating';
$string['create_reviews'] = 'Create Review';
$string['create_review_form'] = 'Create Review Form';
$string['edit_review_form'] = 'Edit Review Form';
$string['pluginheading1'] = 'Rating Create Form';
$string['five_star'] = 'Five Star';
$string['four_star'] = 'Four Star';
$string['three_star'] = 'Three Star';
$string['two_star'] = 'Two Star';
$string['one_star'] = 'One Star';
$string['savebutton'] = 'Submit';
$string['local'] = 'local';
$string['name'] = 'Name Of The Person';
$string['review_image'] = 'Review Image';
$string['title'] = 'Title';
$string['review_text'] = 'Review Text';
$string['manage_datas'] = 'Manage Review';
$string['messagedisplay'] = 'Data deteled successfully.';
$string['createscss'] = 'Data created successfully';
$string['upadatescss'] = 'Data updated successfully';
$string['promsgdisplay'] = '<b>Sorry!</b> there are no data available.';
$string['course_name'] = 'Course Name';
$string['date'] = 'Date';
$string['edit'] = 'Edit Data';
$string['delete'] = 'Delete Data';
$string['sno'] = 'Sno';
$string['actionbtn'] = 'Action';
$string['deletedata'] = 'Are you sure you want to completely delete this review data and all of the data related to this review data<br><br>';
$string['deleteheadding'] = 'Delete data';
$string['review_rating'] = 'Review Rating';
$string['add_review'] = 'Add Review';
$string['add_rating'] = 'Add Rating';
$string['savebutton'] = 'Submit';
$string['review_list'] = 'Review List';
$string['overall_ratings'] = 'Overall Ratings';
$string['total_no_of_ratings'] = 'Total Number of people rated';













