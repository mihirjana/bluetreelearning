<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package
 * @copyright
 * @copyright
 * @license
**/
require_once('../../config.php');
defined('MOODLE_INTERNAL') || die();
global $CFG;
global $PAGE,$OUTPUT;

$title ="Contact us";

$context = context_system::instance();
$contextid = $context->contextlevel;
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/contact/contact.php');
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/contact/custom.js'));
echo $OUTPUT->header();
$html = "";

$html  .= html_writer::start_tag('div',array('class'=>'container'));//container start//
	$html .=html_writer::start_tag('div',array('class'=>'row p-5'));//row start

		$html .=html_writer::start_tag('div',array('class'=>'col-md-12'));//col-md-6 start
			$html .='<div class="section__title text-center">
							<h2 class="title__line">Message us</h2>
							<p>Write message</p>
						</div>';
			$html .=html_writer::start_tag('form',array('id'=>'contactusform','class'=>'mt-4'));//form start//

			$html .=html_writer::start_tag('div',array('class'=>'form-group'));
			$html .=html_writer::start_tag('label',array('for'=>'name'));
			$html .='Your Name *';
			$html .=html_writer::end_tag('label');
			$html .=html_writer::empty_tag('input',array('type'=>'hidden','name'=>'contact_us','value'=>'1'));
			$html .=html_writer::empty_tag('input',array('type'=>'text','name'=>'name','class'=>'form-control','id'=>'name','style'=>'','required'));
			$html .=html_writer::start_tag('span',array('id'=>'name_error'));
			$html .=html_writer::end_tag('span');
			$html .=html_writer::end_tag('div');

			$html .=html_writer::start_tag('div',array('class'=>'form-group'));
			$html .=html_writer::start_tag('label',array('for'=>'email'));
			$html .='Email *';
			$html .=html_writer::end_tag('label');
			$html .=html_writer::empty_tag('input',array('type'=>'email','name'=>'email','class'=>'form-control','id'=>'email', 'style'=>'' ,'onblur'=>'email_check();','required'));
			$html .=html_writer::start_tag('span',array('id'=>'email_info'));
			$html .=html_writer::end_tag('span');
			$html .=html_writer::end_tag('div');

			$html .=html_writer::start_tag('div',array('class'=>'form-group'));
			$html .=html_writer::start_tag('label',array('for'=>'email'));
			$html .='Phone no *';
			$html .=html_writer::end_tag('label');
			$html .=html_writer::empty_tag('input',array('type'=>'text','name'=>'phone','class'=>'form-control','id'=>'phone', 'style'=>'' ,'required'));
			$html .=html_writer::start_tag('span',array('id'=>'phone_info'));
			$html .=html_writer::end_tag('span');
			$html .=html_writer::end_tag('div');

			$html .=html_writer::start_tag('div',array('class'=>'form-group'));
			$html .=html_writer::start_tag('label',array('for'=>'subject'));
			$html .='Message *';
			$html .=html_writer::end_tag('label');
			$html .=html_writer::start_tag('textarea',array('name'=>'subject','class'=>'form-control','id'=>'subject','style'=>'height:100px;','required'));
			$html .=html_writer::end_tag('textarea');
				$html .=html_writer::start_tag('span',array('id'=>'subject_error'));
			$html .=html_writer::end_tag('span');
			$html .=html_writer::end_tag('div');

			$html .=html_writer::start_tag('button',array('type'=>'submit','class'=>'btn btn-primary'));
			$html .='SEND';
			$html .=html_writer::end_tag('button');
            $html .=html_writer::start_tag('span',array('id'=>'message_info','class'=>''));
            $html .=html_writer::end_tag('span');
			$html .=html_writer::end_tag('form');//form end//
		$html .=html_writer::end_tag('div');//col-md-6 end//
	$html .=html_writer::end_tag('div');//row end
$html  .= html_writer::end_tag('div');//container start//

echo $OUTPUT->render_from_template('local_contact/contact_section',array('contact_form'=>$html));


echo $OUTPUT->footer();
