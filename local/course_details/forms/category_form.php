<?php
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// This file is part of Moodle - http://moodle.org/                      //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//                                                                       //
// Moodle is free software: you can redistribute it and/or modify        //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation, either version 3 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// Moodle is distributed in the hope that it will be useful,             //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details.                          //
//                                                                       //
// You should have received a copy of the GNU General Public License     //
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.       //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/**
 * Form for community search
 *
 * @package    local_course_details
 * @author     Manjunath B K <manjunaathbk@elearn10.com>
 * @license    Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @copyright  http://www.lmsofindia.com 2017 or later
 */
global $DB;
require_once($CFG->libdir . '/formslib.php');


class local_course_details_form extends moodleform {

    public function set_data($defaults) {
        $context = context_system::instance();

        $catdescription_draftideditor = file_get_submitted_draft_itemid('catdescription');
        $defaults->catdescription['text'] = file_prepare_draft_area($catdescription_draftideditor, $context->id,
            'local_course_details', 'catdescription', 0, array('subdirs' => true), $defaults->catdescription['text']);
        $defaults->catdescription['itemid'] = $catdescription_draftideditor;
        $defaults->catdescription['format'] = FORMAT_HTML;

        return parent::set_data($defaults);
    }


    public function definition() {
        global $DB,$CFG,$PAGE,$USER;
        $context = context_system::instance();
        $categoryid = optional_param('catid','',PARAM_INT);
        $mform =& $this->_form;
        if($categoryid){
            $formheader = get_string('update_inst_extra_settings', 'local_course_details');

        }else{
            $formheader = get_string('add_inst_extra_settings', 'local_course_details');
        }

        $mform->addElement('html', html_writer::tag('h2', $formheader,array('class'=>'mb-5 border-bottom pb-3')));
        $mform->addElement('hidden', 'id',$categoryid);
        $mform->setType('id', PARAM_INT);
        //moodle main categories dropdown.
        $categories = $DB->get_records('course_categories',array('parent'=>0,'visible'=>1));
        $category_options=[];
        $selectcategory = get_string('pleaseselectcategory', 'local_course_details');
        $category_options[''] = $selectcategory;
        foreach ($categories as $category) {
            if (!empty($categoryid)) {
                $category_options[$category->id] = $category->name;
            }else{
                $catexist = $DB->record_exists('local_course_details',array('catid'=>$category->id));
                if(empty($catexist)){
                    $category_options[$category->id] = $category->name;
                }
            }
            
        }

        if(!empty($category_options)){
            $mform->addElement('select', 'catid', get_string('selectcategory', 'local_course_details'), $category_options);
            $mform->addRule('catid', get_string('selectcategory','local_course_details'), 'required', '', 'client', false, false);
        }
        //main categories dropdown ends
        //description field for category.

        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'noclean' => true, 'context' => $context);
        $mform->addElement('editor', 'catdescription', get_string('selectcategorydesc', 'local_course_details'),null,$editoroptions);

        
        // $mform->addElement('editor', 'catdescription', get_string('selectcategorydesc', 'local_course_details'));
        // $mform->setType('catdescription', PARAM_RAW);
        // $mform->addRule('catdescription', get_string('selectcategorydesc','local_course_details'), 'required', '', 'client', false, false);

        //image for category.
        $mform->addElement('filemanager', 'catimage', get_string('selectcategoryimage', 'local_course_details'), null,
                    array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
        $mform->addRule('catimage', get_string('selectcategoryimage','local_course_details'), 'required', '', 'client', false, false);
        //category link.
        $mform->addElement('text', 'catlink', get_string('selectcategorylink', 'local_course_details'));
        $mform->addRule('catlink', get_string('selectcategorylink','local_course_details'), 'required', '', 'client', false, false);
        $mform->setType('catlink', PARAM_RAW);

        //Category Display name.
        $mform->addElement('text', 'catdisplay', get_string('categorydisplay', 'local_course_details'));
        $mform->addRule('catdisplay', get_string('categorydisplay','local_course_details'), 'required', '', 'client', false, false);
        $mform->setType('catdisplay', PARAM_RAW);

        //Category support email.
        $mform->addElement('text', 'catsupportemail', get_string('catsupportemail', 'local_course_details'));
        $mform->addRule('catsupportemail', get_string('catsupportemail','local_course_details'), 'required', '', 'client', false, false);
        $mform->setType('catsupportemail', PARAM_RAW);

        $mform->addElement('filemanager', 'catlogo', get_string('selectcategorylogo', 'local_course_details'), null,
                    array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
        $mform->addRule('catlogo', get_string('selectcategorylogo','local_course_details'), 'required', '', 'client', false, false);

        $mform->addElement('header', 'loginheader', get_string('loginheader', 'local_course_details'));

        //Student login page title.
        $mform->addElement('text', 'std_login_pg_title', get_string('std_login_pg_title', 'local_course_details'));
        $mform->addRule('std_login_pg_title', get_string('std_login_pg_title','local_course_details'), 'required', '', 'client', false, false);
        $mform->setType('std_login_pg_title', PARAM_RAW);

        //Student login page color.
        $mform->addElement('text', 'std_login_pg_color', get_string('std_login_pg_color', 'local_course_details'));
        $mform->addRule('std_login_pg_color', get_string('std_login_pg_color','local_course_details'), 'required', '', 'client', false, false);
        $mform->setType('std_login_pg_color', PARAM_RAW);

        //Student login page Description.
        $mform->addElement('textarea', 'std_login_pg_desc', get_string('std_login_pg_desc', 'local_course_details'), 'wrap="virtual" rows="10" cols="50"');
        $mform->setType('std_login_pg_desc', PARAM_RAW);

        //Student login page logo.
        $mform->addElement('filemanager', 'std_login_pg_logo', get_string('std_login_pg_logo', 'local_course_details'), null,
                    array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
        $mform->addRule('std_login_pg_logo', get_string('std_login_pg_logo','local_course_details'), 'required', '', 'client', false, false);

        //Student login page backgound image.
        $mform->addElement('filemanager', 'std_login_pg_bgimg', get_string('std_login_pg_bgimg', 'local_course_details'), null,
                    array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
       
        $mform->addElement('submit', 'submit', get_string('submit', 'local_course_details'));
    }
}
