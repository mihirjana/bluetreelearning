<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_course_details
 * @copyright  Manjunath B K <manjunaathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('forms/category_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php'); 
global $DB, $CFG, $USER;  
require_login(true);
$categoryid = required_param('catid',PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_details/edit.php?catid='.$categoryid);
$title = get_string('update_inst_extra_settings', 'local_course_details');
$title1 = get_string('institutions_inst_extra_settings', 'local_course_details');
$PAGE->navbar->add($title);
$PAGE->set_title($title1);
$PAGE->set_heading($title1);
$PAGE->requires->jquery();
//inserting the form.
if(!empty($categoryid)){
	$mform = new local_course_details_form($CFG->wwwroot . '/local/course_details/edit.php',array('catid'=>$categoryid));
}else{
	$mform = new local_course_details_form();
}
if ($formdata = $mform->get_data()) {
	$context = context_system::instance();
	// $contextid = $context->contextlevel;
	$insert  = new stdClass();
	$insert->id = $formdata->id;
	$insert->catid = $formdata->catid; 

	$formdata->catdescription['text'] = file_save_draft_area_files($formdata->catdescription['itemid'], $context->id,
                'local_course_details', 'catdescription',
                0, array('subdirs' => true), $formdata->catdescription['text']);


	$insert->catdescription = $formdata->catdescription['text'];

	$insert->catimage = $formdata->catimage;

	$context = context_user::instance(2);
	$maxbytes = 5000000;


	$imgitemid = $formdata->catimage;

	if(!empty($imgitemid)){

		file_save_draft_area_files($imgitemid,$context->id,'local_course_details','catimage',$imgitemid,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}

	// file_save_draft_area_files($imgitemid,$contextid,'local_course_details','catimage',$imgitemid,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->catlink = $formdata->catlink;
	$insert->catdisplay = $formdata->catdisplay;
	$insert->catlogo = $formdata->catlogo;
	$insert->catsupportemail = $formdata->catsupportemail;
	
	$imgitemid1  = $formdata->catlogo;

	if(!empty($imgitemid1)){

		file_save_draft_area_files($imgitemid1,$context->id,'local_course_details','catlogo',$imgitemid1,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}


	// file_save_draft_area_files($imgitemid1,$contextid,'local_course_details','catlogo',$imgitemid1,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->std_login_pg_title = $formdata->std_login_pg_title;
	//Manoj to update color data
	$insert->std_login_pg_color = $formdata->std_login_pg_color;
	$insert->std_login_pg_desc = $formdata->std_login_pg_desc;

	$insert->std_login_pg_logo  = $formdata->std_login_pg_logo;
	$std_login_pg_logo  = $formdata->std_login_pg_logo;

	if(!empty($std_login_pg_logo)){

		file_save_draft_area_files($std_login_pg_logo,$context->id,'local_course_details','login_pg_logo',$std_login_pg_logo,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}


	// file_save_draft_area_files($std_login_pg_logo,$contextid,'local_course_details','login_pg_logo',$std_login_pg_logo,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->std_login_pg_bgimg  = $formdata->std_login_pg_bgimg;
	$std_login_pg_bgimg  = $formdata->std_login_pg_bgimg;

	if(!empty($std_login_pg_bgimg)){

		file_save_draft_area_files($std_login_pg_bgimg,$context->id,'local_course_details','login_pg_bgimg',$std_login_pg_bgimg,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}


	// file_save_draft_area_files($std_login_pg_bgimg,$contextid,'local_course_details','login_pg_bgimg',$std_login_pg_bgimg,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	
	$updatedata = $DB->update_record('local_course_details',$insert);
	if($updatedata){
		redirect(new moodle_url('/local/course_details/categoryupdate.php'),get_string('updatesuccess', 'local_course_details'));
	}else{
		redirect(new moodle_url('/local/course_details/edit.php',array('catid'=>$categoryid)));
	}
}
echo $OUTPUT->header();
if(!empty($categoryid))
{
	$check_data = $DB->get_record('local_course_details',array('id'=>$categoryid));
	$data = new \stdClass();
	$data->id = $categoryid;
	$data->catid = $check_data->catid;
	$data->catdescription['text'] = $check_data->catdescription;

	$context = context_user::instance(2);
	$maxbytes = 5000000;

	if(!empty($check_data->catimage)){
		
		$catimage_draftitemid = file_get_submitted_draft_itemid('catimage');
			file_prepare_draft_area($catimage_draftitemid, $context->id, 'local_course_details', 'catimage', $check_data->catimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
		$data->catimage = $catimage_draftitemid;

	}


	// $data->catimage = $check_data->catimage;
	$data->catlink = $check_data->catlink;
	$data->catdisplay = $check_data->catdisplay;
	$data->catsupportemail = $check_data->catsupportemail;

	if(!empty($check_data->catlogo)){
		
		$catlogo_draftitemid = file_get_submitted_draft_itemid('catlogo');
			file_prepare_draft_area($catlogo_draftitemid, $context->id, 'local_course_details', 'catlogo', $check_data->catlogo, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
		$data->catlogo = $catlogo_draftitemid;

	}


	// $data->catlogo = $check_data->catlogo;
	$data->std_login_pg_title = $check_data->std_login_pg_title;
	//Manoj to set color data
	$data->std_login_pg_color = $check_data->std_login_pg_color;
	$data->std_login_pg_desc = $check_data->std_login_pg_desc;

	if(!empty($check_data->std_login_pg_logo)){
		
		$std_login_pg_logo_draftitemid = file_get_submitted_draft_itemid('login_pg_logo');
			file_prepare_draft_area($std_login_pg_logo_draftitemid, $context->id, 'local_course_details', 'login_pg_logo', $check_data->std_login_pg_logo, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
		$data->std_login_pg_logo = $std_login_pg_logo_draftitemid;

	}

	if(!empty($check_data->std_login_pg_bgimg)){
		
		$std_login_pg_bgimg_draftitemid = file_get_submitted_draft_itemid('login_pg_bgimg');
			file_prepare_draft_area($std_login_pg_bgimg_draftitemid, $context->id, 'local_course_details', 'login_pg_bgimg', $check_data->std_login_pg_bgimg, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
		$data->std_login_pg_bgimg = $std_login_pg_bgimg_draftitemid;

	}
	
	// $data->std_login_pg_logo = $check_data->std_login_pg_logo;
	// $data->std_login_pg_bgimg = $check_data->std_login_pg_bgimg;
	$mform->set_data($data);
}
$mform->display();
echo $OUTPUT->footer();