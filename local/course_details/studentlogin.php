<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_course_details
 * @copyright  Manjunath<manjunathbk@elearn10.com>
 * @copyright  EDZLearn Services Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('lib.php');    
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('eps_pages');
$PAGE->set_url($CFG->wwwroot . '/local/course_details/studentdetails.php');
$title = get_string('studentlogin', 'local_course_details');
$PAGE->set_title($title);
$PAGE->requires->jquery();
$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/local/course_details/js/script.js'));
echo $OUTPUT->header(); 
echo display_login_page_header_section();
echo gs_catlinks();
echo $OUTPUT->footer();