//Manoj for wishlist datatable
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );



var input = document.querySelector("#phone");
var input1 = document.querySelector("#contact_number");

window.intlTelInput(input);
window.intlTelInput(input1);

    window.intlTelInput(input, {
     utilsScript: "js/utils.js",
    });

    window.intlTelInput(input1, {
     utilsScript: "js/utils.js",
    });


//for cell phone validation//
var input = document.querySelector("#phone"),
    errorMsg = document.querySelector("#error-msg"),
    validMsg = document.querySelector("#valid-msg");

// Error messages based on the code returned from getValidationError
var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

// Initialise plugin
var intl = window.intlTelInput(input, {
    utilsScript: "js/utils.js"
});

var reset = function() {
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
};

// Validate on blur event
input.addEventListener('blur', function() {
    reset();
    if(input.value.trim()){
        if(intl.isValidNumber()){
            validMsg.classList.remove("hide");
        }else{
            input.classList.add("error");
            var errorCode = intl.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("hide");
        }
    }
});

// Reset on keyup/change event
input.addEventListener('change', reset);
input.addEventListener('keyup', reset);

//for cell phone validation//
var input1 = document.querySelector("#contact_number"),
    errorMsg1 = document.querySelector("#error-msgg"),
    validMsg1 = document.querySelector("#valid-msgg");

// Error messages based on the code returned from getValidationError
var errorMap1 = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

// Initialise plugin
var intll = window.intlTelInput(input1, {
    utilsScript: "js/utils.js"
});

var resett= function() {
    input1.classList.remove("error");
    errorMsg1.innerHTML = "";
    errorMsg1.classList.add("hide");
    validMsg1.classList.add("hide");
};

// Validate on blur event
input1.addEventListener('blur', function() {
    resett();
    if(input1.value.trim()){
        if(intll.isValidNumber()){
            validMsg1.classList.remove("hide");
        }else{
            input1.classList.add("error");
            var errorCodee = intll.getValidationError();
            errorMsg1.innerHTML = errorMap1[errorCodee];
            errorMsg1.classList.remove("hide");
        }
    }
});

// Reset on keyup/change event
input1.addEventListener('change', resett);
input1.addEventListener('keyup', resett);


//Manoj to add wish list and send mail

function add_to_wishlist(cid,userid,reg_type){
    var userid = userid;
    var cid = cid;
    var reg_type = reg_type;
    $.ajax({
        type: 'post',
        url:M.cfg.wwwroot+"/local/course_details/ajax.php",
        data: {'userid': userid ,'cid':cid,'reg_type':reg_type},
        success: function(data) {
            if(data == 'succss'){
                $("#myModal").modal("show");
            }
        }
    });
}
