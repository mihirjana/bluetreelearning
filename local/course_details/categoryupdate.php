<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_course_details
 * @copyright  Manjunath B K <manjunaathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('forms/category_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php'); 
global $DB, $CFG, $USER;  
require_login(true);
$categoryid = optional_param('catid','',PARAM_INT);
$did = optional_param('did','',PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_details/categoryupdate.php');
$title = get_string('institutions_inst_extra_settings', 'local_course_details');
$title1 = get_string('add_inst_extra_settings', 'local_course_details');
$PAGE->navbar->add($title);
$PAGE->set_title($title1);
$PAGE->set_heading($title1);
$PAGE->requires->jquery();
$mform = new local_course_details_form();
if(!empty($did)){
	$response = $DB->delete_records('local_course_details', array('id'=>$did));
	if($response){
		$sucssmsg = get_string('delsuccessmsg','local_course_details');
		echo $OUTPUT->notification($sucssmsg, 'notifysuccess');
	}else{
		$delerrormsg = get_string('delerrormsg','local_course_details');
		echo $OUTPUT->notification($delerrormsg);
	}
}
if ($formdata = $mform->get_data()) {
	// print_object($formdata);
	// die();
	$context = context_system::instance();
	// $contextid = $context->contextlevel;
	$insert  = new stdClass();
	$insert->catid = $formdata->catid; 

	$formdata->catdescription['text'] = file_save_draft_area_files($formdata->catdescription['itemid'], $context->id,
                'local_course_details', 'catdescription',
                0, array('subdirs' => true), $formdata->catdescription['text']);

	
	$insert->catdescription = $formdata->catdescription['text'];
	$insert->catimage = $formdata->catimage;

	$context = context_user::instance(2);
	$maxbytes = 5000000;


	$imgitemid = $formdata->catimage;

	if(!empty($imgitemid)){

		file_save_draft_area_files($imgitemid,$context->id,'local_course_details','catimage',$imgitemid,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}


	// file_save_draft_area_files($imgitemid,$contextid,'local_course_details','catimage',$imgitemid,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->catlink = $formdata->catlink;
	$insert->catdisplay = $formdata->catdisplay;
	$insert->catlogo = $formdata->catlogo;
	$insert->catsupportemail = $formdata->catsupportemail;
	

	
	$imgitemid1  = $formdata->catlogo;

	if(!empty($imgitemid1)){

		file_save_draft_area_files($imgitemid1,$context->id,'local_course_details','catlogo',$imgitemid1,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}

	// file_save_draft_area_files($imgitemid1,$contextid,'local_course_details','catlogo',$imgitemid1,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->std_login_pg_title = $formdata->std_login_pg_title;
	//Manoj 03/03/2020 adding student login color
	$insert->std_login_pg_color = $formdata->std_login_pg_color;
	$insert->std_login_pg_desc = $formdata->std_login_pg_desc;

	$insert->std_login_pg_logo  = $formdata->std_login_pg_logo;
	$std_login_pg_logo  = $formdata->std_login_pg_logo;

	if(!empty($std_login_pg_logo)){

		file_save_draft_area_files($std_login_pg_logo,$context->id,'local_course_details','login_pg_logo',$std_login_pg_logo,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}


	// file_save_draft_area_files($std_login_pg_logo,$contextid,'local_course_details','login_pg_logo',$std_login_pg_logo,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$insert->std_login_pg_bgimg  = $formdata->std_login_pg_bgimg;
	$std_login_pg_bgimg  = $formdata->std_login_pg_bgimg;

	if(!empty($std_login_pg_bgimg)){

		file_save_draft_area_files($std_login_pg_bgimg,$context->id,'local_course_details','login_pg_bgimg',$std_login_pg_bgimg,array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
	}

	// file_save_draft_area_files($std_login_pg_bgimg,$contextid,'local_course_details','login_pg_bgimg',$std_login_pg_bgimg,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 1));

	$res = $DB->insert_record('local_course_details',$insert);
	if($res){
		$redirecturl = new moodle_url($CFG->wwwroot.'/local/course_details/categoryupdate.php');
		redirect($redirecturl,'Saved successfully!');
	}
}
$catdata = $DB->get_records('local_course_details');
$usertable  = new \html_table();
$usertable->id = 'cattable';
$usertable->head = array(get_string('slno', 'local_course_details'),
	get_string('categoryname', 'local_course_details'),
	// get_string('catdescription', 'local_course_details'),
	get_string('catimage', 'local_course_details'),
	get_string('catlink', 'local_course_details'),
	get_string('catlogo', 'local_course_details'),
	get_string('edit', 'local_course_details'),
	get_string('delete', 'local_course_details')
);
$count = 1;
foreach ($catdata as $value) {
	if(!empty($value->catimage)){
		$filearea1="catimage";
		$catimage = local_course_details_image($value->catimage,$filearea1);
		if(!empty($catimage)){
			$cimage = '<img src="'.$catimage.'" alt="'.$value->catdisplay.'" width="100px" class="img">';
		}else{
			$cimage=' ';

		}
	}
		
	if(!empty($value->catlogo)){
		$filearea2="catlogo";
		$catlogo = local_course_details_image($value->catlogo,$filearea2);
		$clogo = '<img src="'.$catlogo.'" alt="'.$value->catdisplay.'" width="50px" >';
	}else{
		$clogo='';
	}
	$edit = new moodle_url($CFG->wwwroot.'/local/course_details/edit.php?catid='.$value->id);
	$editbutton = '<a href ='.$edit.'>'.'<i class="fa fa-pencil" aria-hidden="true"></i>'.'</a>';

	$delete = new moodle_url($CFG->wwwroot.'/local/course_details/delete.php?catid='.$value->id);
	$deletebutton = '<a href ='.$delete.'>'.'<i class="fa fa-trash-o" aria-hidden="true"></i>'.'</a>';
	$usertable->data[] = array($count,$value->catdisplay,$cimage,$value->catlink,$clogo,$editbutton,$deletebutton);
	$count++;
}
$data ='';
$data .= html_writer::start_div('container');
$data .= html_writer::start_div('row');
$data .= html_writer::start_div('col-md-12');
$data .= html_writer::table($usertable);
$data .= html_writer::end_div();
$data .= html_writer::end_div();//end row
$data .= html_writer::end_div();//end container

// print_object(get_institutions_extra_fields(22));

echo $OUTPUT->header();
$mform->display();
echo $data;

echo $OUTPUT->footer();
